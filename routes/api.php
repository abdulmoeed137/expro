<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/find-detail/{qrcodeid}','ApiController@userdetail')->name('scanneddata');
Route::get('/user-qrscan/{userid}/{title}','ApiController@qrscan')->name('qrscan');


// Route::get('lang', function () {

//     $response=[];
//     $msg = [];
    

// $user =

// $response[$user, ]
//     return Response::json([
//         'result' => $response,
//         'msg' => $msg
//     ], 201); // Status code here
// });