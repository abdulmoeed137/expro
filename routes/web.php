<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

//---------------------------------Email Of Qr Copy Modal ----------------------
Route::post('send-emails/qrcopy', 'EmailController@Send_Email_Qrcopy')->name('admin.sendemailqrcopy');



Route::get('/emailtemplate','HomeController@emailtemplate')->name('qrcode');
// Route::get('/','HomeController@index')->name('home');
Route::post('lang', function () {
    if (\Session::has('lang') && \Session::get('lang') == 'en') {
        \Session::put('lang', 'ar');
    } else {
        \Session::put('lang', 'en');
    }

    // dd(\App::getLocale());

    return back();
})->name('lang');
Route::get('/','HomeController@registration')->name('home');
Route::post('/register-store','HomeController@StoreRegistration')->name('storeregister');
Route::get('/dashboard','HomeController@dashboard')->name('dashboard');
Route::get('/livelinks','DashboardController@livelink')->name('livelink');
Auth::routes();
// Route::get('/registration','HomeController@showregsitration')->name('register');
// Route::post('/registration-store', 'HomeController@registration')->name('storeregister');
// Route::get('/home','HomeController@index')->name('home');
Route::get('/speakers','HomeController@speakers')->name('speakers');
Route::get('/agenda','HomeController@agenda')->name('agenda');
Route::get('/objectives','HomeController@objectives')->name('objectives');

Route::get('/sign-up','HomeController@registration2')->name('register2');
Route::post('/registration2-store', 'HomeController@registration_store')->name('storeregister2');

Route::get('/consultancy','DashboardController@consultancy')->name('consultancy');
Route::get('/verification','DashboardController@VerifyUser')->name('verifyuser');
Route::post('/verification/check','DashboardController@VerifyUser_Store')->name('verifyuserstore');
Route::get('/verification/confirm','DashboardController@qr')->name('qr');
    Route::get('/consultancy-form','DashboardController@show_consultancy_form')->name('showconsultancy');
    Route::post('/store-consultancy-form','DashboardController@store_consultancy_form')->name('storeconsultancy');
    Route::get('/consultancy-complete','DashboardController@complete_consultancy_form')->name('consultancycomplete');

Route::middleware('auth')->group(function(){
    
});

Route::prefix('admin')->group(function () {
    Route::get('/login', 'Auth\AdminController@ShowLogin')->name('adminlogin');
    Route::post('/login', 'Auth\AdminController@login')->name('checkadminlogin');

    Route::middleware(['auth:admin'])->group(function () {
        Route::get('admin/edit', 'AdminController@edit')->name('admin.edit');
        Route::put('admin/update', 'AdminController@update')->name('admin.update');
        Route::get('', 'AdminController@dashboard')->name('admindashboard');
        Route::post('logout', 'Auth\AdminController@logout')->name('admin.logout');
        Route::get('/registrations','AdminController@Registrations')->name('admin.registrations');
        //  -----------------------Speakers-------------------------------------------------------------
        Route::get('/speakers', 'AdminController@speakers')->name('admin.speakers');
        Route::prefix('speaker')->group(function () {
            Route::get('/create', 'AdminController@speakerForm')->name('admin.speaker.create');
            Route::post('/stored', 'AdminController@speakerCreate')->name('admin.speaker.store');
            Route::post('/approve', 'AdminController@speakerApprove')->name('admin.speaker.approve');
            Route::get('/{id}', 'AdminController@viewSpeaker')->name('admin.speaker.view');
            Route::get('edit/{id}', 'AdminController@editSpeaker')->name('admin.speaker.edit');
            Route::post('update/{id}', 'AdminController@updateSpeaker')->name('admin.speaker.update');
            Route::post('/sessions', 'AdminController@speakerSessions')->name('admin.speaker.sessions');
            Route::post('/session', 'AdminController@speakerSession')->name('admin.speaker.session');
            Route::post('/meeting', 'AdminController@speakerMeeting')->name('admin.speaker.meeting');
    
        });
        Route::delete('speakers/{speaker}', 'AdminController@deleteSpeakers')->name('admin.speaker.delete');
        Route::post('speakers/{speaker}/restore', 'AdminController@restoreSpeakers')->name('admin.speaker.restore');
        // -----------------Sessions-----------------------------------------
     Route::get('/sessions', 'AdminController@sessions')->name('admin.sessions');
     Route::prefix('session')->group(function () {
         Route::get('/create', 'AdminController@sessionForm')->name('admin.session.create');
         Route::post('/stored', 'AdminController@sessionCreate')->name('admin.session.store');
         Route::post('/approve', 'AdminController@sessionApprove')->name('admin.session.approve');
         Route::get('/{id}', 'AdminController@viewSession')->name('admin.session.view');
         Route::get('edit/{id}', 'AdminController@editSession')->name('admin.session.edit');
         Route::post('update/{id}', 'AdminController@updateSession')->name('admin.session.update');
         Route::post('/sessions', 'AdminController@writerSessions')->name('admin.writer.sessions');
         Route::post('/session', 'AdminController@writerSession')->name('admin.writer.session');
         Route::post('/meeting', 'AdminController@writerMeeting')->name('admin.writer.meeting');

     });
     Route::delete('sessions/{session}', 'AdminController@deleteSession')->name('admin.session.delete');
     Route::post('sessions/{session}/restore', 'AdminController@restoreSession')->name('admin.session.restore');
     
        // -----------------Sponsors-----------------------------------------
     Route::get('/sponsor', 'AdminController@sponsors')->name('admin.sponsors');
     Route::prefix('sponsor')->group(function () {
         Route::get('/create', 'AdminController@sponsorForm')->name('admin.sponsor.create');
         Route::post('/stored', 'AdminController@sponsorCreate')->name('admin.sponsor.store');
         Route::post('/approve', 'AdminController@sponsorApprove')->name('admin.sponsor.approve');
         Route::get('/{id}', 'AdminController@viewSponsor')->name('admin.sponsor.view');
         Route::get('edit/{id}', 'AdminController@editSponsor')->name('admin.sponsor.edit');
         Route::post('update/{id}', 'AdminController@updateSponsor')->name('admin.sponsor.update');
         Route::post('/sponsors', 'AdminController@writerSponsor')->name('admin.writer.sponsors');
         Route::post('/sponsor', 'AdminController@writerSponsor')->name('admin.writer.sponsor');
         Route::post('/meeting', 'AdminController@writerMeeting')->name('admin.writer.meeting');

     });
     Route::delete('sponsors/{sponsor}', 'AdminController@deleteSponsor')->name('admin.sponsor.delete');
     Route::post('sponsors/{sponsor}/restore', 'AdminController@restoreSponsor')->name('admin.sponsor.restore');
        
     //  -----------------------Agenda-------------------------------------------------------------

     Route::prefix('agenda')->group(function () {
        Route::get('/', 'AdminController@agenda')->name('admin.agenda.index');
        Route::get('/create', 'AdminController@agendaAdd')->name('admin.agenda.add');
        Route::post('/create-agenda', 'AdminController@agendaCreate')->name('admin.agenda.create');
        Route::get('/edit/{id}', 'AdminController@agendaEdit')->name('admin.agenda.edit');
        Route::post('/update/{id}', 'AdminController@agendaUpdate')->name('admin.agenda.update');

    });
    Route::delete('agenda/{agenda}', 'AdminController@deleteAgenda')->name('admin.agenda.delete');
    Route::post('agenda/{agenda}/restore', 'AdminController@restoreAgenda')->name('admin.agenda.restore');
    Route::post('send/emails/{type}', 'EmailController@send')->name('admin.send.emails');

        //  -----------------------Links-------------------------------------------------------------
    Route::get('links', 'AdminController@links')->name('admin.links');
    Route::prefix('link')->group(function () {
        Route::post('/create', 'AdminController@linkCreate')->name('admin.link.create');
        Route::post('/update/{id}', 'AdminController@linkUpdate')->name('admin.link.update');
        Route::get('/toggle/status', 'AdminController@toggleLink')->name('admin.link.toggle');
    });

    // ------------------------------------------Consultancy----------------------------

    Route::get('/consultancies','AdminController@Consultancy')->name('admin.consultancy');
    
    // ------------------------------------------Workshop Atendees----------------------------

    Route::get('/workshopattendees','AdminController@WorkshopAttendees')->name('admin.workshopattendees');
    Route::get('/eventattendees','AdminController@EventAttendees')->name('admin.eventattendees');

    //------------------------------Email Route-----------------------------------------
        Route::get('send/emails', 'EmailController@sendEmail')->name('admin.sendemail.emails');




        
    });
});
