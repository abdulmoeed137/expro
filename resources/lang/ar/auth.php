<?php
return [
    'failed' => 'لا يوجد تطابق',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'required'    => 'الحقل مطلوب',
]
?>
