<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{asset('assets/img/fav.png')}}" type="" sizes="16x16">
    <title>EXPRO</title>


    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ URL::asset('admin-assets/plugins/fontawesome-free/css/all.min.css') }}">
    @yield('custom-css')

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::asset('admin-assets/dist/css/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('admin-assets/dist/css/custom.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets2/ccs/style.css') }}">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    @toastr_css
</head>
<style>
    .admin-active{
        background: #44c4ce;
color: #1e1c1c !important;
    }
    .nav-pills .nav-link.active, .nav-pills .show>.nav-link {
    color: #fff;
    background-color: #44c4ce;
}
</style>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i
                            class="fas fa-bars"></i></a>
                </li>
                {{-- <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li> --}}
            </ul>

            <!-- SEARCH FORM -->
            {{-- <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form> --}}

            <!-- Right navbar links -->
            {{-- <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-comments"></i>
          <span class="badge badge-danger navbar-badge">3</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="{{ URL::asset('admin-assets/dist/img/user1-128x128.jpg') }}" alt="User Avatar" class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Brad Diesel
                  <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">Call me whenever you can...</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="{{ URL::asset('admin-assets/dist/img/user8-128x128.jpg') }}" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  John Pierce
                  <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">I got your message bro</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="{{ URL::asset('admin-assets/dist/img/user3-128x128.jpg') }}" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Nora Silvester
                  <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">The subject goes here</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
        </div>
      </li>
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <span class="badge badge-warning navbar-badge">15</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">15 Notifications</span>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> 4 new messages
            <span class="float-right text-muted text-sm">3 mins</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> 8 friend requests
            <span class="float-right text-muted text-sm">12 hours</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> 3 new reports
            <span class="float-right text-muted text-sm">2 days</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button"><i
            class="fas fa-th-large"></i></a>
      </li>
    </ul> --}}
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="#" class="brand-link ">
                <img src="{{ URL::asset('assets/img/fav.png') }}" style="width: 60px;display: block;margin: auto;">
                <!-- <span class="brand-text font-weight-light">Healthcare</span> -->
                <p class="m-0">EXPRO 2022</p>
            </a>

            <!-- Sidebar -->
            <div class="sidebar" style="padding-top: 36px;">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    {{-- <div class="image">
          <img src="{{ URL::asset('admin-assets/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
        </div> --}}
                    <div class="info d-flex justify-content-center align-items-center">
                        <p class="mb-0">{{ Auth::user()->name }}</p>

                        <a href="{{route('admin.edit')}}" class="btn btn-link">Edit</a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                        <li class="nav-item has-treeview">
                            <a  href="{{ route('admindashboard') }}" class="nav-link {{ request()->is('admin') ? 'admin-active' : '' }}" class="">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p class="ff-primary">Dashboard</p>
                            </a>
                        </li>

                        <li class="nav-item has-treeview">
                            <a href="{{route('admin.registrations')}}" class="nav-link {{ request()->is('admin/registrations') ? 'admin-active' : '' }}">
                                <i class="nav-icon fas fa-users"></i>
                                <p>Registrations</p>
                            </a>
                        </li>
                        {{-- <li class="nav-item has-treeview">
                            <a href="{{route('admin.speakers')}}" class="nav-link {{ request()->is('admin/speakers') ? 'admin-active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Speakers</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.sessions')}}" class="nav-link {{ request()->is('admin/sessions') ? 'admin-active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Sessions</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.sponsors')}}" class="nav-link {{ request()->is('admin/sponsor') ? 'admin-active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Sponsor</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.agenda.index')}}" class="nav-link {{ request()->is('admin/agenda') ? 'admin-active' : '' }}">
                                <i class="nav-icon fas fa-bell"></i>
                                <p>Agendas</p>
                            </a>
                        </li>
                        
                        <li class="nav-item has-treeview">
                            <a href="{{route('admin.links')}}" class="nav-link {{ request()->is('admin/links') ? 'admin-active' : '' }}">
                                <i class="nav-icon fab fa-meetup"></i>
                                <p>Meetings</p>
                            </a>
                        </li>
                        <li class="nav-item has-treeview">
                          <a href="{{route('admin.consultancy')}}" class="nav-link {{ request()->is('admin/consultancies') ? 'admin-active' : '' }}">
                              <i class="nav-icon fas fa-users"></i>
                              <p>Consultancy</p>
                          </a>
                        </li>
                        <li class="nav-item has-treeview">
                          <a href="{{route('admin.workshopattendees')}}" class="nav-link {{ request()->is('admin/workshopattendees') ? 'admin-active' : '' }}">
                              <i class="nav-icon fas fa-users"></i>
                              <p>Workshop Attendees</p>
                          </a>
                        </li>
                        <li class="nav-item has-treeview">
                          <a href="{{route('admin.eventattendees')}}" class="nav-link {{ request()->is('admin/eventattendees') ? 'admin-active' : '' }}">
                              <i class="nav-icon fas fa-users"></i>
                              <p>Event Attendees</p>
                          </a>
                        </li> --}}
                        
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="nav-icon fas fa-sign-out-alt"></i>
                                <p>Logout</p>
                                <form id="logout-form" action="{{route('admin.logout')}}" method="POST"
                                    class="d-none">
                                    @csrf
                                </form>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            @yield('section-content')

        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        <footer class="main-footer">
            Copyright &copy; 2022-{{ date('Y') }}<strong style="color: #1c3780"> <a style="color: #1c3780"
                    href="#"> EXPRO
                </a>.</strong>
            All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <script src="{{ URL::asset('admin-assets/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ URL::asset('admin-assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    @yield('custom-script')

    <!-- AdminLTE App -->
    <script src="{{ URL::asset('admin-assets/dist/js/adminlte.js') }}"></script>

    <!-- OPTIONAL SCRIPTS -->
    <script src="{{ URL::asset('admin-assets/dist/js/demo.js') }}"></script>

    @toastr_js
    @toastr_render
</body>

</html>
