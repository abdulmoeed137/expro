@extends('layouts.admin')

@section('custom-css')
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet"
        href="{{ URL::asset('admin-assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet"
        href="{{ URL::asset('admin-assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">
@endsection

@section('section-content')
    <style>
         table th {
        font-weight: 100% !important;
    }

    /* btn animation  */
    .custom-btn {
        color: #ffffff !important;
        border-radius: 5px;
        padding: 12px 55px;
        font-family: 'Lato', sans-serif;
        font-weight: 500;
        background: #006b52 !important;
        cursor: pointer;
        transition: all 0.3s ease;
        position: relative;
        display: inline-block;
        /* box-shadow: inset 2px 2px 2px 0px rgb(255 255 255 / 50%), 7px 7px 20px 0px rgb(0 0 0 / 10%), 4px 4px 5px 0px rgb(0 0 0 / 10%); */
        outline: none;
        text-decoration: none;
        width: 100%;
    }


    /* 10 */
    .btn-10 {
        background: rgb(22, 9, 240);
        background: linear-gradient(0deg, rgb(255 255 255) 0%, rgb(255 255 255) 100%);
        color: #fff;
        border: none;
        transition: all 0.3s ease;
        overflow: hidden;
    }

    .btn-10:after {
        position: absolute;
        content: " ";
        top: 0;
        left: 0;
        z-index: -1;
        width: 100%;
        height: 100%;
        transition: all 0.3s ease;
        -webkit-transform: scale(.1);
        transform: scale(.1);
    }

    .btn-10:hover {
        color: #fff !important;
        background: transparent;
        text-decoration: none;
    }

    .btn-10:hover:after {
        background: #afcc00 !important;
        -webkit-transform: scale(1);
        transform: scale(1);
        color: white !important;

    }
    </style>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Meeting Links</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h3 class="card-title">All Meeting Links</h3>
                            <div>
                                <a href="{{ route('admin.link.toggle') }}" style="z-index: 9;"
                                    class="send ml-1 custom-btn btn-10">
                                    @if ($active == 1)
                                        Deactive Links
                                    @else
                                        Activate Links
                                    @endif
                                </a>
                            </div>
                            <div>
                                <button type="button" style="z-index: 99999999;" class="send ml-1 custom-btn btn-10"
                                    data-toggle="modal" data-target="#add-modal">Add Meeting Link</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Title</th>
                                    <th>Link</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($links as $link)
                                    <tr>
                                        <td>{{ $link->id }}</td>
                                        <td>{{ $link->title }}</td>
                                        <td>{{ $link->link }}</td>
                                        <td>
                                            <button class="btn btn-primary btn-sm editBtn" data-toggle="modal"
                                                data-target="#edit-modal" data-id="{{ $link->id }}"
                                                data-title="{{ $link->title }}" data-link="{{ $link->link }}">
                                                {{-- icon creating confusion in jquery event --}}
                                                {{-- <i class="fas fa-edit"></i> --}}
                                                Edit
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
    <div class="modal fade" id="add-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Create Meeting Link</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ route('admin.link.create') }}">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" id="title" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="link">Link</label>
                            <textarea name="link" id="link" class="form-control" rows="5" required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-end">
                        <button type="submit" style="z-index: 99999999;" class="send ml-1 custom-btn btn-10">Add</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="edit-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Meeting Link</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" id="e_form">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="e_title">Title</label>
                            <input type="text" name="title" id="e_title" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="link">Link</label>
                            <textarea name="link" id="e_link" class="form-control" rows="5" required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-end">
                        <button type="submit" style="z-index: 99999999;" class="send ml-1 custom-btn btn-10">Update</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('custom-script')
    <!-- DataTables -->
    <script src="{{ URL::asset('admin-assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('admin-assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ URL::asset('admin-assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
    </script>
    <script src="{{ URL::asset('admin-assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}">
    </script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.colVis.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                "columnDefs": [{
                    "orderable": false,
                    "targets": 0
                }],
                'aaSorting': [
                    [0, 'desc']
                ],
                dom: 'Bfrtip',
                buttons: [{
                        extend: 'excelHtml5',
                        title: 'Meeting Links',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        title: 'Meeting Links',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    'colvis'
                ]
            });

            $(document).on('click', function(e) {
                console.log($(e.target).hasClass('editBtn'));
                if ($(e.target).hasClass('editBtn')) {
                    var id = $(e.target).attr('data-id');
                    var title = $(e.target).attr('data-title');
                    var link = $(e.target).attr('data-link');
                    var url = '{{ route('admin.link.update', ':id') }}';
                    url = url.replace(':id', id);
                    $('#e_title').val(title);
                    $('#e_link').val(link);
                    $('#e_form').attr('action', url);
                }
            })
        });
    </script>
@endsection
