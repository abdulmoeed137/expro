@extends('layouts.admin')

@section('custom-css')
    <link href="{{ URL::asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/menu.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/vendors.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/custom.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/circle-animated.css') }}" rel="stylesheet">
    @toastr_css

@endsection

@section('section-content')
<style>

table th {
        font-weight: 100 !important;
    }

    /* btn animation  */
    .custom-btn {
        color: #ffffff !important;
        border-radius: 5px;
        padding: 12px 55px;
        font-family: 'Lato', sans-serif;
        font-weight: 500;
        background: #153E58 !important;
        cursor: pointer;
        transition: all 0.3s ease;
        position: relative;
        display: inline-block;
        /* box-shadow: inset 2px 2px 2px 0px rgb(255 255 255 / 50%), 7px 7px 20px 0px rgb(0 0 0 / 10%), 4px 4px 5px 0px rgb(0 0 0 / 10%); */
        outline: none;
        text-decoration: none;
        width: 100%;
    }


    /* 10 */
    .btn-10 {
        background: rgb(22, 9, 240);
        background: linear-gradient(0deg, rgb(255 255 255) 0%, rgb(255 255 255) 100%);
        color: #fff;
        border: none;
        transition: all 0.3s ease;
        overflow: hidden;
    }

    .btn-10:after {
        position: absolute;
        content: " ";
        top: 0;
        left: 0;
        z-index: -1;
        width: 100%;
        height: 100%;
        transition: all 0.3s ease;
        -webkit-transform: scale(.1);
        transform: scale(.1);
    }

    .btn-10:hover {
        color: #fff !important;
        background: transparent;
        text-decoration: none;
    }

    .btn-10:hover:after {
        background: #F5CC44;
        -webkit-transform: scale(1);
        transform: scale(1);
        color: white !important;

    }

</style>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Agenda</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex">
                        <h3 class="card-title">Update Agenda</h3>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('admin.agenda.update',$agendaData->id) }}" method="POST"  enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-6">
                                
                            </div>
                            {{-- <div class="form-group col-md-6">
                                <label for="type">Type</label>
                                
                                <select name="type" id="type" class="form-control" required>
                                    @if ($agendaData->type=="Opening Ceremony")
                                        <option value="Opening Ceremony" selected>Opening Ceremony</option>
                                        <option value="Session">Session</option>
                                        <option value="Break">Break</option>
                                        <option value="Closing Ceremony">Closing Ceremony</option>

                                    @elseif ($agendaData->type=="Session")
                                        <option value="Session" selected>Session</option>
                                        <option value="Opening Ceremony">Opening Ceremony</option>
                                        <option value="Break">Break</option>
                                        <option value="Closing Ceremony">Closing Ceremony</option>

                                    @elseif ($agendaData->type=="Break")
                                        <option value="Break" selected>Break</option>
                                        <option value="Session">Session</option>
                                        <option value="Opening Ceremony">Opening Ceremony</option>
                                        <option value="Closing Ceremony">Closing Ceremony</option>

                                    @else
                                        <option value="Closing Ceremony" selected>Closing Ceremony</option>
                                        <option value="Break">Break</option>
                                        <option value="Session">Session</option>
                                        <option value="Opening Ceremony">Opening Ceremony</option>
                                    @endif
                                    
                                </select>
                            </div> --}}
                            <div class="form-group col-md-6">
                                <label for="day">Day</label>
                                <div>
                                    @if ($agendaData->day == "1")
                                        <input type="radio" checked name="day" value="1" required> Day 1
                                        <input type="radio" name="day" value="2"> Day 2
                                        <input type="radio" name="day" value="3"> Day 3
                                    @elseif ($agendaData->day == "2")
                                        <input type="radio" name="day" value="1" required> Day 1
                                        <input type="radio" checked name="day" value="2"> Day 2
                                        <input type="radio" name="day" value="3"> Day 3
                                    @elseif ($agendaData->day == "3")
                                        <input type="radio" name="day" value="1" required> Day 1
                                        <input type="radio"  name="day" value="2"> Day 2
                                        <input type="radio" checked name="day" value="3"> Day 3
                                    @endif

                                </div>
                            </div>
                           
                            <div class="form-group col-md-6">
                                <label for="start">Start Time</label>
                                <input type="time" value="{{$agendaData->start}}" name="start" id="start" class="form-control" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="end">End Time</label>
                                <input type="time" value="{{$agendaData->end}}" name="end" id="end" class="form-control" required>
                            </div>
                            <div class="form-group col-md-6 topic_en">
                                <label for="topic_en">Title English</label>
                                <input type="text" value="{{$agendaData->topic_en}}" name="topic_en" id="topic_en" class="form-control" required>
                            </div>
                            <div class="form-group col-md-6 topic_ar">
                                <label for="topic_ar">Title Arabic</label>
                                <input type="text" value="{{$agendaData->topic_ar}}" name="topic_ar" id="topic_ar" class="form-control" required>
                            </div>


                            <div class="form-group col-md-6 desc_en">
                                <label for="desc_en">Description English</label>
                                <textarea  name="desc_en"  id="desc_en" class="form-control" >{{$agendaData->desc_en}}</textarea>
                            </div>
                            <div class="form-group col-md-6 desc_ar">
                                <label for="desc_ar">Description Arabic</label>
                                <textarea name="desc_ar"  id="desc_ar" class="form-control" >{{$agendaData->desc_ar}}</textarea>
                            </div>
                            {{-- <div class="form-group col-md-12 speaker_image">
                                <label for="speaker_image">Speaker Picture</label>
                                <input type="file" accept="image/*" name="speaker_image" id="speaker_image" class="form-control" required>
                            </div> --}}
                        </div>
                     <div class="row">
                        <div class="col-md-10"></div>
                        <div class="col-md-2 text-right">
                        <button type="submit" style="z-index: 99999999;" class="send ml-1 custom-btn btn-10">Save</button>
                        </div>
                     </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('custom-script')
    <script>
        $(document).ready(function() {
            // $('input[name="type"]').change(function () {
            //     if (this.value == 'break') {
            //         $('#topic_en, #topic_ar, #speaker_name_en, #speaker_name_ar, #speaker_image').removeAttr('required');
            //         $('.topic_en, .topic_ar, .speaker_name_en, .speaker_name_ar, .speaker_image').hide();
            //     } else {
            //         $('#topic_en, #topic_ar, #speaker_name_en, #speaker_name_ar, #speaker_image').attr('required', true);
            //         $('.topic_en, .topic_ar, .speaker_name_en, .speaker_name_ar, .speaker_image').show();
            //     }
            // })
            $('#type').change(function () {
                if (this.value == 'Session') {
                    $('#topic_en, #topic_ar, #desc_en, #desc_ar,#title_en,#title_ar').attr('required', true);
                    $('.topic_en, .topic_ar, .desc_en, .desc_ar,.title_en,.title_ar ').show();
                } else if (this.value == 'Break') {
                    $('#topic_en, #topic_ar, #desc_en, #desc_ar,#title_en,#title_ar ').removeAttr('required');
                    $('.topic_en, .topic_ar, .desc_en, .desc_ar,.title_ar,.title_en ').hide();
                } else {
                    $('#desc_en, #desc_ar,#title_en,#title_ar').removeAttr('required');
                    $('#topic_en, #topic_ar').attr('required', true);
                    $('.title_ar,.title_en ').hide();
                    // $('.speaker_name_en, .speaker_name_ar, .speaker_image').hide();
                    $('.topic_en, .topic_ar, .desc_en, .desc_ar').show();
                }
            })
        });
    </script>
@endsection
