<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Greening Arabia</title>
    <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Source+Sans+Pro:ital,wght@0,300;0,600;0,700;0,900;1,700;1,900&display=swap"
        rel="stylesheet">

</head>
<style >
    @media only screen and (max-width : 991px) {
        .main-div {
            width: 80% !important;
        }
    }
</style>

<body style="margin: 0;">
    <div
        style="padding-top: 55px; background-image: url('https://imgur.com/0b4deJF.png'); height: 100%; background-repeat: no-repeat;    background-size: cover;">
        <div class="main-div"
            style="width: 400px; margin: auto; box-shadow: 0px 0px 15px 2px #0000001f; background: white;">

            <table style="width: 100%; margin: auto;border-spacing: 0;" border="0" cellpsacing="0" bor>
                <tr>
                    <td style="width: 50%; padding: 5px;"> <img
                            style="    display: block;margin: auto; max-width: 100%;"
                            src="https://greeningarabia.spidertech.dev/assets/img/logo.png" alt="" width="100"> </td>
                    <td style="width: 50%; padding: 5px;"> <img
                            style="    display: block;margin: auto; max-width: 100%;"
                            src="https://greeningarabia.spidertech.dev/assets/img/ini.png" alt="" width="200"> </td>
                </tr>
                <tr>
                    <td colspan="2"
                        style="padding: 20px; text-align: center; font-family: arial; padding-top: 40px;font-family: 'Source Sans Pro', sans-serif;color: black;">
                        Thank you for registering in International Exhibition & Forum On Afforestation Technologies
                    </td>
                </tr>
                <tr>
                    <td colspan="2"
                        style="padding: 20px; text-align: center; font-family: arial; padding-top: 0px;font-family: 'Source Sans Pro', sans-serif;">
                        شكرا لتسجيلك في المعرض والمنتدى الدولي لتقنيات التشجير
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <img width="120" style="width: 140px;margin: auto;display: block; margin-bottom: 30px;"
                        src="{{asset('storage/qr-code/'.$qrcodeid.'.png')}}"
                            alt="">
                    </td>
                </tr>

            </table>
            <table style="width: 100%; margin: auto;border-spacing: 0; border: 0;">
                <tr style="background-color: #006b52;">
                    <td style="padding: 10px 0; text-align: center; padding-left: 10px; padding-right: 10px;">
                        <img style="max-width: 100%;" src="https://greeningarabia.spidertech.dev/assets/img/v-logo.png"
                            alt="" width="100">
                    </td>
                    <td style="padding: 10px 0; text-align: center;">
                        <img style="max-width: 100%;" src="https://greeningarabia22.spidertech.dev/assets/img/new-image/logo1-3.png"
                            alt="" width="224">
                    </td>
                    <td style="padding: 10px 10px; text-align: center;">
                        <img style="max-width: 100%;" src="https://greeningarabia.spidertech.dev/assets/img/mini.png"
                            alt="" width="160">
                    </td>
                </tr>
            </table>
        </div>
        <p
            style="text-align: center; margin-top: 10px; margin-bottom: 10px; font-family: arial;padding-bottom: 10px;font-family: 'Source Sans Pro', sans-serif;">
            ALL Right Reserved</p>
    </div>

</body>

</html>