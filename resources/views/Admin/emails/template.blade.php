<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $subject }}</title>
    <style>
        @font-face {
            font-family: ff-primary-regular;
            src: url({{ asset('assets2/fonts/custom/DINNextLTArabic-Regular-4.ttf') }});
        }

        @font-face {
            font-family: ff-primary-regular-bold;
            src: url({{ asset('assets2/fonts/custom/DINNextLTArabic-Bold-4.ttf') }});
        }

        * {
            box-sizing: border-box;
        }

        body {
            font-family: ff-primary-regular !important;
            padding: 0;
            margin: 0;

        }

    </style>
</head>

<body>
    {!! $content !!}
    {{-- {!! \QrCode::size(300)->generate($name) !!} --}}
    {{-- {!!$qr!!} --}}
    
    {{-- <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(100)->generate('QrCode as PNG image!')) !!}" /> --}}
    
</body>

</html>


