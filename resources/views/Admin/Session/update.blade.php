@extends('layouts.admin')

@section('custom-css')
    <link href="{{ URL::asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/menu.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/vendors.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/custom.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/circle-animated.css') }}" rel="stylesheet">
    @toastr_css

@endsection

@section('section-content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Update Session</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->

    <section class="content">
        <div class="container">
            <form method="POST" action="{{ route('admin.session.update', $session->id) }}" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-body">
                        {{-- <div class="from-group mb-2">
                            <label for="image">Old Image</label>
                            <img src="{{ asset($session->image) }}" alt="img" class="img-fluid form-control"
                                style="height: 10rem; width:10rem;">
                        </div> --}}
                        {{-- <div class="form-group">
                            <label for="image">New Image</label>
                            <input type="file" name="image" id="image"
                                class="form-control @error('image') is-invalid @enderror">
                            @error('image')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div> --}}
                        <div class="row">
                            <h4 class="col-sm-12">Arabic:</h4>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="ar-name">Name</label>
                                    <input type="text" name="ar_name" id="ar-name"
                                        class="form-control @error('ar_name') is-invalid @enderror"
                                        value="{{ $session->ar->name }}" required>
                                    @error('ar_name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="ar-title">Title</label>
                                    <input type="text" name="ar_title" id="ar-title"
                                        class="form-control @error('ar_title') is-invalid @enderror"
                                        value="{{ $session->ar->title }}" required>
                                    @error('ar_title')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="ar-description">Description</label>
                                    <textarea class="form-control @error('ar_description') is-invalid @enderror"
                                        name="ar_description" id="ar-description" rows="10"
                                        required>{{ $session->ar->description }}</textarea>
                                    @error('ar_description')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <h4 class="col-sm-12">English:</h4>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="en-name">Name</label>
                                    <input type="text" name="en_name" id="en-name"
                                        class="form-control @error('en_name') is-invalid @enderror"
                                        value="{{ $session->en->name }}" required>
                                    @error('en_name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="en-title">Title</label>
                                    <input type="text" name="en_title" id="en-title"
                                        class="form-control @error('en_title') is-invalid @enderror"
                                        value="{{ $session->en->title }}" required>
                                    @error('en_title')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="en-description">Description</label>
                                    <textarea class="form-control @error('en_title') is-invalid @enderror"
                                        name="en_description" id="en-description" rows="10"
                                        required>{{ $session->en->description }}</textarea>
                                    @error('en_description')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-end">
                    <button type="submit" class="btn btn-primary">Edit</button>
                </div>
            </form>
        </div>
        <!-- /Wizard container -->
    </section>
@endsection

@section('custom-script')
    <script src="{{ URL::asset('assets/js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/common_scripts.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/velocity.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/functions.js') }}"></script>
    <script src="{{ URL::asset('assets/js/survey_func.js') }}"></script>

@jquery
@toastr_js
@toastr_render

@endsection
