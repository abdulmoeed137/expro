@extends('layouts.admin')

@section('custom-css')
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet"
        href="{{ URL::asset('admin-assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet"
        href="{{ URL::asset('admin-assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">
    @toastr_css
@endsection

<style>
    table th {
        font-weight: 100 !important;
    }

        /* btn animation  */
        .custom-btn {
        color: #ffffff !important;
        border-radius: 5px;
        padding: 12px 55px;
        font-family: 'Lato', sans-serif;
        font-weight: 500;
        background: #006b52 !important;
        cursor: pointer;
        transition: all 0.3s ease;
        position: relative;
        display: inline-block;
        /* box-shadow: inset 2px 2px 2px 0px rgb(255 255 255 / 50%), 7px 7px 20px 0px rgb(0 0 0 / 10%), 4px 4px 5px 0px rgb(0 0 0 / 10%); */
        outline: none;
        text-decoration: none;
        width: 100%;
    }


    /* 10 */
    .btn-10 {
        background: rgb(22, 9, 240);
        background: linear-gradient(0deg, rgb(255 255 255) 0%, rgb(255 255 255) 100%);
        color: #fff;
        border: none;
        transition: all 0.3s ease;
        overflow: hidden;
    }

    .btn-10:after {
        position: absolute;
        content: " ";
        top: 0;
        left: 0;
        z-index: -1;
        width: 100%;
        height: 100%;
        transition: all 0.3s ease;
        -webkit-transform: scale(.1);
        transform: scale(.1);
    }

    .btn-10:hover {
        color: #fff !important;
        background: transparent;
        text-decoration: none;
    }

    .btn-10:hover:after {
        background: #afcc00 !important;
        -webkit-transform: scale(1);
        transform: scale(1);
        color: white !important;

    }

    .pri-btn {
        padding: 13px 10px !important;
        line-height: 0 !important;
        height: 0 !important;
        background: #153E58 !important;
        border: 1px solid #153E58 !important;
    }
    .dan-nt{
        padding: 13px 10px !important;
        line-height: 0 !important;
        height: 0 !important;
    }
</style>
@section('section-content')
    <!-- Content Header (Page header) -->
    {{-- <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Sessoins</h1>
                </div>
            </div>
        </div>
    </section> --}}

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card mt-3">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h3 class="card-title">All Sponsors</h3>
                            <div>
                                <a href="{{ route('admin.sponsor.create') }}" style="z-index: 99999999;"
                                    class="send ml-1 custom-btn btn-10">Create Sponsor</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Image</th>
                                        <th>Arabic</th>
                                        <th>English</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($sessions as $key => $session)
                                        <tr>
                                            <td>{{ $session->id }}</td>
                                            <td>
                                                <img src="{{ asset($session->image) }}" class="img-fluid"
                                                    style="height: 5rem;" alt="img">
                                            </td>
                                            <td>
                                                <div class="p-2">
                                                    
                                                    <h5 class="mb-0">Title:</h5>
                                                    <p class="text-nowrap text-truncate">{{ $session->ar->title }}</p>
                                                    
                                                </div>
                                            </td>
                                            <td>
                                                <div class="p-2">
                                                    
                                                    <h5 class="mb-0">Title:</h5>
                                                    <p class="text-nowrap text-truncate">{{ $session->en->title }}</p>
                                                    
                                                </div>
                                            </td>
                                            <td style="">
                                                <div style="display: flex;">
                                                    <a href="{{ route('admin.sponsor.edit', $session->id) }}"
                                                        class="btn btn-success mr-1 pri-btn">Edit</a>
                                                   @if (!$session->deleted_at)
                                                       <a href="#"  class="btn btn-danger dan-nt"
                                                           onclick="toggleSpeaker({{ $session->id }})">Delete</a>
                                                   @else
                                                       <a href="#" style="z-index: 99999999;" class="send ml-1 custom-btn btn-10"
                                                           onclick="toggleSpeaker({{ $session->id }}, false)">Restore</a>
                                                   @endif
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    <div class="modal fade" id="add-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Create Sponsor</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ route('admin.sponsor.create') }}">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="image">Image</label>
                            <input type="file" name="image" id="image" class="form-control">
                        </div>
                        <div class="row">
                            <h4>Arabic</h4>
                            
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" name="title" id="title" class="form-control">
                                </div>
                            </div>
                           
                        </div>
                    </div>
                    <div class="modal-footer justify-content-end">
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('custom-script')
    <!-- DataTables -->
    <script src="{{ URL::asset('admin-assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('admin-assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ URL::asset('admin-assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
    </script>
    <script src="{{ URL::asset('admin-assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}">
    </script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.colVis.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                "columnDefs": [{
                    "orderable": false,
                    "targets": 0
                }],
                'aaSorting': [
                    [1, 'asc']
                ],
                dom: 'Bfrtip',
                buttons: [{
                        extend: 'excelHtml5',
                        title: 'Sponsors',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        title: 'Sponsors',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    'colvis'
                ]
            });

        });

        function toggleSpeaker(id, del = true) {
            let url = "{{ route('admin.sponsor.delete', 'ss_id') }}"
            if (!del) {
                url = "{{ route('admin.sponsor.restore', 'ss_id') }}"
            }
            url = url.replace("ss_id", id, url)
            document.body.insertAdjacentHTML("beforeend", `
            <form id="toggle" class="d-none" action="${url}" method="POST">
                @csrf
                ${del ? '@method("Delete")' : ""}
            </form>`)
            document.getElementById("toggle").submit()
        }
    </script>

@jquery
@toastr_js
@toastr_render

@endsection
