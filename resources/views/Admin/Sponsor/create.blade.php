@extends('layouts.admin')

@section('custom-css')
    <link href="{{ URL::asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/menu.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/vendors.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/custom.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/circle-animated.css') }}" rel="stylesheet">
@toastr_css

@endsection
<style>
     table th {
        font-weight: 100 !important;
    }

        /* btn animation  */
        .custom-btn {
        color: #ffffff !important;
        border-radius: 5px;
        padding: 12px 55px;
        font-family: 'Lato', sans-serif;
        font-weight: 500;
        background: #006b52 !important;
        cursor: pointer;
        transition: all 0.3s ease;
        position: relative;
        display: inline-block;
        /* box-shadow: inset 2px 2px 2px 0px rgb(255 255 255 / 50%), 7px 7px 20px 0px rgb(0 0 0 / 10%), 4px 4px 5px 0px rgb(0 0 0 / 10%); */
        outline: none;
        text-decoration: none;
        width: 100%;
    }


    /* 10 */
    .btn-10 {
        background: rgb(22, 9, 240);
        background: linear-gradient(0deg, rgb(255 255 255) 0%, rgb(255 255 255) 100%);
        color: #fff;
        border: none;
        transition: all 0.3s ease;
        overflow: hidden;
    }

    .btn-10:after {
        position: absolute;
        content: " ";
        top: 0;
        left: 0;
        z-index: -1;
        width: 100%;
        height: 100%;
        transition: all 0.3s ease;
        -webkit-transform: scale(.1);
        transform: scale(.1);
    }

    .btn-10:hover {
        color: #fff !important;
        background: transparent;
        text-decoration: none;
    }

    .btn-10:hover:after {
        background: #afcc00 !important;
        -webkit-transform: scale(1);
        transform: scale(1);
        color: white !important;

    }

    .pri-btn {
        padding: 13px 10px !important;
        line-height: 0 !important;
        height: 0 !important;
        background: #153E58 !important;
        border: 1px solid #153E58 !important;
    }
    .dan-nt{
        padding: 13px 10px !important;
        line-height: 0 !important;
        height: 0 !important;
    }
</style>
@section('section-content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Create Sponsor</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->

    <section class="content">
        <div class="container">
            <form method="POST" action="{{ route('admin.sponsor.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="image">Image</label>
                            <input type="file" name="image" id="image"
                                class="form-control @error('image') is-invalid @enderror">
                            @error('image')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="row">
                            <h4 class="col-sm-12">Arabic:</h4>
                            {{-- <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="ar-name">Name</label>
                                    <input type="text" name="ar_name" id="ar-name"
                                        class="form-control @error('ar_name') is-invalid @enderror" required>
                                    @error('ar_name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div> --}}
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="ar-title">Title</label>
                                    <input type="text" name="ar_title" id="ar-title"
                                        class="form-control @error('ar_title') is-invalid @enderror" >
                                    @error('ar_title')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            {{-- <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="ar-description">Description</label>
                                    <textarea class="form-control @error('ar_description') is-invalid @enderror"
                                        name="ar_description" id="ar-description" rows="10" required></textarea>
                                    @error('ar_description')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div> --}}
                        </div>
                        <div class="row">
                            <h4 class="col-sm-12">English:</h4>
                            {{-- <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="en-name">Name</label>
                                    <input type="text" name="en_name" id="en-name"
                                        class="form-control @error('en_name') is-invalid @enderror" required>
                                    @error('en_name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div> --}}
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="en-title">Title</label>
                                    <input type="text" name="en_title" id="en-title"
                                        class="form-control @error('en_title') is-invalid @enderror" >
                                    @error('en_title')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            {{-- <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="en-description">Description</label>
                                    <textarea class="form-control @error('en_title') is-invalid @enderror"
                                        name="en_description" id="en-description" rows="10" required></textarea>
                                    @error('en_description')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10"></div>
                <div class="col-md-2">
                    <button type="submit" style="z-index: 99999999;" class="send ml-1 custom-btn btn-10">Create</button>
                </div>
                </div>
            </form>
        </div>
        <!-- /Wizard container -->
    </section>
@endsection

@section('custom-script')
    <script src="{{ URL::asset('assets/js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/common_scripts.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/velocity.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/functions.js') }}"></script>
    <script src="{{ URL::asset('assets/js/survey_func.js') }}"></script>


    <script>
@if(Session::has('message'))
toastr.options = {
    "closeButton": true,
    "progressBar": true
}
toastr.success("{{ session('message') }}");
@endif
</script>

@jquery
@toastr_js
@toastr_render

@endsection
