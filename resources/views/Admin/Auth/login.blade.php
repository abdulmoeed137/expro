<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="{{asset('assets/img/fav.png')}}" type="" sizes="16x16">
  <title>EXPRO</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ URL::asset('admin-assets/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ URL::asset('admin-assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ URL::asset('admin-assets/dist/css/adminlte.min.css') }}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style>
    .login-bg-img {
      min-height: 100vh;
      background-size: cover;
      background-repeat: no-repeat;
      background-position: right;
    }
    .login-bg-img::before{
      content: "";
      position: absolute;
      height: 100%;
      width: 100%;
      /* background: rgb(0 0 0 / 50%); */
    }

    
  /* btn animation  */
  .custom-btn {
    color: #ffffff !important;
    border-radius: 5px;
    padding: 9px 55px;
    font-family: 'Lato', sans-serif;
    font-weight: 500;
    background: #1c3780 !important;
    cursor: pointer;
    transition: all 0.3s ease;
    position: relative;
    display: inline-block;
    /* box-shadow: inset 2px 2px 2px 0px rgb(255 255 255 / 50%), 7px 7px 20px 0px rgb(0 0 0 / 10%), 4px 4px 5px 0px rgb(0 0 0 / 10%); */
    outline: none;
    text-decoration: none;
    width: 100%;
  }

  
/* 10 */
.btn-10 {
    background: #1c3780 ;
    background: linear-gradient(0deg, rgb(255 255 255) 0%, rgb(255 255 255) 100%);
	color: #fff;
	border: none;
	transition: all 0.3s ease;
	overflow: hidden;
  }
  .btn-10:after {
	position: absolute;
	content: " ";
	top: 0;
	left: 0;
	z-index: -1;
	width: 100%;
	height: 100%;
	transition: all 0.3s ease;
	-webkit-transform: scale(.1);
	transform: scale(.1);
  }
  .btn-10:hover {
	color: #fff !important;
	border: none;
	background: transparent;
	text-decoration: none;
  }
  .btn-10:hover:after {
	background:#44c4ce;
	-webkit-transform: scale(1);
	transform: scale(1);
	color: white !important;
  }
  .login-box, .register-box {
    width: 460px;
}

  </style>
  @toastr_css

</head>

<body class="hold-transition login-page login-bg-img">

  <section class="login-bg">
    <div class="login-box">
      <div class="login-logo">
        <a href="javascript:;">
          <img style="position: relative;" class="logo" src="{{ URL::asset('images/logo-wide.png') }}" alt="" width="400"></a>
      </div>
      <!-- /.login-logo -->
      <div class="card">
        <div class="card-body login-card-body">
          <h3 class="login-box-msg" style="color: #1c3780;">EXPRO</h3>

          <form method="POST" action="{{ route('checkadminlogin') }}">
            @csrf
            <div class="input-group mb-3">
              <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" name="email" value="{{ old('email') }}" autocomplete="email" autofocus>
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-envelope"></span>
                </div>
              </div>
              @error('email')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
            <div class="input-group mb-3">
              <input type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" name="password" autocomplete="current-password">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
              @error('password')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
            <div class="row">
              {{-- <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div> --}}
              <!-- /.col -->
              <div class="col-12">
                <button type="submit" style="z-index: 99999999;" class="send ml-1 custom-btn btn-10">Sign In</button>
              </div>
              <!-- /.col -->
            </div>
          </form>

          {{-- <div class="social-auth-links text-center mb-3">
        <p>- OR -</p>
        <a href="#" class="btn btn-block btn-primary">
          <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
        </a>
        <a href="#" class="btn btn-block btn-danger">
          <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
        </a>
      </div> --}}
          <!-- /.social-auth-links -->

          {{-- <p class="mb-1">
        <a href="forgot-password.html">I forgot my password</a>
      </p>
      <p class="mb-0">
        <a href="register.html" class="text-center">Register a new membership</a>
      </p> --}}
        </div>
        <!-- /.login-card-body -->
      </div>
    </div>
  </section>
  <!-- /.login-box -->

  <!-- jQuery -->
  <script src="{{ URL::asset('admin-assets/plugins/jquery/jquery.min.js') }}"></script>
  <!-- Bootstrap 4 -->
  <script src="{{ URL::asset('admin-assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ URL::asset('admin-assets/dist/js/adminlte.min.js') }}"></script>
  @jquery
  @toastr_js
  @toastr_render
</body>

</html>