@extends('layouts.app')
@section('content')
<div class="vision-main">
    <div class="vision-section">
        <div class="custom-container">
    <div classs="relative">
            <div class="absolute leave">
              <img src="{{ asset('assets/img/banner-icon/live-leave.png') }}" alt="">
            </div>
            <div class="absolute circle fade-in-text">
              <img src="{{ asset('assets/img/anime/circle.svg') }}" alt="">
            </div>
            <div class="absolute downwardC blink" style="left: 0;bottom: 0;">
              <img src="{{ asset('assets/img/banner-icon/banner-dbl.png') }}" alt="">
            </div>

             <div class="absolute downwardC slightMove" style="top: 277%;z-index: 9;">
              <img src="{{ asset('assets/img/banner-icon/banner-dbl.png') }}" alt="">
            </div>
         </div>

            <div class="row">
                <div class="col-md-6">
                    <h1 class="ab-first">{{ __('Exhibition & Forum Main Objectives') }}</h1>
                    <ul>
                        <li>
                            {{ __('Providing a platform to encourage government agencies, semi-governmental organizations, companies, and nonprofit organizations targeting stakeholders for combating desertification and increasing vegetation cover') }}

                        </li>
                        <li>
                            {{ __('Highlighting the pioneering role of the Kingdom of Saudi Arabia and the importance of the National Center for Vegetation Cover Development and Combating Desertification') }}
                        </li>
                        <li style="margin-bottom: 0 !important;">
                            {{ __('Bringing together major investors, corporate leaders, decision-makers, technology solution providers, and green industry leaders interested in climate, environmental issues, and the development of sustainability tools') }}
                        </li>
                    </ul>
                </div>

                <div class="col-md-6">
                    <div class="left-box">

                        <img src="{{ asset('assets/img/circle/cir-1.jpg')}}" width="100%" height="100%"/>
                        <!--<figure style="background-image: url('{{ asset('assets/img/circle/cir-1.jpg') }}');"></figure> -->
                    </div>
                </div>

            </div>

            <div class="row col-reverse py-md-5 py-2">

                <div class="col-md-6">
                    <div class="left-box-2">


                        <figure
                            style="background-image: url('{{ asset('assets/img/circle/cir-4.JPG') }}');">

                        </figure>
                    </div>
                </div>

                <div class="col-md-6 padd-l">

                    <ul class="sec-ul-med" style="">
                        <h1 class="ab-first">{{ __('Exhibition & Forum Strategic Objectives') }}</h1>
                        <li>{{ __('Assessing and reviewing the most important research, studies, and Technologies in the field of afforestation and combating desertification') }}

                        </li>
                        <li>
                            {{ __('Bridging the gap by converging global experiences and learning about success models implemented around the world') }}
                        </li>
                        <li style="margin-bottom: 0 !important;">
                            {{ __('Contributing to the achievement of Sustainable Development Goals of the United Nations') }}
                        </li>
                    </ul>
                </div>

            </div>


        </div>
    </div>
</div>

<section class="exhib-main">
    <div class="custom-container">
        <h1>{{__('Exhibition & Forum Investment Objectives')}}</h1>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-6 my-2">
               <div class="exhib-item ">
                <img src="{{ asset('assets/img/new-image/ob-1.png') }}" alt="">
                <h6>{{__('Enhancing attendees’ knowledge')}}</h6>
               </div>
            </div>
            <div class="col-md-3 col-sm-6 col-6 my-2">
                <div class="exhib-item">
                 <img src="{{ asset('assets/img/new-image/ob-2.png') }}" alt="">
                 <h6>{{__('Concluding agreements and memorandums of understanding.')}}</h6>
                </div>
             </div>
             <div class="col-md-3 col-sm-6 col-6 my-2">
                <div class="exhib-item ">
                 <img src="{{ asset('assets/img/new-image/ob-3.png') }}" alt="">
                 <h6>{{__('Highlighting investment opportunities in the environmental sector')}}</h6>
                </div>
             </div>
             <div class="col-md-3 col-sm-6 col-6 my-2">
                <div class="exhib-item ">
                 <img src="{{ asset('assets/img/new-image/ob-4.png') }}" alt="">
                 <h6>{{__('To open a window of communication to launch research partnerships with universities along with local, regional and international research centers.')}}</h6>
                </div>
             </div>
        </div>
    </div>
</section>



<div class="axes-sec">
    <div class="custom-container">
        <div class="row">
            <div class="col-md-5">
                <h1 class="audien-p-head">
                    {{__('Target Audience')}}
                </h1>
            </div>
            {{-- <div class="col-md-7">
                <p>The International Exhibition & Forum On Afforestation Technologies includes a set of comprehensive and integrated axes regarding afforestation and its Technologies and combating desertification, on the following:</p>
            </div> --}}
        </div>
        <div class="axes-row">
            <div class="axes-item audience-item">
                <img src="{{ asset('assets/img/new-image/audi-1.png') }}" alt="">
                <p>{{__('Experts and specialists in the environment and afforestation sector - combating desertification - water - soil')}}</p>
            </div>
            <div class="axes-item audience-item">
                <img src="{{ asset('assets/img/new-image/audi-2.png') }}" alt="">
                <p>{{__('Those interested and activists in afforestation and environmental sustainability issues')}}</p>
            </div>
            <div class="axes-item audience-item">
                <img src="{{ asset('assets/img/new-image/audi-3.png') }}" alt="">
                <p>{{__('Local and International Investors and Entrepreneurs')}}</p>
            </div>
            <div class="axes-item audience-item">
                <img src="{{ asset('assets/img/new-image/audi-4.png') }}" alt="">
                <p>{{__('Industries, projects and consultancies related to afforestation and combating desertification')}}</p>
            </div>
            <div class="axes-item audience-item">
                <img src="{{ asset('assets/img/new-image/audi-5.png') }}" alt="">
                <p>{{__('Media, opinion influencers and social media celebrities')}}</p>
            </div>
            <div class="axes-item audience-item">
                <img src="{{ asset('assets/img/new-image/audi-6.png') }}" alt="">
                <p>{{__('Providers of technology, equipment, and systems specialized in the theme of the exhibition')}}</p>
            </div>
            <div class="axes-item audience-item">
                <img src="{{ asset('assets/img/new-image/audi-7.png') }}" alt="">
                <p>{{__('Research centers - universities and specialized colleges')}}</p>
            </div>
            <div class="axes-item audience-item">
                <img src="{{ asset('assets/img/new-image/audi-8.png') }}" alt="">
                <p>{{__('General Public')}}</p>
            </div>
        </div>
    </div>
</div>
@endsection
