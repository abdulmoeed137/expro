{{-- @extends('layouts.app')

@section('content')
<style>
    .login{
        display: none !important;
    }
    .gallery-slider{
        display: none !important;
    }
    .gallery-heading{
        display: none !important;
    }
    .main-header{
        background: #F8F7F5;
    }
</style>
<div class="login-section">
<div class="custom-container">
  <div classs="relative">
            <div class="absolute leave">
              <img src="{{ asset('assets/img/anime/leave.png') }}" alt="">
            </div>
            <div class="absolute circle fade-in-text">
              <img src="{{ asset('assets/img/anime/circle.svg') }}" alt="">
            </div>
            <div class="absolute downwardC blink" style="left: 40%;
    right: 0;
    margin: 0 auto;">
              <img src="{{ asset('assets/img/anime/C.svg') }}" alt="">
            </div>
         </div>

    <div class="row foot-row">

        <div class="col-md-6 hidden-xs">
          <h1 class="login-left-head">
            {{__('International Exhibition & Forum On Afforestation Technologies')}}
          </h1>
          <p class="login-sub-head">{{__('29th - 31st May 2020')}}</p>
        </div>
        <div class="col-md-5">
            <div class="form-sec" style="    padding-bottom: 40px;">
                <div class="section-tittle text-left mb-10 head">
                    <h2 class="form-heading ">{{__('Log In')}}</h2>

         
            </div>
            <div class="reg-form">
                <form method="POST" action="{{ route('login') }}">
                    @CSRF
                    <div class="form-group col-md-12">
                        <label for="email">{{__('Email')}}</label>
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror">
                        @error('email')
                       <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>{{$message}}</span>
                        @enderror
                    </div>

                    <div class="form-group col-md-12">
                        <label for="password">{{__('Password')}}</label>
                        <input name="password" type="password" class="form-control @error('password') is-invalid @enderror">
                        @error('password')
                        <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>{{$message}}</span>
                        @enderror

                    </div>



                    <div class="form-group col-md-12 text-left rt-tex">
                 
                        <div class="header-right-btn f-right d-lg-block ml-30 submit-btn mt-3 mb-5">
                            <button type="submit" class="btn header-btn w-40 border-r">{{__('Log In')}}</button>
                        </div>

                        <br>
                        <br>
                    </div>
                </form>
            </div>
        </div>
        </div>
        </div>
    </div>
</div>
</div>

@endsection --}}
