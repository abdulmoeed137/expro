@extends('layouts.app')
<style>
</style>
@section('content')
<style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }

    #back-top-2 {
        display: none !important;
    }

    .gallery-slider {
        display: none !important;
    }

    .gallery-heading {
        display: none !important;
    }

    .main-header {
        background: #F8F7F5;
    }
</style>
@toastr_css
<div class="reg-section">

    <div class="custom-container reg-form">
        <div classs="relative">
            <div class="absolute leave">
                <img src="{{ asset('assets/img/anime/leave.png') }}" alt="">
            </div>
            <div class="absolute circle fade-in-text">
                <img src="{{ asset('assets/img/anime/circle.svg') }}" alt="">
            </div>
            <div class="absolute downwardC blink">
                <img src="{{ asset('assets/img/anime/downwardC.svg') }}" alt="">
            </div>
        </div>
        <div class="col-md-12">
            <h2 class="reg-heading">{{__('Register')}}</h2>
        </div>
        <form action="" method="POST">
            @CSRF
            <div class="row">
                <div class="col-md-5 mb-md-4 mb-2">
                    <div class="form-sec">
                        {{-- <div class="section-tittle text-left mb-10 head">
                            <h2 class="form-heading ">{{__('Contact Information')}}</h2>
                        </div> --}}
                        <div class="col-md-12">
                            <label for="">{{__('Full Name')}} <span style="color: red;">*</span></label>
                            <input name="name" class="@error('name') is-invalid @enderror" value="{{ old('name') }}"
                                type="text">
                            @error('name')
                            <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i> {{$message}}</span>
                            @enderror
                        </div>

                        {{-- <div class="col-md-12">
                            <label for="">{{__('Country')}} <span style="color: red;">*</span></label>
                            <input name="country" class="@error('country') is-invalid @enderror"
                                value="{{ old('country') }}" type="text">
                            @error('country')
                            <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>{{$message}}</span>
                            @enderror
                        </div> --}}
                        <div class="col-md-12">
                            <label for="">{{__('Phone Number')}} <span style="color: red;">*</span></label>
                            <input type="number" name="phonenumber" class="@error('phonenumber') is-invalid @enderror"
                                value="{{ old('phonenumber') }}">
                            @error('phonenumber')
                            <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>{{$message}}</span>
                            @enderror
                        </div>


                        {{-- <div class="col-md-12">
                            <label for="">{{__('Mobile/Whatsapp')}} <span style="color: red;">*</span></label>
                            <input type="number" name="mobilenumber" class="@error('mobilenumber') is-invalid @enderror"
                                value="{{ old('mobilenumber') }}">
                            @error('mobilenumber')
                            <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>{{$message}}</span>
                            @enderror
                        </div> --}}
                        <div class="col-md-12">
                            <label for="">{{__('Email Address')}} <span style="color: red;">*</span></label>
                            <input name="email" class="@error('email') is-invalid @enderror" value="{{ old('email') }}"
                                type="email">
                            @error('email')
                            <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>{{$message}}</span>
                            @enderror
                        </div>

                        {{-- <div class="col-md-12">
                            <label for="">{{__('Password')}} <span style="color: red;">*</span></label>
                            <input name="Password" class="@error('password') is-invalid @enderror" type="password">
                            @error('password')
                            <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>{{$message}}</span>
                            @enderror
                        </div> --}}


                    </div>
                </div>

                <div class="col-md-5 mb-md-4 mb-2">
                    <div class="form-sec">
                        {{-- <div class="section-tittle text-left mb-10 head">
                            <h2 class="form-heading">{{__('Company Information')}}</h2>
                        </div> --}}
                        <div class="col-md-12">
                            <label for="">{{__('Entity Name')}} <span style="color: red;">*</span></label>
                            <input name="companyname" class="@error('companyname') is-invalid @enderror"
                                value="{{ old('companyname') }}" type="text">
                            @error('companyname')
                            <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>{{$message}}</span>
                            @enderror
                        </div>

                        {{-- <div class="col-md-12">
                            <label for="">{{__('Company Website')}} <span style="color: red;">*</span></label>
                            <input name="companywebsite" class="@error('companywebsite') is-invalid @enderror"
                                value="{{ old('companywebsite') }}" type="text">
                            @error('companywebsite')
                            <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>{{$message}}</span>
                            @enderror
                        </div> --}}

                        <div class="col-md-12">
                            <label for="">{{__('Job Title')}} <span style="color: red;">*</span></label>
                            <input name="jobtitle" class="@error('jobtitle') is-invalid @enderror"
                                value="{{ old('jobtitle') }}" type="text">
                            @error('jobtitle')
                            <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>{{$message}}</span>
                            @enderror
                        </div>
                        {{-- <div class="col-md-12">
                            <label class="last-labe" for="" style="color: #707070; width: 100%;">{{__('How Did You hear about us ?')}} <span style="color: red;">*</span></label>
                            <select name="reference" class="@error('reference') is-invalid @enderror" id="">
                                <option value="">{{\App::getLocale()=="en"?"Select Option":"حدد الخيار"}}</option>
                                <option value="Official Website">{{\App::getLocale()=="en"?"Official Website":"الموقع
                                    الرسمي"}}</option>
                                <option value="Google Search">{{\App::getLocale()=="en"?"Google Search":"بحث جوجل"}}
                                </option>
                                <option value="Promotion Mails">{{\App::getLocale()=="en"?"Promotion Mails":"رسائل
                                    ترويجية"}}</option>
                                <option value="One-to-One/ direct email">{{\App::getLocale()=="en"?"One-to-One/ direct
                                    email":"البريد الالكتروني المباشر"}}</option>
                                <option value="Social Media">{{\App::getLocale()=="en"?"Social Media":"وسائل التواصل
                                    الاجتماعي"}}</option>
                                <option value="Printed Media">{{\App::getLocale()=="en"?"Printed Media":"وسائل الاعلام
                                    المطبوعة"}}</option>
                                <option value="Online Magazine">{{\App::getLocale()=="en"?"Online Magazine":"المجلة
                                    الالكترونية"}}</option>
                                <option value="Recommended by friend">{{\App::getLocale()=="en"?"Recommended by
                                    friend":"اوصى به صديق"}}</option>
                                <option value="You contacted me">{{\App::getLocale()=="en"?"You contacte me":"لقد تواصلت
                                    معي"}}</option>
                            </select>
                            @error('reference')
                            <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>{{$message}}</span>
                            @enderror
                        </div> --}}
                        <div class="">

                            <div class="col-md-6">
                                <div class="header-right-btn f-right d-lg-block ml-30 submit-btn mt-5">
                                    <button type="submit"
                                        class="btn header-btn w-100 border-r reg-bbt">{{__('Submit')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>
            </div>

            <div class="row">


            </div>
        </form>
    </div>
</div>
<script>
    @if (Session::has('message'))
            toastr.options = {
            "closeButton": true,
            "progressBar": true,
            }
            toastr.success("{{ session('message') }}");
        @endif

</script>
@jquery
@toastr_js
@toastr_render

@endsection