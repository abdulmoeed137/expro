@extends('layouts.app')
@section('content')

    <div class="speakers-page-main">
        <div class="custom-container">
         <div classs="relative">
            <div class="absolute leave">
              <img src="{{ asset('assets/img/banner-icon/live-leave.png') }}" alt="">
            </div>
            <div class="absolute circle fade-in-text">
              <img src="{{ asset('assets/img/anime/circle.svg') }}" alt="">
            </div>
            <div class="absolute downwardC blink" style="left: 0;bottom: 0;">
              <img src="{{ asset('assets/img/banner-icon/banner-dbl.png') }}" alt="">
            </div>

             <div class="absolute downwardC slightMove" style="top: 277%;z-index: 9;">
              <img src="{{ asset('assets/img/banner-icon/banner-dbl.png') }}" alt="">
            </div>
         </div>
            <h1>{{__('Speakers')}}</h1>
            <div class="speakers-row">
                @foreach ($speakers as $speakers)
                <div class="speakers-item">
                    <img src="{{ asset($speakers->image) }}" alt="">
                    <h4>{{ \App::getLocale() == 'en' ? $speakers->en->name : $speakers->ar->name }}</h4>
                    <p>{{ \App::getLocale() == 'en' ? $speakers->en->title : $speakers->ar->title }}</p>
                </div>
                @endforeach

            </div>
        </div>
    </div>

@endsection
