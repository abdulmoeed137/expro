@extends('layouts.app')
@section('content')
<style>
     /* timer css */

 .days-en-1 {
        position: relative;
    }
    .days-en-1::before {
        content: "1";
        position: absolute;
        font-size: 22px !important;
        color: #006b55;
        font-size: 3.5rem;
        color: #006b55 !important;
    }
    .days-ar-1 {
        position: relative;
    }
    .days-ar-1::before {
        content: "١";
        position: absolute;
        font-size: 22px !important;
        color: #006b55;
        font-size: 3.5rem;
        color: #006b55 !important;
    }
    .en-1::before {
        content: "1";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .ar-1::before {
        content: "١";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .en-2::before {
        content: "2";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .ar-2::before {
        content: "٢";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .en-3::before {
        content: "3";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .ar-3::before {
        content: "٣";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .en-4::before {
        content: "4";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .ar-4::before {
        content: "٤";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .en-5::before {
        content: "5";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .ar-5::before {
        content: "٥";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .en-6::before {
        content: "6";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .ar-6::before {
        content: "٦";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .en-7::before {
        content: "7";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .ar-7::before {
        content: "٧";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .en-8::before {
        content: "8";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .ar-8::before {
        content: "٨";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .en-9::before {
        content: "9";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .ar-9::before {
        content: "٩";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .en-0::before {
        content: "0";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .ar-0::before {
        content: "٠";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }
    .header-new-btn{
        display: none !important
    }
</style>
<section class="banner-img dashboard-main" 
{{-- style="background-image: url({{ asset('assets/img/banner-new.jpg') }});  background-size: cover; background-repeat: no-repeat;background-position: bottom;padding: 40px 0;    padding-bottom: 70px;" --}}
>
<div class="youtube-main">
    <div class="absolute leave" style=" visibility: inherit; transform: translate3d(0px, -159.86px, 0px);">
        <img src="{{ asset('assets/img/new-image/live-leave.png')}}" alt="">
      </div>
      <div class="absolute circle fade-in-text">
        <img src="{{ asset('assets/img/new-image/brown-circle.png')}}" alt="" width="120">
      </div>
    <div class="custom-container">
        <h1 class="li-head">{{__('live stream')}}</h1>

        {{-- <div class="row">
            <div class="col-md-6">
                <p>Session: Session Name</p>
            </div>
            <div class="col-md-6">
                <p>Speakers: Speakers Name</p>
            </div>
        </div> --}}
        <div class="row">
            <div class="col-md-12 col-sm-12 col-12 join-btn">
                {{-- href="{{ asset('storage/qr-code/'.$qrcodebyemail->qrcodeid.'.png') }}" --}}
                <a target="_blank" href="https://youtu.be/An5DjwYcdRk" style="color: white" class="btn header-btn w-28 border-r reg-bbt">{{__('Join Youtube')}}</a>
                <a target="_blank" href="https://twitter.com/greeningarabia/status/1531157588552859649?t=zEOtRvsAKnSujlSnMdy_oQ&s=08" style="color: white" class="btn header-btn w-28 border-r reg-bbt">{{__('Join Twitter')}}</a>
            </div>
            {{-- youtube previous link --}}
                {{-- https://youtu.be/Ux0__q7Fe-Y --}}
                
                {{-- Twitter previous link --}}

                {{-- https://twitter.com/greeningarabia/status/1530790365862805504 --}}
            <div class="col-md-6 col-sm-12 col-12">
                {{-- <h4 c/lass="text-right" style="    color: #a6cd37;">{{__('Remaining Time to Register')}}</h4> --}}
                {{-- <div class="countdown">
                    <div class="container-day child first-child text-center">
                        <h3 class="time-dis" style="    color: #a6cd37;">{{ __('Days') }} </h3>
                        <h3 id="days">Time</h3>
                    </div>
                    <div class="container-hour child text-center">
                        <h3 class="time-dis" style="    color: #a6cd37;"> {{ __('Hours') }} </h3>
                        <h3 id="hours">Time</h3>
                    </div> --}}
                    {{-- <div class="container-minute child">
                        <h3 class="time-dis">{{ __('Minutes') }}</h3>
                        <h3 id="minutes">Time</h3>
                    </div>
                    <div class="container-second child-last">
                        <h3 class="time-dis">{{ __('Seconds') }}</h3>
                        <h3 id="seconds">Time</h3>
                    </div> --}}
                </div>
            </div>
        </div>
        
            {{-- @foreach ($youtubelink as $youtubelink)
                <iframe width="100%" height="409" src="{{ $youtubelink->link }}" title="YouTube video player"
                    frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
            @endforeach --}}
       
    </div>
</div>
</section>
@endsection