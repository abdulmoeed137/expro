<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="{{ app()->getLocale() == 'en' ? 'ltr' : 'rtl' }}">


<head>
    <meta charset="utf-8">
    <title> Greening Arabia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/fav.png')}}">
    <!-- CSS here -->

    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/slicknav.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/gijgo.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/slick.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/new-style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/responsiveness.css')}}">
</head>

<body>
    <header>
        <!--? Header Start -->
        <div class="header-area">
            <div class="main-header">
                <div class="custom-container">
                    <div class="row align-items-center bot-bor-head">
                        <!-- Logo -->
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-10 col-10">
                            <div class="logo" style="display: flex;gap: 5px;">
                                <a href="{{route('home')}}"><img class="set-logo-mob-1"
                                        src="{{asset('assets/img/logo.png')}}" alt="" width="90"></a>
                                <a target="_blank"><img class="logo-two set-logo-mob-2"
                                        src="{{asset('assets/img/ini.png')}}" alt=""></a>
                                <a href="{{route('register')}}" class="header-new-btn">{{__('Registration')}}</a>
                            </div>
                        </div>
                        <div class="col-xl-9 col-lg-9 col-md-9 pr-0">
                            <div class="menu-main d-flex align-items-center justify-content-end">
                                <!-- Main-menu -->
                                <div class="main-menu f-right d-none d-lg-block">
                                    <nav>
                                        <ul id="navigation">
                                            <li><a class="{{ request()->is('home') ? 'active' : '' }} {{ request()->is('/*') ? 'active' : '' }}"
                                                    href="{{ route('home') }}">{{ __('Home') }}</a></li>



                                            {{-- <li><a  href="home#intro" onclick="myFunction()">{{__('About')}}</a></li> --}}
                                            <li><a href="{{ route('speakers') }}"
                                                    class="{{ request()->is('speakers') ? 'active' : '' }}">{{ __('Speakers') }}</a>
                                            </li>
                                            <li><a href="{{ route('agenda') }}"
                                                    class="{{ request()->is('agenda') ? 'active' : '' }}">{{ __('Agenda') }}</a>
                                            </li>
                                            <li><a href="{{ route('objectives') }}"
                                                    class="{{ request()->is('objectives') ? 'active' : '' }}">{{ __('Exhibition Objectives') }}</a>
                                            </li>
                                            <li><a href="{{ route('livelink') }}"
                                                class="{{ request()->is('livelinks') ? 'active' : '' }}">{{ __('live stream') }}</a>
                                            @auth
                                                <li><a href="{{ route('dashboard') }}"
                                                        class="{{ request()->is('dashboard') ? 'active' : '' }}">{{ __('Dashboard') }}</a>
                                                </li>
                                            @endauth

                                            @guest

                                                {{-- <li><a class="{{ request()->is('login') ? 'active' : '' }}" href="{{route('login')}}">{{__('Log In')}}</a></li> --}}

                                            @endguest

                                            @guest
                                                <li><a class="{{ request()->is('register') ? 'active' : '' }}"
                                                        href="{{ route('register') }}">{{ __('Register') }}</a></li>
                                            @endguest

                                            @auth


                                            @endauth




                                            @auth
                                                <li>
                                                    <details class="accor-dis-b">
                                                        <summary>{{ Auth::user()->name }}</summary>

                                                        <a href="{{ route('logout') }}"
                                                            onclick="event.preventDefault();
                                                                          document.getElementById('logout-form').submit();">
                                                            {{ __('Logout') }}
                                                        </a>

                                                        <form id="logout-form" action="{{ route('logout') }}"
                                                            method="POST" class="d-none">
                                                            @csrf
                                                        </form>
                                                    </details>

                                                    <div class="dropdown dropd-hide-mob">
                                                        <button class=" dropdown-toggle" type="button"
                                                            id="dropdownMenuButton" data-toggle="dropdown"
                                                            aria-haspopup="true" aria-expanded="false">
                                                            {{ Auth::user()->name }}
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">


                                                            <a class="drop-hover" href="{{ route('logout') }}"
                                                                onclick="event.preventDefault();
                                                                          document.getElementById('logout-form').submit();">
                                                                {{ __('Logout') }}
                                                            </a>

                                                            <form id="logout-form" action="{{ route('logout') }}"
                                                                method="POST" class="d-none">
                                                                @csrf
                                                            </form>


                                                        </div>
                                                    </div>
                                                </li>
                                            @endauth
                                            <li class="cl-han" style="cursor: pointer">
                                                <a
                                                    onClick="document.getElementById('lang').submit();">{{ \App::getLocale() == 'en' ? 'عربي' : 'EN' }}</a>
                                                <form id="lang" method="POST" action="{{ route('lang') }}">@csrf
                                                </form>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>


                        <!-- Mobile Menu -->
                        <div class="col-12 p-0">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->
    </header>
    @yield('content')


    <div class="reg-section">

        <div class="custom-container reg-form">
            <div classs="relative">
                <div class="absolute leave">
                    <img src="{{ asset('assets/img/anime/leave.png') }}" alt="">
                </div>
                <div class="absolute circle fade-in-text">
                    <img src="{{ asset('assets/img/anime/circle.svg') }}" alt="">
                </div>
                <div class="absolute downwardC blink">
                    <img src="{{ asset('assets/img/anime/downwardC.svg') }}" alt="">
                </div>
            </div>

            <div class="row">
            <div class="col-md-12 ">
                <h2 class="reg-heading mb-0 text-center">{{__('Successfully Verified')}}</h2>
                {{-- <p class="line-first">{{('Please verify your registration by entering your Email or Phone Number')}}</p> --}}
                <p></p>
            </div>
            </div>

            @if($differentiatevarriable[0] == "qrcodebyphonenumber")
            <img src="{{ asset('storage/qr-code/'.$qrcodebyphonenumber->qrcodeid.'.png') }}" alt=""
                style="display: block; margin: auto;width: 200px; margin-top: 20px; max-width: 100%;">
            @else
            <img src="{{ asset('storage/qr-code/'.$qrcodebyemail->qrcodeid.'.png') }}" alt=""
                style="display: block; margin: auto;width: 200px; margin-top: 20px; max-width: 100%;">
@endif

            {{-- <img src="{{ asset('assets/img/QR.png') }}" alt="" style="display: block; margin: auto;width: 550px; max-width: 100%;"> --}}

            <br>
            <br>
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="form-sec">
                        <div class="header-right-btn f-right d-lg-block ml-30 submit-btn">

                            @if($differentiatevarriable[0] == "qrcodebyphonenumber")
                            <a href="{{ asset('storage/qr-code/'.$qrcodebyphonenumber->qrcodeid.'.png') }}" class="btn header-btn w-15 border-r reg-bbt" download>{{__('Download QR')}}</a>
                            @else
                            <a href="{{ asset('storage/qr-code/'.$qrcodebyemail->qrcodeid.'.png') }}" class="btn header-btn w-15 border-r reg-bbt" download>{{__('Download QR')}}</a>
                            @endif
                            {{-- <button type="submit" class="btn header-btn w-15 border-r reg-bbt" data-toggle="modal"
                                data-target="#exampleModalCenter">{{__('Send me a copy')}}</button> --}}
                           

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <footer style="position: relative; padding-top: 0;">
        <!-- Footer Start-->
        <div class="footer-area footer-padding">
            <div class="custom-container">
                <div class="row pb-4 no-rtl">
                    <div class="col-md-3">
                        <h6 class="text-white"> {{__('Phone')}} : +966 11 200 6677</h6>

                        <h6 class="text-white">{{__('Email')}} : info@greeningarabia.org</h6>

                        <h6 class="text-white">{{__('Address')}} : {{__('Riyadh, Kingdom of Saudi Arabia')}}</h6>

                    </div>
                    <div class="col-md-3 d-none-on-mbile">
                        <img class="center-logo foot-frst" src="{{asset('assets/img/v-logo.png')}}" alt="">
                    </div>
                    <div class="col-md-3 d-none-on-mbile">
                        <a target="_blank" href="https://ncvc.gov.sa/index.html"><img class="last-logo"
                                src="{{asset('assets/img/new-image/logo1-3.png')}}" alt="" width="220"></a>
                    </div>
                    <div class="col-md-3 d-none-on-mbile">
                        <a target="_blank" href="https://www.mewa.gov.sa/ar/Pages/default.aspx"><img class="mini-logo"
                                src="{{asset('assets/img/mini.png')}}" alt="" width="220"></a>
                    </div>

                </div>
                <div class="footer-mob-logo">

                    <img class="center-logo-mob-1" src="{{asset('assets/img/v-logo.png')}}" alt="" width="180">

                    <a target="_blank" href="https://ncvc.gov.sa/index.html"><img class="center-logo-mob-3"
                            src="{{asset('assets/img/LOGO10.png')}}" alt="" width="180"></a>

                    <a target="_blank" href="https://www.mewa.gov.sa/ar/Pages/default.aspx"><img
                            class="center-logo-mob-2" src="{{asset('assets/img/mini.png')}}" alt="" width="180"></a>

                </div>

            </div>
        </div>
        <!-- footer-bottom area -->
        <div class="footer-bottom-area footer-bg">
            <div class="container">
                <div class="footer-border">
                    <div class="row d-flex justify-content-between align-items-center">
                        <div class="col-xl-12 col-lg-12 ">
                            <div class="footer-copy-right">
                                <p>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    {{__('Copyright')}} &copy;
                                    <script>
                                        document.write(new Date().getFullYear());
                                    </script> {{__('All rights reserved |')}}
                                    <!-- This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a
                                        href="https://colorlib.com" target="_blank">Colorlib</a> -->
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                </p>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-4">
                            {{-- <div class="footer-social f-right">
                                <a href=""><i class="fab fa-twitter"></i></a>
                                <a href=""><i class="fab fa-facebook-f"></i></a>
                                <a href=""><i class="fas fa-globe"></i></a>
                                <a href=""><i class="fab fa-behance"></i></a>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer End-->
        <div id="back-top">
            <a title="Go to Top" href="#"> <i class="fas fa-level-up-alt"></i></a>
        </div>
    </footer>
    <!-- Scroll Up -->

    {{-- @guest

    <div id="back-top-2">
        <a title="Go to Top" href="{{route('register')}}">{{__('Registration')}}</a>
    </div>

    @endguest --}}

    <!-- JS here -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <script src="{{asset('assets/js/vendor/modernizr-3.5.0.min.js')}}"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="{{asset('assets/js/vendor/jquery-1.12.4.min.js')}}"></script>
    <script src="{{asset('assets/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- Jquery Mobile Menu -->
    <script src="{{asset('assets/js/jquery.slicknav.min.js')}}"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('assets/js/slick.min.js')}}"></script>
    <!-- One Page, Animated-HeadLin -->
    <script src="{{asset('assets/js/wow.min.js')}}"></script>
    <script src="{{asset('assets/js/animated.headline.js')}}"></script>
    <script src="{{asset('assets/js/jquery.magnific-popup.js')}}"></script>

    <!-------------------------Toastr message-------------------------------->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <!-- Date Picker -->
    <script src="{{asset('assets/js/gijgo.min.js')}}"></script>
    <!-- Nice-select, sticky -->
    <script src="{{asset('assets/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.sticky.js')}}"></script>

    <!-- counter , waypoint -->
    <script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('assets/js/waypoints.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>
    <!-- contact js -->
    <script src="{{asset('assets/js/contact.js')}}"></script>
    <script src="{{asset('assets/js/jquery.form.js')}}"></script>
    <script src="{{asset('assets/js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/js/mail-script.js')}}"></script>
    <script src="{{asset('assets/js/jquery.ajaxchimp.min.js')}}"></script>
    <script src="{{asset('assets/js/lozad.min.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.10.4/gsap.min.js"></script>

    <!-- Jquery Plugins, main Jquery -->
    <script src="{{asset('assets/js/plugins.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>

    <script>
        var countDownDate = new Date("May 29, 2022 9:00:00").getTime();
        // var countDownDate = new Date("March 10, 2022 12:37:25").getTime();

// Update the count down every 1 second
const lang = "{{ app()->getLocale() }}"
var x = setInterval(function () {

    // Get today's date and time
    var now = new Date().getTime();

    // Find the distance between now and the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = (("0" + Math.floor(distance / (1000 * 60 * 60 * 24))).slice(-2)).split("");
    var hours = (("0" + Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))).slice(-2)).split("");
    var minutes = (("0" + Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))).slice(-2)).split("");
    var seconds = (("0" + Math.floor((distance % (1000 * 60)) / 1000)).slice(-2)).split("");

    // Output the result in an element with id="demo"
    document.getElementById("days").innerHTML = `<span class="${lang}-${days[0]}"></span><span class="${lang}-${days[1]}"></span>`;
    document.getElementById("hours").innerHTML = `<span class="${lang}-${hours[0]}"></span><span class="${lang}-${hours[1]}"></span>`;
    document.getElementById("minutes").innerHTML = `<span class="${lang}-${minutes[0]}"></span><span class="${lang}-${minutes[1]}"></span>`;
    document.getElementById("seconds").innerHTML = `<span class="${lang}-${seconds[0]}"></span><span class="${lang}-${seconds[1]}"></span>`;
    // If the count down is over, write some text
    // element.getElementsByClassName("aftercountdownbutton").style.display : 'none';


    if (distance < 0) {
        clearInterval(x);
        document.getElementById("countdown").innerHTML = "Expired";
        // element.getElementsByClassName("countdown").style.display : 'none';
        // element.getElementsByClassName("aftercountdownbutton").style.display : 'block';

        // document.getElementById("demo").innerHTML = `<iframe style="display: block;" width="100%" height="450px;" src="https://www.youtube.com/watch?v=BLl32FvcdVM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
    }
}, 1000);

    </script>

    <script>
        function openCity(evt, cityName) {
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }
      tablinks = document.getElementsByClassName("tablinks");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
      }
      document.getElementById(cityName).style.display = "block";
      evt.currentTarget.className += " active";
    }
    // Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
    </script>
    <script>
        $("#dd").click(function () {
        $("#navigation").css("display","none !important")
        // $("#").show();
    });
    </script>
    <script>
        //           function myFunction() {
//     var x = document.getElementsByClassName("slicknav_nav");
//     if (x.style.display === "none") {
//     //   x.style.display = "block";
//     } else {
//       x.style.display = "none";
//     }
//   }
  function myFunction(){
  var elms = document.getElementsByClassName("slicknav_nav");

  Array.from(elms).forEach((x) => {
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  })
}
    </script>
    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="line-first" style="color: #006b52 !important;" id="exampleModalLongTitle">{{(' Send me QR copy')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span style="    font-size: 30px;" aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                        <div class="form-sec1">

                            <div class="col-md-12">
                                <label style="    color: #707070;
                                padding-left: 13px;
                                margin-top: 3px;
                                position: relative;" for="">{{__('Enter Email Address you want to receive the copy on')}} 
                                {{-- <span style="color: red;">*</span> --}}
                                </label>
                                @if($differentiatevarriable[0] == "qrcodebyphonenumber")
                                <input style="    width: 100%;
                                padding: 10px 0px;
                                border: solid 1px #707070;
                                padding-left: 10px;
                                padding-right: 15px;
                                border-radius: 14px;
                                background: transparent;
                                color: black;
                                margin-bottom: 17px;" name="email" class="@error('email') is-invalid @enderror"
                                    value="{{$qrcodebyphonenumber->email}}" placeholder="{{ \App::getLocale() == " en"
                                    ?"Write your answer here...": "اكتب إجابتك هنا ..." }}" type="email"
                                    id="emailtextbox">
                                <input type="hidden" id="hiddenqrcodetextbox"
                                    value="{{$qrcodebyphonenumber->qrcodeid}}">
                                @else
                                <input name="email" class="@error('email') is-invalid @enderror"
                                    value="{{$qrcodebyemail->email}}" placeholder="{{ \App::getLocale() == " en" ?"Write
                                    your answer here...": "اكتب إجابتك هنا ..." }}" type="email" id="emailtextbox">
                                <input type="hidden" id="hiddenqrcodetextbox" value="{{$qrcodebyemail->qrcodeid}}">

                                @endif

                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <a id="sendcopybtnofmodal" type="button" class="btn header-btn w-100 border-r reg-bbt" download style="width: 35% !important;
                    color: white;">Send Copy</a>
                    {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</b  utton> --}}
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).on('click','#sendcopybtnofmodal',function(){

        var emailofmodal = document.getElementById('emailtextbox').value;
        var qrcodehiddenfield = document.getElementById('hiddenqrcodetextbox').value;

        
        // alert(qrcodehiddenfield);
        $.ajax({
            url: "{{route('admin.sendemailqrcopy')}}",
            type: "POST",
            data: {
                "_token": "{{ csrf_token() }}",
                emailofmodal:emailofmodal,
                qrcodehiddenfield:qrcodehiddenfield,
               
            },
            success: function(response) {
               
                    // alert(response.message);
                    $('#exampleModalCenter').modal('hide');
               
                toastr.success(response.message);
            },

        });
    });
    toastr.options = {
          "closeButton": true,
          "newestOnTop": true,
          "positionClass": "toast-top-right"
        };
    </script>



</body>

</html>