@extends('layouts.app')
@section('content')
<main class="home-main">
    <div class="home-banner">
        <img class="banner-round" src="{{ asset('assets/img/banner-icon/banner-round.png') }}" alt="">
        <img class="leave-1" src="{{ asset('assets/img/banner-icon/live-leave.png') }}" alt="">
        <img class="leave-2" src="{{ asset('assets/img/banner-icon/leave-right.png') }}" alt="">
        <img class="leave-3" src="{{ asset('assets/img/banner-icon/banner-dbl.png') }}" alt="">
        <img class="leave-4" src="{{ asset('assets/img/banner-icon/single.png') }}" alt="">
        <img class="leave-5" src="{{ asset('assets/img/banner-icon/banner-bubble.png') }}" alt="">
        <div class="custom-container">
            <div class="row">
                <div class="col-md-5">
                    <img src="{{ asset('assets/img/new-image/banner-left.png') }}" alt="">
                </div>
                <div class="col-md-7">
                    <h6 class="en-tit">

                        {{__('Under the patronage of His Royal Highness Prince Mohammed bin Salman bin Abdulaziz, Crown Prince, Deputy Prime Minister and Minister of Defense')}}
                    </h6>
                    <h6 class="ar-tit">
                        "{{__('The Saudi Green Initiative will include a number of ambitious initiatives, one of the most notable is the planting of 10 billion trees in the Kingdom of Saudi Arabia over the coming decades, equivalent to the rehabilitation of about 40 million hectares of degraded land, which means an increase by twelvefold the area covered by existing trees.')}}"
                    </h6>
                    <h1 class="new-tit">
                        {{__('Mohammed bin Salman bin Abdulaziz Al Saud')}}
                    </h1>
                </div>
            </div>
        </div>
    </div>


    <div class="exhibition-sec-new">
        <img class="exhibition-1" src="{{ asset('assets/img/banner-icon/exhi-bubble.png') }}" alt="">
        <img class="exhibition-2" src="{{ asset('assets/img/banner-icon/exhi-2.png') }}" alt="">
        <img class="exhibition-3" src="{{ asset('assets/img/banner-icon/exhi-3.png') }}" alt="">
        <img class="exhibition-4" src="{{ asset('assets/img/banner-icon/exhi-4.png') }}" alt="">
        <div class="custom-container">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="sec-sectoion-head">
                        {{__('International Exhibition & Forum On Afforestation Technologies')}}
                    </h1>
                </div>
                <div class="col-md-6">
                    <figure style="background-image: url('{{ asset('assets/img/new-image/exhi-bg.png') }}')"></figure>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-6">
                    <a href="{{route('register')}}" class="reg-btn">{{__('Registration')}}</a>
                </div>
                <div class="col-md-6 col-sm-6 col-6">
                    <h4>{{__('Remaining Time to Register')}}</h4>
                    <div class="countdown">
                        <div class="container-day child first-child">
                            <h3 class="time-dis">{{ __('Days') }} </h3>
                            <h3 id="days">Time</h3>
                        </div>
                        <div class="container-hour child">
                            <h3 class="time-dis"> {{ __('Hours') }} </h3>
                            <h3 id="hours">Time</h3>
                        </div>
                        {{-- <div class="container-minute child">
                            <h3 class="time-dis">{{ __('Minutes') }}</h3>
                            <h3 id="minutes">Time</h3>
                        </div>
                        <div class="container-second child-last">
                            <h3 class="time-dis">{{ __('Seconds') }}</h3>
                            <h3 id="seconds">Time</h3>
                        </div> --}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h1>
                        {{__('Introduction')}}
                    </h1>
                    <p>
                        {{__('The National Center for the Vegetation Cover Development and Combating Desertification with the Ministry of Environment, Water and Agriculture is organizing the International Exhibition & Forum On Afforestation Technologies in the city of Riyadh, based on its role and tasks in developing, Protecting, and sustaining vegetation cover, rehabilitating degraded sites, and achieving sustainable management in pastures and national parks, within the framework of Achieving the objectives of the Kingdom’s Vision 2030, the Green Saudi Initiatives and the Green Middle East, and the promising investment opportunities and innovative research that will result from them, which will contribute to the development of vegetation cover and combating desertification in Saudi Arabia and the Middle East')}}
                    </p>
                    <br>
                    <br>
                    <h1 class="d-none-in-en">
                        {{__('introduction')}}
                    </h1>
                    <p class="d-none-in-en">
                        {{__('A')}}
                    </p>
                    <p class="d-none-in-en mt-0">
                        {{__('B')}}
                    </p>
                    <p class="d-none-in-en mt-0">
                        {{__('C')}}
                    </p>
                    <p class="d-none-in-en mt-0">
                        {{__('D')}}
                    </p>

                </div>
            </div>
        </div>
    </div>

    <div class="contribute-sec contribute-sec-mob">
        <img class="contribute-1" src="{{ asset('assets/img/banner-icon/live-leave.png') }}" alt="">
        <img class="contribute-2" src="{{ asset('assets/img/banner-icon/banner-dbl.png') }}" alt="">
        <div class="custom-container">
            <div class="row">
                <div class="col-md-4">
                    <h1>{{__('Exhibition & Forum Vision')}}</h1>
                </div>
                <div class="col-md-8">
                    <p>{{__('The Kingdom plays a major role in contributing to the support of vegetation cover with many initiatives and the preservation of afforestation, combating desertification and sustainability, as the National Center for the Vegetation Cover Development organizes the International Exhibition & Forum On Afforestation Technologies with the participation of local and international companies, experts and those interested in the environmental field and climate issues, and a review of all developments and the latest findings. The world of technology and innovation in the field of combating desertification, mitigating its effects, developing and protecting vegetation cover, and the experiences and research they have reached, in turn, aim to achieve a significant impact on climate preservation.')}}</p>
                </div>
            </div>
        </div>
    </div>

    <div class="exhibi-slider dots-n-arrows-6">
        <div class="exhibi-item">
            <figure style="background-image: url('{{ asset('assets/img/new-image/exhi-1.png') }}')">

            </figure>
        </div>

        <div class="exhibi-item">
            <figure style="background-image: url('{{ asset('assets/img/new-image/exhi-2.png') }}')">

            </figure>
        </div>

        <div class="exhibi-item">
            <figure style="background-image: url('{{ asset('assets/img/new-image/exhi-3.png') }}')">

            </figure>
        </div>

        <div class="exhibi-item">
            <figure style="background-image: url('{{ asset('assets/img/new-image/exhi-4.png') }}')">

            </figure>
        </div>

        <div class="exhibi-item">
            <figure style="background-image: url('{{ asset('assets/img/new-image/exhi-5.png') }}')">

            </figure>
        </div>

    </div>

    <div class="ven-sec-new ven-sec-new-mob">
        <img class="ven-sec-new-1" src="{{ asset('assets/img/banner-icon/exhi-bubble.png') }}" alt="">
        <img class="ven-sec-new-2" src="{{ asset('assets/img/banner-icon/venue-2.png') }}" alt="">
        <img class="ven-sec-new-3" src="{{ asset('assets/img/banner-icon/brown-circle.png') }}" alt="">
        <img class="ven-sec-new-4" src="{{ asset('assets/img/banner-icon/venue-4.png') }}" alt="">
        <img class="ven-sec-new-5" src="{{ asset('assets/img/banner-icon/venue-5.png') }}" alt="">
        <div class="custom-container">
            <div class="row">
                <div class="col-md-5">
                    <h1>
                        {{__('Exhibition Times')}}
                    </h1>
                    {{-- <p>
                        {{__('The exhibition will be held on the grounds of the Riyadh international convention &
                        exhibition center at the main showroom and the external showroom, and the exhibition will be
                        divided based on the main axes of the exhibition and allocating sections and pavilions to
                        partners, sponsors, and participating parties on the activity of each side:')}}
                    </p> --}}
                    <h6 class="vn-new-1"> <img src="{{ asset('assets/img/new-image/cal.png') }}" alt=""> {{__('29th  - 31st  May 2022')}}</h6>
                    <h6 class="vn-new-2"> <img src="{{ asset('assets/img/new-image/mar.png') }}" alt=""> {{__('Riyadh International Convention & Exhibition Center')}}</h6>
                </div>
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="venue-item">
                                <h6>{{__('Forum Time')}}</h6>
                                <p>{{__('From 09:00 AM to 03:00 PM')}}</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="venue-item">
                                <h6>{{__('Exhibition Time')}}</h6>
                                <p>{{__('From 12:00 PM to 08:00 PM')}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="axes-sec axes-sec-mob">
        <img class="axes-sec-1" src="{{ asset('assets/img/banner-icon/axes-1.png') }}" alt="">
        <img class="axes-sec-2" src="{{ asset('assets/img/banner-icon/live-leave.png') }}" alt="">
        <img class="axes-sec-3" src="{{ asset('assets/img/banner-icon/brown-circle.png') }}" alt="">
        <img class="axes-sec-4" src="{{ asset('assets/img/banner-icon/banner-dbl.png') }}" alt="">
        <div class="custom-container">
            <div class="row">
                <div class="col-md-5">
                    <h1>
                        {{__('The Axes Of The Exhibition & Forum and Their Fields')}}
                    </h1>
                </div>
                <div class="col-md-7">
                    <p>{{__('The International Exhibition & Forum On Afforestation Technologies includes a set of comprehensive and integrated axes regarding afforestation and its Technologies and combating desertification, on the following:')}}</p>
                </div>
            </div>
            <div class="axes-row">
                <div class="axes-item">
                    <img src="{{ asset('assets/img/new-image/axe-icon-1.png') }}" alt="">
                    <p>{{__('Water Resources and Technologies')}}</p>
                </div>
                <div class="axes-item">
                    <img src="{{ asset('assets/img/new-image/axe-icon-2.png') }}" alt="">
                    <p>{{__('Nurseries & seeds')}}</p>
                </div>
                <div class="axes-item">
                    <img src="{{ asset('assets/img/new-image/axe-icon-3.png') }}" alt="">
                    <p>{{__('Waste Recycling')}}</p>
                </div>
                <div class="axes-item">
                    <img src="{{ asset('assets/img/new-image/axe-icon-4.png') }}" alt="">
                    <p>{{__('Afforestation Technologies')}}</p>
                </div>
                <div class="axes-item">
                    <img src="{{ asset('assets/img/new-image/axe-icon-5.png') }}" alt="">
                    <p>{{__('Lands & Combating Desertification')}}</p>
                </div>
                <div class="axes-item">
                    <img src="{{ asset('assets/img/new-image/axe-icon-2.png') }}" alt="">
                    <p>{{__('Rehabilitation of degraded land and combating desertification Environmental solutions in plant carbon storage')}}</p>
                </div>
                <div class="axes-item">
                    <img src="{{ asset('assets/img/new-image/axe-icon-6.png') }}" alt="">
                    <p>{{__('Irrigation Technologies')}}</p>
                </div>
                <div class="axes-item">
                    <img src="{{ asset('assets/img/new-image/axe-icon-7.png') }}" alt="">
                    <p>{{__('Forest management and development')}}</p>
                </div>
                <div class="axes-item">
                    <img src="{{ asset('assets/img/new-image/axe-icon-8.png') }}" alt="">
                    <p>{{__('Pest Control')}}</p>
                </div>

            </div>
        </div>
    </div>


    <div class="speakers-section speakers-section-mob">
        <img class="speakers-section-1" src="{{ asset('assets/img/banner-icon/speaker-1.png') }}" alt="">
        <div class="custom-container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-6">
                    <h1 class="head">{{__('Speakers')}}</h1>
                </div>
                <div class="col-md-6 col-sm-6 col-6">
                    <a href="{{route('speakers')}}">{{__('See ALL')}} <img class="see-all-arrow rtl-arrow"
                            src="{{ asset('assets/img/new-image/right-arrow.png') }}" alt=""> </a>
                </div>
            </div>
        </div>
        <div class="speaker-container">
            <div class="speaker-slider dots-n-arrows-7">
                @foreach ($speakers as $speakers)
                <div class="speaker-item speaker-item-home">
                    <figure
                        style="background-image: url('{{ asset($speakers->image) }}');height: 300px; background-size: cover; ">

                    </figure>
                    <h4>{{ \App::getLocale() == 'en' ? $speakers->en->name : $speakers->ar->name }}</h4>
                    <p>{{ \App::getLocale() == 'en' ? $speakers->en->title : $speakers->ar->title }}</p>
                </div>

                @endforeach




            </div>
        </div>



    </div>



    <div class="speakers-section agenda-new speakers-section-mob">
        <div class="custom-container seesion" style="margin-top: 0">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-6">
                    <h1 class="head">{{__('Agenda')}}</h1>
                </div>
                <div class="col-md-4 col-sm-6 col-6">
                    <h1 class="head-2">{{__('Certificates of attendance are available')}}</h1>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <a href="{{route('agenda')}}">{{__('See ALL details')}} <img class="see-all-arrow rtl-arrow"
                            src="{{ asset('assets/img/new-image/right-arrow.png') }}" alt=""></a>
                </div>
            </div>

            <div class="schedule-tab">
                <ul class="nav nav-tabs " role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab" aria-selected="false">
                            <h5>{{__('First Day')}}</h5>
                            <p>{{__('Sunday, 29th May 2022')}}</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab" aria-selected="false">
                            <h5>{{__('Second Day')}}</h5>
                            <p>{{__('Monday, 30th May 2022')}}</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link  show" data-toggle="tab" href="#tabs-3" role="tab" aria-selected="true">
                            <h5>{{__('Third Day')}}</h5>
                            <p>{{__('Tuesday, 31st May 2022')}}</p>
                        </a>
                    </li>

                </ul>

                <div class="tab-content">
                    <div class="tab-pane  active show" id="tabs-1" role="tabpanel">
                        <div class="new-agent-header-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <p>{{__('Time')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <p>{{__('Title')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>{{__('Speakers')}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="new-agent-child-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:00-10:00')}}</p>
                                </div>
                                <div class="col-md-7">
                                    <h6>{{__('Opening Ceremony')}}</h6>
                                    <ul>
                                        <li>
                                            {{__('The National Anthem of the Kingdom of Saudi Arabia')}}
                                        </li>
                                        <li>
                                            {{__('Quran')}}
                                        </li>
                                        <li>
                                            {{__('Organizing Committee Speech by Dr. Khaled Al-Abdulkader, CEO of the National Center for Vegetation Cover and Combating Desertification')}}
                                        </li>
                                        <li>
                                            {{__('Opening Speech by his Excellency Eng. Abdulrahman AlFadhli, Minister of Environment, Water & Agriculture')}}
                                        </li>
                                        <li>
                                            {{__('Professor. Mr. Lee White, the Ministry of Environment (Gabonese Republic)')}}
                                        </li>
                                        <li>
                                            {{__('Mr. Abdulrahim Houmy, Acting Director general of the National Agency for Water and Forests (Morocco)')}}
                                        </li>
                                        <li>
                                            {{__('Ibrahim Thiaw, UNCCD Executive Secretary and Deputy Executive Director of United Nations Environment Program.')}}
                                        </li>
                                        <li>
                                            {{__('Li Taian, Director of Division of Asia and Africa Affairs, Foreign Affairs Office of the Peoples Government of Gansu China')}}
                                        </li>

                                    </ul>
                                </div>
                                <div class="col-md-3">
                                </div>
                            </div>
                        </div>
                        <h1 class="breaking-heading">{{__('Sponsors Honoring')}}</h1>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:00-10:45')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Exhibition Official Opening')}}</h6>

                                </div>
                                <div class="col-md-4">
                                </div>
                            </div>
                        </div>
                        <h1 class="breaking-heading">{{__('First Session A')}}</h1>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/agenda-1.jpeg  ') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:45-12:00')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6></h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman: Dr. Osama I. Faqeeha')}}</h6>
                                    <p class="sb-title">{{__('Deputy Minister for Environment')}}</p>

                                    <h6>{{__('Appointed: Dr. Ibrahim Aldujain')}}</h6>
                                    <p class="sb-title">{{__('Adviser at Ministry of Municipal Rural Affairs and Housing')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:45 - 11:05')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Environmental Initiatives within The Saudi Green Initiative')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Osama I. Faqeeha')}}</h6>
                                    <p class="sb-title">{{__('Deputy Minister for Environment')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:05 - 11:20')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Strategy and National Forestry Sustainable Plan in Saudi Arabia')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Khaled Al-Abdulkader')}}</h6>
                                    <p class="sb-title">{{__('CEO of the National Center for Vegetation Development and Combating Desertification')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:20 - 11:35')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Global Initiative on Reducing Land Degradation and Enhancing Conservation of Terrestrial Habitats')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Muralee Thummarukudy')}}</h6>
                                    <p class="sb-title">{{__('UNCCD')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:35-11:50')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Governance, possibilities and success factors of afforestation in some countries')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Andrea Kutter')}}</h6>
                                    <p class="sb-title">{{__('The World Bank')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:50 – 12:00')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Discussion')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Andrea Kutter')}}</h6>
                                    <p class="sb-title">{{__('The World Bank')}}</p> --}}
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:00 -12:30')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Dhohr Prayer')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Andrea Kutter')}}</h6>
                                    <p class="sb-title">{{__('The World Bank')}}</p> --}}
                                </div>
                            </div>
                        </div>
                        <h1 class="breaking-heading">{{__('Second Session A')}}</h1>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:30-1:45')}}</p>
                                </div>
                                <div class="col-md-6">
                                    {{-- <h6>{{__('Discussion')}}</h6> --}}

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman: MohammedQurban')}}</h6>
                                    <p class="sb-title">{{__('CEO of the National Centre for Wildlife Development')}}
                                    </p>

                                    <h6>{{__('Appointed: Dr. Ahmed Alghamdi')}}</h6>
                                    <p class="sb-title">{{__('National Center for Vegetation Development and Combating Desertification')}}</p>

                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:30 - 12:50')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Impact of Livestock grazing management and changing the animal behavior on vegetation cover')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Tibbo Markos')}}</h6>
                                    <p class="sb-title">{{__('FAO')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:50 - 1:05')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Sustainable management of Rangelands in Kingdom of Saudi Aarabia')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Abdullah Alhajoj')}}</h6>
                                    <p class="sb-title">{{__('National Center for Vegetation Development and Combating Desertification')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:05 - 1:20')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Sustainable management of Rangelands in Kingdom of Saudi Aarabia')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Mounir Louhaichi')}}</h6>
                                    <p class="sb-title">{{__('ICARDA')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:20 - 1:35')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('The role of nature reserves and protected areas in the development of vegetation cover')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Abdullah Altalasat')}}</h6>
                                    <p class="sb-title">{{__('National Centre for Wildlife Development')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:35 – 1:45')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Discussion')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Abdullah Altalasat')}}</h6>
                                    <p class="sb-title">{{__('National Centre for Wildlife Development')}}</p> --}}
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            {{-- <img src="{{ asset('assets/img/new-image/user.png') }}" alt=""> --}}

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:45 - 2:00')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Coffee Break')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Abdullah Altalasat')}}</h6>
                                    <p class="sb-title">{{__('National Centre for Wildlife Development')}}</p> --}}
                                </div>
                            </div>
                        </div>
                        <h1 class="breaking-heading">{{__('Third Session B')}}</h1>

                        <div class="new-agent-child-row">
                            {{-- <img src="{{ asset('assets/img/new-image/user.png') }}" alt=""> --}}

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:30-1:45')}}</p>
                                </div>
                                <div class="col-md-6">
                                    {{-- <h6>{{__(' Coffee Break')}}</h6> --}}

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman:Eng. Ali Alghamdi')}}</h6>
                                    <p class="sb-title">{{__('CEO of the National Center for Environment Compliance')}}
                                    </p>

                                    <h6>{{__('Appointed: Dr. Arwa Alhuqail')}}</h6>
                                    <p class="sb-title">{{__('Princess Nourah Bint Abdul Rahman University')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:30 - 12:50')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Carbon sequestration and Storage in mangroves (Benchmarking)')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. CarlosM. Duarte')}}</h6>
                                    <p class="sb-title">{{__('King Abdullah University forScience and Technology')}}</p>

                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:50 - 1:05')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Carbon storage in trees and shrubs dominant in the Arabian Peninsula')}}
                                    </h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__(' Massaoudou Moussa')}}</h6>
                                    <p class="sb-title">{{__('Nagger')}}</p>

                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:05 - 1:20')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Techniques for estimating the efficiency of plants in carbon fixation')}}
                                    </h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Mohmed Elsheikh')}}</h6>
                                    <p class="sb-title">{{__('King Saud University')}}</p>

                                </div>
                            </div>
                        </div>



                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:20 - 1:35')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Afforestation as nature-based solution to reduce heatwaves and floods')}}
                                    </h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Paulo Pereira')}}</h6>
                                    <p class="sb-title">{{__('Mykolas Romeris University, Lithuania')}}</p>

                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:35 – 1:45')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Discussion')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Paulo Pereira')}}</h6>
                                    <p class="sb-title">{{__('Mykolas Romeris University, Lithuania')}}</p> --}}

                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:45 – 2:00')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Coffee Break')}}</h6>

                                </div>
                                <div class="col-md-4">
                                </div>
                            </div>
                        </div>
                        <h1 class="breaking-heading">{{__('First Panel Discussion Room: A')}} {{__('')}}</h1>
                        <h1 class="breaking-heading">{{__('The role of the private sector, the non-profit organizations, and the charitable societies in vegetation cover development')}}</h1>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('2:00 – 3:15')}}</p>
                                </div>
                                <div class="col-md-6">
                                    {{-- <h6>{{__('Coffee Break')}}</h6> --}}

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman: Mr. Ahmed Saleh Hariri')}}</h6>
                                    <p class="sb-title">{{__('Consultant at Ministry of Environment, Water and
                                        Agriculture')}}</p>

                                    <h6>{{__('Appointed: Dr. Haifa Alolayan')}}</h6>
                                    <p class="sb-title">{{__('Advisor atMinistry of Environment, Water and Agriculture
                                        of Saudi Arabi')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-header-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <p>{{__('Time')}}</p>
                                </div>
                                <div class="col-md-3">
                                    <p>{{__('Speaker')}}</p>
                                </div>
                                <div class="col-md-3">
                                    <p>{{__('Participants and Guests')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>{{__('Supervision and organization')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('2:00 – 2:10')}}</p>
                                </div>
                                <div class="col-md-3">
                                    <h6>{{__('Dr. Khaled Mreziq Al-Harbi, Director of Innovation and Customer Experience
                                        Department')}}</h6>

                                </div>
                                <div class="col-md-3">
                                    <h6>{{__('Local Banks - Environment Fund + The World Bank WWF Agricultural companies and major companies(SABIC, Aramco, Red Sea, Qiddiya and NEOM)')}}</h6>

                                </div>

                                <div class="col-md-4">
                                    <h6>{{__('Supervisor Dr. Dr. Haifa Alolayan')}}</h6>
                                    <h6>{{__('Ms. Shahd Aldurman')}}</h6>
                                    <h6>{{__('Ms. Safa Alsalamah')}}</h6>

                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('2:10 – 2:20')}}</p>
                                </div>
                                <div class="col-md-3">
                                    <h6>{{__('Eng./ Abdulrahman bin Suleiman Abdullah Al - Rashoud, Director General of Quality and Development Programs')}}</h6>

                                </div>
                                <div class="col-md-3">
                                    <h6>{{__('Local Banks - Environment Fund + The World Bank WWF Agricultural companies and major companies(SABIC, Aramco, Red Sea, Qiddiya and NEOM)')}}</h6>

                                </div>

                                <div class="col-md-4">
                                    <h6>{{__('Supervisor Dr. Dr. Haifa Alolayan')}}</h6>
                                    <h6>{{__('Ms. Shahd Aldurman')}}</h6>
                                    <h6>{{__('Ms. Safa Alsalamah')}}</h6>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('2:20 – 2:30')}}</p>
                                </div>
                                <div class="col-md-3">
                                    <h6>{{__('Eng. Ahmed Abdullah Alotaibi, Green Department and Environmental Policy (Aramco)')}}</h6>

                                </div>
                                <div class="col-md-3">
                                    <h6>{{__('Local Banks - Environment Fund + The World Bank WWF Agricultural companies and major companies(SABIC, Aramco, Red Sea, Qiddiya and NEOM)')}}</h6>

                                </div>

                                <div class="col-md-4">
                                    <h6>{{__('Supervisor Dr. Dr. Haifa Alolayan')}}</h6>
                                    <h6>{{__('Ms. Shahd Aldurman')}}</h6>
                                    <h6>{{__('Ms. Safa Alsalamah')}}</h6>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('2:30 - 2:40')}}</p>
                                </div>
                                <div class="col-md-3">
                                    <h6>{{__('Morne Fourie Roshn Real Estate')}}</h6>

                                </div>
                                <div class="col-md-3">
                                    <h6>{{__('Local Banks - Environment Fund + The World Bank WWF Agricultural companies and major companies(SABIC, Aramco, Red Sea, Qiddiya and NEOM)')}}</h6>

                                </div>

                                <div class="col-md-4">
                                    <h6>{{__('Supervisor Dr. Dr. Haifa Alolayan')}}</h6>
                                    <h6>{{__('Ms. Shahd Aldurman')}}</h6>
                                    <h6>{{__('Ms. Safa Alsalamah')}}</h6>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">


                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('2:40 – 3:15')}}</p>
                                </div>
                                <div class="col-md-3">
                                    <h6>{{__('Open Panel Discussion')}}</h6>

                                </div>
                                <div class="col-md-3">
                                    {{-- <h6>{{__('Local Banks - Environment Fund +The World Bank WWF Agricultural
                                        companies and major companies (SABIC, Aramco, Red Sea, Qiddiya and NEOM)')}}
                                    </h6> --}}

                                </div>

                                <div class="col-md-4">
                                    {{-- <h6>{{__('Supervisor Dr. Dr. Haifa Alolayan Ms. Shahd Aldurman Ms. Safa
                                        Alsalamah')}}</h6> --}}

                                </div>
                            </div>
                        </div>


                        <h1 class="breaking-heading">{{__('Second Panel Discussion')}} {{__('Room: B')}}</h1>
                        <h1 class="breaking-heading">{{__('The Modern techniques for the effective management of nurseries and seed banks to support vegetation development projects')}}</h1>



                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('2:00 – 3:15')}}</p>
                                </div>
                                <div class="col-md-6">
                                    {{-- <h6>{{__('Coffee Break')}}</h6> --}}

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman Dr. Khaled Aljarallah')}}</h6>
                                    <p class="sb-title">{{__('CEO of the National Center for the Prevention and Control of Plants Pests and Animal Diseases')}}</p>

                                    <h6>{{__('Appointed: Dr. Abdullah Alburaiki')}}</h6>
                                    <p class="sb-title">{{__('National Center for Vegetation Development and Combating Desertification.')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-header-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <p>{{__('Time')}}</p>
                                </div>
                                <div class="col-md-3">
                                    <p>{{__('Speaker')}}</p>
                                </div>
                                <div class="col-md-3">
                                    <p>{{__('Participants and Guests')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>{{__('Supervision and organization')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('2:00 – 2:10')}}</p>
                                </div>
                                <div class="col-md-3">
                                    <h6>{{__('M. Nasser Bu Dasta Executive Manager at Climate Technology')}}</h6>
                                    {{-- <h6>{{__('Dr. Naiemah Alenezy Hail University')}}</h6> --}}

                                </div>
                                <div class="col-md-3">
                                    <h6>{{__('Nurseries, seed dispersal companies and seed multiplication stations')}}
                                    </h6>

                                </div>

                                <div class="col-md-4">
                                    <h6>{{__('Supervisor Dr. Abdullah Alburaiki')}}</h6>
                                    <h6>{{__('Ms. Rawan Al-Qahtani')}}</h6>
                                    <h6>{{__('Ms. Amira Al-Qahtani')}}</h6>
                                    <h6>{{__('Ms. Reham Al-Mutairi')}}</h6>

                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('2:00 – 2:20')}}</p>
                                </div>
                                <div class="col-md-3">
                                    <h6>{{__('Dr. Naiemah Alenezy Hail University')}}</h6>

                                </div>
                                <div class="col-md-3">
                                    <h6>{{__('Nurseries, seed dispersal companies and seed multiplication stations')}}
                                    </h6>

                                </div>

                                <div class="col-md-4">
                                    <h6>{{__('Supervisor Dr. Abdullah Alburaiki')}}</h6>
                                    <h6>{{__('Ms. Rawan Al-Qahtani')}}</h6>
                                    <h6>{{__('Ms. Amira Al-Qahtani')}}</h6>
                                    <h6>{{__('Ms. Reham Al-Mutairi')}}</h6>

                                </div>
                            </div>
                        </div>



                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('2:20 – 2:30')}}</p>
                                </div>
                                <div class="col-md-3">
                                    <h6>{{__('Bruce Pederson Mawarid Holding Investment')}}</h6>
                                    {{-- <h6>{{__('Mawarid Holding Investment')}}</h6> --}}

                                </div>
                                <div class="col-md-3">
                                    <h6>{{__('Resources Investment Holding')}}</h6>
                                </div>

                                <div class="col-md-4">

                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('2:30 – 3:15')}}</p>
                                </div>
                                <div class="col-md-3">
                                    <h6>{{__('Open Panel Discussion')}}</h6>
                                    {{-- <h6>{{__('Mawarid Holding Investment')}}</h6> --}}

                                </div>
                                <div class="col-md-3">

                                </div>

                                <div class="col-md-4">

                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('3:15 – 4:00')}}</p>
                                </div>
                                <div class="col-md-3">
                                    <h6>{{__('Lunch Break')}}</h6>
                                    {{-- <h6>{{__('Mawarid Holding Investment')}}</h6> --}}

                                </div>
                                <div class="col-md-3">

                                </div>

                                <div class="col-md-4">

                                </div>
                            </div>
                        </div>



                    </div>
                    <div class="tab-pane " id="tabs-2" role="tabpanel">
                        <br>
                        <h1 class="breaking-heading">{{__('First Session A')}}</h1>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/agenda-3.jpeg') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:15 – 10:30')}}</p>
                                </div>
                                <div class="col-md-6">

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman: Dr. Khaled Al-Abdulkader')}}</h6>
                                    <p class="sb-title">{{__('CEO of the National Center for Vegetation Development & Combating Desertification')}}</p>

                                    <h6>{{__('Appointed:Eng. Sameer Malaika')}}</h6>
                                    <p class="sb-title">{{__('National Center for Vegetation & Development Combating Desertification')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-header-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <p>{{__('Time')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <p>{{__('Title')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>{{__('Speakers')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:15 - 9:35')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Rehabilitation of Forests and natural plant communities')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Shahid Rashid Awan')}}</h6>
                                    <p class="sb-title">{{__('Pakistan')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:35 - 9:50')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Rehabilitation of Forests and natural plant communities')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Rahma Aljeraisy')}}</h6>
                                    <p class="sb-title">{{__('King Khalid University')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:50 – 10:05')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Using modern technologies in monitoring and developing of vegetation
                                        cover')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Ms.Elsy Ibrahim')}}</h6>
                                    <p class="sb-title">{{__('Belgium')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:05 – 10:20')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Post-fire Management in the climate context')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Paulo Pereira')}}</h6>
                                    <p class="sb-title">{{__('Mykolas Romeris University, Lithuania')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            {{-- <img src="{{ asset('assets/img/new-image/user.png') }}" alt=""> --}}
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:20 - 10:30')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Discussion')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Paulo Pereira')}}</h6>
                                    <p class="sb-title">{{__('Mykolas Romeris University, Lithuania')}}</p> --}}
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            {{-- <img src="{{ asset('assets/img/new-image/user.png') }}" alt=""> --}}
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:30 - 10:45')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Coffee Break')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Paulo Pereira')}}</h6>
                                    <p class="sb-title">{{__('Mykolas Romeris University, Lithuania')}}</p> --}}
                                </div>
                            </div>
                        </div>

                        <h1 class="breaking-heading">{{__('Second Session B')}}</h1>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:15 – 10:30')}}</p>
                                </div>
                                <div class="col-md-6">

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman: Dr. Hamad Albatshan')}}</h6>
                                    <p class="sb-title">{{__('Deputy Minister for Innovation and Development')}}</p>

                                    <h6>{{__('Appointed: Prof. Dr. Abdulaziz Al-Saeed')}}</h6>
                                    <p class="sb-title">{{__('Board Member of National Center for Vegetation Development and Combating Desertification')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-header-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <p>{{__('Time')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <p>{{__('Title')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>{{__('Speakers')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:15 - 9:35')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Effects of exotic plants on biodiversity and environment')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Tony Miller')}}</h6>
                                    <p class="sb-title">{{__('Royal Commission for Al-Ula')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:35 - 9:50')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Invasive and exotic plant species in Saudi Arabia')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Jacob Thomas Pandalayil')}}</h6>
                                    <p class="sb-title">{{__('India')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:50 – 10:05')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Mechanisms of controlling invasive plants')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Prof. Dr. Jamal Muhammad Hajjar')}}</h6>
                                    <p class="sb-title">{{__('King Faisal University')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:05 – 10:20')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Suitable types of afforestation in different environments')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Abdulaziz Al-Saeed')}}</h6>
                                    <p class="sb-title">{{__('Board Member of National Center for Vegetation Development and Combating Desertification')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            {{-- <img src="{{ asset('assets/img/new-image/user.png') }}" alt=""> --}}
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:20 - 10:30')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Discussion')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Dr. Abdulaziz Al-Saeed')}}</h6>
                                    <p class="sb-title">{{__('Board Member of National Center for Vegetation Development
                                        and Combating Desertification')}}</p> --}}
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            {{-- <img src="{{ asset('assets/img/new-image/user.png') }}" alt=""> --}}
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:30 – 10:45')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Coffee Break')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Dr. Abdulaziz Al-Saeed')}}</h6>
                                    <p class="sb-title">{{__('Board Member of National Center for Vegetation Development
                                        and Combating Desertification')}}</p> --}}
                                </div>
                            </div>
                        </div>

                        <h1 class="breaking-heading">{{__('Third Session A')}}</h1>



                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:45-12:00')}}</p>
                                </div>
                                <div class="col-md-6">
                                    {{-- <h6>{{__('Coffee Break')}}</h6> --}}

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman: Mr. Saleh alhumaidi')}}</h6>
                                    <p class="sb-title">{{__('Board Member of National Center for Vegetation Development and Combating Desertification')}}</p>

                                    <h6>{{__('Appointed:: dr. Najeeb Alharbi')}}</h6>
                                    <p class="sb-title">{{__('National Center for Vegetation Development and Combating Desertification')}}</p>

                                </div>
                            </div>
                        </div>


                        <div class="new-agent-header-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <p>{{__('Time')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <p>{{__('Title')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>{{__('Speakers')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:45 - 11:05')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('The role of green areas in enhancing quality of life and public health')}}
                                    </h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Saif Al Ghais')}}</h6>
                                    <p class="sb-title">{{__('Director General, Environment Protection and Development Authority, UAE')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:05 - 11:20')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('The role of green areas in enhancing quality of life and public health')}}
                                    </h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Amal Al-Daej')}}</h6>
                                    <p class="sb-title">{{__('National Center for Vegetation Development and Combating Desertification..')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:20- 11:35')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Modern concepts in Urban Forestry')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('M. Mohammed Abdullah Al-Khaled')}}</h6>
                                    <p class="sb-title">{{__('Saudi ARAMCO')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:35 -11:50')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Modern concepts in Urban Forestry')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Nadine Al-Bitar')}}</h6>
                                    <p class="sb-title">{{__('The Environment Center in Arab Cities')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:50-12:00')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Discussion')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Nadine Al-Bita')}}</h6>
                                    <p class="sb-title">{{__('The Environment Center in Arab Cities')}}</p> --}}
                                </div>
                            </div>
                        </div>




                        <h1 class="breaking-heading">{{__('Fourth Session B')}}</h1>



                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:45 – 12:00')}}</p>
                                </div>
                                <div class="col-md-6">
                                    {{-- <h6>{{__('Coffee Break')}}</h6> --}}

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman: Dr. Abdulaziz Al Shaibani')}}</h6>
                                    <p class="sb-title">{{__('Deputy Minister for Water')}}</p>

                                    <h6>{{__('Appointed: Dr. Mishaal Al Harbi')}}</h6>
                                    <p class="sb-title">{{__('National Center for Vegetation Development and Combating Desertification..')}}</p>

                                </div>
                            </div>
                        </div>


                        <div class="new-agent-header-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <p>{{__('Time')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <p>{{__('Title')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>{{__('Speakers')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:45 - 11:05')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Afforestation Impacts on Hydrological Ecosystem Services')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Prof. Damia Barcelo Culleres')}}</h6>
                                    <p class="sb-title">{{__('Spanish National Research Council')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:05 - 11:20')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('The use of non-conventional water sources (gray and treated) in
                                        afforestation')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Eng. Abdul Mohsen Alfraihi')}}</h6>
                                    <p class="sb-title">{{__('Ministry of Environment, Water and Agriculture at Al Gaseem')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:20 -11:35')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Water irrigation for plants')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Abdullah Al-Rajhi')}}</h6>
                                    <p class="sb-title">{{__('King Abdulaziz City for Science and Technology')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:35 -11:50')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Saltwater Dependent Plants')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr.Mohammed Muzakr Almutairi')}}</h6>
                                    <p class="sb-title">{{__('Ministry of Environment, Water and Agriculture')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:50-12:00')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Discussion')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('M. Mohammed Abdullah Al-Khaled')}}</h6>
                                    <p class="sb-title">{{__('Saudi ARAMCO')}}</p> --}}
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:00 -12:30')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Dhohr Prayer')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Nadine Al-Bita')}}</h6>
                                    <p class="sb-title">{{__('The Environment Center in Arab Cities')}}</p> --}}
                                </div>
                            </div>
                        </div>



                        <h1 class="breaking-heading">{{__('Fifth Session A')}}</h1>



                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:30 – 1:45')}}</p>
                                </div>
                                <div class="col-md-6">
                                    {{-- <h6>{{__('Coffee Break')}}</h6> --}}

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman Zaid Alothman')}}</h6>
                                    <p class="sb-title">{{__('Dean of College of Science - King Saud University')}}</p>

                                    <h6>{{__('Appointed:Dr. Turki Alturki')}}</h6>
                                    <p class="sb-title">{{__('King Abdulazize City for Science and Technology')}}</p>

                                </div>
                            </div>
                        </div>


                        <div class="new-agent-header-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <p>{{__('Time')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <p>{{__('Title')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>{{__('Speakers')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:30 - 12:50')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Methods and Techniques of Soil improvement for the development vegetation cover')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Ole Kristian Sivertsen')}}</h6>
                                    <p class="sb-title">{{__('CEO of Desert Control')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:50 – 1:05')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Methods and Techniques of Soil improvement for the development vegetation cover')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Muhammad Ahmad Al-Sanad')}}</h6>
                                    <p class="sb-title">{{__('King Faisal University')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:05 – 1:20')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('The role of Sabkah and Wetland for the development of vegetation cover')}}
                                    </h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Suleiman Al-Khatib')}}</h6>
                                    <p class="sb-title">{{__('Deputy Minister of Agriculture')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:20 – 1:35')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('The role of Sabkah and Wetland for the development of vegetation cover')}}
                                    </h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Khalil Omar')}}</h6>
                                    <p class="sb-title">{{__('(ICBA)')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:35 – 1:45')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Discussion')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Dr. Khalil Omar')}}</h6>
                                    <p class="sb-title">{{__('(ICBA)')}}</p> --}}
                                </div>
                            </div>
                        </div>


                        <h1 class="breaking-heading">{{__('Sixth Session B')}}</h1>



                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:30 – 1:45')}}</p>
                                </div>
                                <div class="col-md-6">
                                    {{-- <h6>{{__('Coffee Break')}}</h6> --}}

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman: Dr. Saud Alrowaily')}}</h6>
                                    <p class="sb-title">{{__('Member of Shura')}}</p>

                                    <h6>{{__('Appointed: Mr. Kutaiba Alsaadon')}}</h6>
                                    <p class="sb-title">{{__('Advisor Ministry of Environment, Water and Agriculture of Saudi Arabia')}}</p>

                                </div>
                            </div>
                        </div>


                        <div class="new-agent-header-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <p>{{__('Time')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <p>{{__('Title')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>{{__('Speakers')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:30 - 12:50')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Afforestation practices in Pakistan')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Shabir Hussain')}}</h6>
                                    <p class="sb-title">{{__('Pakistan')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:50 – 1:05')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Afforestation practices in Morocco')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Khaled Al Sharqi')}}</h6>
                                    <p class="sb-title">{{__('Morocco')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:05 – 1:20')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Afforestation practices in Spain')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Mr. Fransisco Martinez')}}</h6>
                                    <p class="sb-title">{{__('Spain')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:20 – 1:35')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Afforestation practices in Saudi Arabia')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Abdullah Alburaiki')}}</h6>
                                    <p class="sb-title">{{__('National Center for Vegetation Development and Combating Desertification')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:35 – 1:45')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Discussion')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Dr. Khalil Omar')}}</h6>
                                    <p class="sb-title">{{__('(ICBA)')}}</p> --}}
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:45 – 2:00')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Coffee Break')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Dr. Khalil Omar')}}</h6>
                                    <p class="sb-title">{{__('(ICBA)')}}</p> --}}
                                </div>
                            </div>
                        </div>




                        <h1 class="breaking-heading">{{__('First Panel Discussion Room A')}}</h1>
                        <h1 class="breaking-heading">{{__('Investing in national parks and nature reserves')}}</h1>



                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('2:00 – 3:15')}}</p>
                                </div>
                                <div class="col-md-6">
                                    {{-- <h6>{{__('Coffee Break')}}</h6> --}}

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman: Dr. Talal Alhuraiki')}}</h6>
                                    <p class="sb-title">{{__('The CEO of Imam Abdulaziz bin Mohammed Royal Reserve Development Authority')}}</p>

                                    <h6>{{__('Appointed: Ms. Amani Albalawi')}}</h6>
                                    <p class="sb-title">{{__('National Center for Vegetation Cover and Combating Desertification')}}</p>

                                </div>
                            </div>
                        </div>


                        <div class="new-agent-header-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <p>{{__('Time')}}</p>
                                </div>
                                <div class="col-md-3">
                                    <p>{{__('Speaker')}}</p>
                                </div>
                                <div class="col-md-3">
                                    <p>{{__('Participants and Guests')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>{{__('Supervision and organization')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="row new-agent-child-row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('2:00 – 2:10')}}</p>
                            </div>
                            <div class="col-md-3">
                                <h6>{{__('Prince Saud bin Nahar Al-Saud Governor of Taif Governorate')}}</h6>

                            </div>
                            <div class="col-md-3">
                                <h6>{{__('Parks Investors Royal Reserve members and Beneficiaries.')}}</h6>
                            </div>

                            <div class="col-md-4">
                                {{-- <h6>{{__('Supervisor Ms. Amani Albalawi')}}</h6> --}}
                                <h6>{{__('Supervisor Ms. Amani Albalawi')}}</h6>
                                <h6>{{__('Mr.Hamad from Imam Abdul Aziz Reserve Development Authority')}}</h6>
                                <h6>{{__('Ms.Wassef Al - Qahtani')}}</h6>

                            </div>
                        </div>


                        <div class="row new-agent-child-row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('2:10 – 2:20')}}</p>
                            </div>
                            <div class="col-md-3">
                                <h6>{{__('Prince: Haifa Al Saud Ministry of Tourism')}}</h6>

                            </div>
                            <div class="col-md-3">
                                <h6>{{__('Parks Investors Royal Reserve members and Beneficiaries.')}}</h6>
                            </div>

                            <div class="col-md-4">
                                <h6>{{__('Supervisor Ms. Amani Albalawi')}}</h6>
                                <h6>{{__('Mr.Hamad from Imam Abdul Aziz Reserve Development Authority')}}</h6>
                                <h6>{{__('Ms.Wassef Al - Qahtani')}}</h6>

                            </div>
                        </div>
                        <div class="row new-agent-child-row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('2:20 – 2:30')}}</p>
                            </div>
                            <div class="col-md-3">
                                <h6>{{__('Ms. Kawthar from the Imam Abdulaziz Bin Mohammed Royal Reserve Development Authority')}}</h6>

                            </div>
                            <div class="col-md-3">
                                <h6>{{__('Parks Investors Royal Reserve members and Beneficiaries.')}}</h6>
                            </div>

                            <div class="col-md-4">
                                <h6>{{__('Supervisor Ms. Amani Albalawi')}}</h6>
                                <h6>{{__('Mr.Hamad from Imam Abdul Aziz Reserve Development Authority')}}</h6>
                                <h6>{{__('Ms.Wassef Al - Qahtani')}}</h6>

                            </div>
                        </div>

                        <div class="row new-agent-child-row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('2:30 – 2:40')}}</p>
                            </div>
                            <div class="col-md-3">
                                <h6>{{__('Dr. Muhammad Mufrah')}}</h6>

                            </div>
                            <div class="col-md-3">
                                <h6>{{__('Parks Investors Royal Reserve members and Beneficiaries.')}}</h6>
                            </div>

                            <div class="col-md-4">
                                {{-- <h6>{{__('Supervisor Ms. Amani Albalawi')}}</h6>
                                <h6>{{__('Mr.Hamad from Imam Abdul Aziz Reserve Development Authority')}}</h6>
                                <h6>{{__('Ms.Wassef Al - Qahtani')}}</h6> --}}
                            </div>
                        </div>


                        <div class="row new-agent-child-row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('2:40 – 3:25')}}</p>
                            </div>
                            <div class="col-md-3">
                                <h6>{{__('Open Panel Discussion.')}}</h6>

                            </div>
                            <div class="col-md-3">
                                {{-- <h6>{{__('Parks Investors Royal Reserve
                                    members and Beneficiaries.')}}</h6> --}}
                            </div>

                            <div class="col-md-4">
                                {{-- <h6>{{__('Supervisor Ms. Amani Albalawi')}}</h6>
                                <h6>{{__('Mr. Hamad from Imam Abdul Aziz Reserve Development Authority
                                    Ms. Wassef Al-Qahtani')}}</h6> --}}

                            </div>
                        </div>





                        <h1 class="breaking-heading">{{__('Second Panel Discussion')}} {{__('Room: B')}}</h1>
                        <h1 class="breaking-heading">{{__('The role of green association in the afforestation of KSA')}}
                        </h1>



                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('2:00 – 3:15')}}</p>
                                </div>
                                <div class="col-md-6">
                                    {{-- <h6>{{__('Coffee Break')}}</h6> --}}

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman: Prience Moteb bin Fahad Al Saud')}}</h6>
                                    <p class="sb-title">{{__('The President of Environmental Association& Combat Desertification and Weather and Climate Saudi Association.')}}</p>

                                    <h6>{{__('Appointed: Dr. Abdulrahman Alseqair')}}</h6>
                                    <p class="sb-title">{{__('Qassim University')}}</p>

                                </div>
                            </div>
                        </div>


                        <div class="new-agent-header-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <p>{{__('Time')}}</p>
                                </div>
                                <div class="col-md-3">
                                    <p>{{__('Speaker')}}</p>
                                </div>
                                <div class="col-md-3">
                                    <p>{{__('Participants and Guests')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>{{__('Supervision and organization')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="row new-agent-child-row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('2:00 – 2:10')}}</p>
                            </div>
                            <div class="col-md-3">
                                <h6>{{__('Talal Al-Rasheed, Environment Friend Association in Sharqia')}}</h6>

                            </div>
                            <div class="col-md-3">
                                <h6>{{__('All relevant Associations')}}</h6>
                            </div>

                            <div class="col-md-4">
                                <h6>{{__('Supervisor: Dr. Abdulrahman')}}</h6>
                                <h6>{{__('Alseqair')}}</h6>
                                <h6>{{__('Ms. Nouf Aldrehm')}}</h6>
                                <h6>{{__('Ms. Afnan Alotaibi')}}</h6>

                            </div>
                        </div>

                        <div class="row new-agent-child-row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('2:10 – 2:20')}}</p>
                            </div>
                            <div class="col-md-3">
                                <h6>{{__('M. Saud Al-Zahrani, Al-Baha Albaha Association')}}</h6>

                            </div>
                            <div class="col-md-3">
                                <h6>{{__('All relevant Associations')}}</h6>
                            </div>

                            <div class="col-md-4">
                                <h6>{{__('Supervisor: Dr. Abdulrahman')}}</h6>
                                <h6>{{__('Alseqair')}}</h6>
                                <h6>{{__('Ms. Nouf Aldrehm')}}</h6>
                                <h6>{{__('Ms. Afnan Alotaibi')}}</h6>

                            </div>
                        </div>


                        <div class="row new-agent-child-row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('2:20 – 2:30')}}</p>
                            </div>
                            <div class="col-md-3">
                                <h6>{{__('Dr Abdullah Alfahad, Environment Friends Association in Zulfi')}}</h6>

                            </div>
                            <div class="col-md-3">
                                <h6>{{__('All relevant Associations')}}</h6>
                            </div>

                            <div class="col-md-4">
                                <h6>{{__('Supervisor: Dr. Abdulrahman')}}</h6>
                                <h6>{{__('Alseqair')}}</h6>
                                <h6>{{__('Ms. Nouf Aldrehm')}}</h6>
                                <h6>{{__('Ms. Afnan Alotaibi')}}</h6>

                            </div>
                        </div>


                        <div class="row new-agent-child-row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('2:30 - 2:40')}}</p>
                            </div>
                            <div class="col-md-3">
                                <h6>{{__('Mustafa Ismail Alawi Morocco')}}</h6>

                            </div>
                            <div class="col-md-3">
                                <h6>{{__('All relevant Associations')}}</h6>
                            </div>

                            <div class="col-md-4">
                                <h6>{{__('Supervisor: Dr. Abdulrahman')}}</h6>
                                <h6>{{__('Alseqair')}}</h6>
                                <h6>{{__('Ms. Nouf Aldrehm')}}</h6>
                                <h6>{{__('Ms. Afnan Alotaibi')}}</h6>

                            </div>
                        </div>


                        <div class="row new-agent-child-row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('2:40 – 3:15')}}</p>
                            </div>
                            <div class="col-md-3">
                                <h6>{{__('Open Panel Discussion')}}</h6>

                            </div>
                            <div class="col-md-3">
                                {{-- <h6>{{__('All relevant Associations')}}</h6> --}}
                            </div>

                            <div class="col-md-4">
                                {{-- <h6>{{__('Supervisor: Dr. Abdulrahman Alseqair')}}</h6>
                                <h6>{{__('Ms. Nouf Aldrehm')}}</h6>
                                <h6>{{__('Ms. Afnan Alotaibi')}}</h6> --}}

                            </div>
                        </div>


                        <div class="row new-agent-child-row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('3:15 -4:00')}}</p>
                            </div>
                            <div class="col-md-3">
                                <h6>{{__('Lunch Break')}}</h6>

                            </div>
                            <div class="col-md-3">
                                {{-- <h6>{{__('All relevant Associations')}}</h6> --}}
                            </div>

                            <div class="col-md-4">
                                {{-- <h6>{{__('Supervisor: Dr. Abdulrahman Alseqair')}}</h6>
                                <h6>{{__('Ms. Nouf Aldrehm')}}</h6>
                                <h6>{{__('Ms. Afnan Alotaibi')}}</h6> --}}

                            </div>
                        </div>










                    </div>
                    <div class="tab-pane " id="tabs-3" role="tabpanel">

                        <br>
                        <h1 class="breaking-heading">{{__('First Session A')}}</h1>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:15 – 10:30')}}</p>
                                </div>
                                <div class="col-md-6">

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman: Dr. Ayman Gholam')}}</h6>
                                    <p class="sb-title">{{__('CEO of the National Center for Meteorology')}}</p>

                                    <h6>{{__('Appointed:Eng. Ahmed Al Nuaimi')}}</h6>
                                    <p class="sb-title">{{__('National Center for Vegetation Development and Combating Desertification')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-header-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <p>{{__('Time')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <p>{{__('Title')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>{{__('Speakers')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:15 –9:35')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Rainwater harvesting techniques and methods suitable in Dryland')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('prof. Abdulaziz Alturbag')}}</h6>
                                    <p class="sb-title">{{__('King Saud University')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:35– 9:50')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Rainwater harvesting techniques and methods suitable in Dryland')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Mr. Dr. Mohamed Taher Al-Barqawi')}}</h6>
                                    <p class="sb-title">{{__('ICSAD')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:50 – 10:05')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Artificial rain techniques to enhance water resources in dryland')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Eng. Ayman Albar')}}</h6>
                                    <p class="sb-title">{{__('National Center for Meteorology')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:05 - 10:20')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Artificial rain techniques to enhance water resources in dryland')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Kucera Paul')}}</h6>
                                    <p class="sb-title">{{__('National Center for Meteorology')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            {{-- <img src="{{ asset('assets/img/new-image/user.png') }}" alt=""> --}}
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:20 – 10:30')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Discussion')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Paulo Pereira')}}</h6>
                                    <p class="sb-title">{{__('Mykolas Romeris University, Lithuania')}}</p> --}}
                                </div>
                            </div>
                        </div>

                        <h1 class="breaking-heading">{{__('Second Session B')}}</h1>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:15 – 10:30')}}</p>
                                </div>
                                <div class="col-md-6">

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman: : Dr. Thobet Al Shahrani')}}</h6>
                                    <p class="sb-title">{{__('Member of the Board Directors of the National Center for Vegetation Development and Combating Desertification')}}</p>

                                    <h6>{{__('Appointed:Eng. Mossa Alowa')}}</h6>
                                    <p class="sb-title">{{__('National Center for Vegetation Development and Combating Desertification')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-header-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <p>{{__('Time')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <p>{{__('Title')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>{{__('Speakers')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:15 - 9:35')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('The role of Local communities in the development of vegetation cover')}}
                                    </h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Laila Saeed Al Harthy Amman')}}</h6>
                                    <p class="sb-title">{{__('Oman')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:35 - 9:50')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('The role of Local communities in the development of vegetation cover')}}
                                    </h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Shabir Hussain')}}</h6>
                                    <p class="sb-title">{{__('Pakistan')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:50 – 10:05')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('International experiences in community participation in tree planting initiatives')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Abdul Hamid Hamed')}}</h6>
                                    <p class="sb-title">{{__('FAO')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:05 – 10:20')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Afforestation and development of local plant communities')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Issam Ahbri')}}</h6>
                                    <p class="sb-title">{{__('Morocco')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            {{-- <img src="{{ asset('assets/img/new-image/user.png') }}" alt=""> --}}
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:20 - 10:30')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Discussion')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Dr. Abdulaziz Al-Saeed')}}</h6>
                                    <p class="sb-title">{{__('Board Member of National Center for Vegetation Development
                                        and Combating Desertification')}}</p> --}}
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            {{-- <img src="{{ asset('assets/img/new-image/user.png') }}" alt=""> --}}
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:30 – 10:45')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Coffee Break')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Dr. Abdulaziz Al-Saeed')}}</h6>
                                    <p class="sb-title">{{__('Board Member of National Center for Vegetation Development
                                        and Combating Desertification')}}</p> --}}
                                </div>
                            </div>
                        </div>

                        <h1 class="breaking-heading">{{__('Third Session A')}}</h1>



                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:45 - 12:00')}}</p>
                                </div>
                                <div class="col-md-6">
                                    {{-- <h6>{{__('Coffee Break')}}</h6> --}}

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman: Dr. Amr Homoud Abdallah Alamri')}}</h6>
                                    <p class="sb-title">{{__('Deputy CEO of Aseer Region Development Authority')}}</p>

                                    <h6>{{__('Appointed: Dr. Areej Alosaimi')}}</h6>
                                    <p class="sb-title">{{__('Imam Abdulrahman Bin Faisal University')}}</p>

                                </div>
                            </div>
                        </div>


                        <div class="new-agent-header-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <p>{{__('Time')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <p>{{__('Title')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>{{__('Speakers')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:45 - 11:05')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Agroforestry (integrated agriculture) and its environmental, economic and social roles')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Hamed Daly-Hassen')}}</h6>
                                    <p class="sb-title">{{__('Director General of National Observatory Agriculture')}}
                                    </p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:05 - 11:20')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Agricultural Terraces rehabilitation experience')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Mr. Saud AlKhial')}}</h6>
                                    <p class="sb-title">{{__('Ministry of Environment, Water and Agriculture of Saudi Arabia')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:20- 11:35')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Agricultural Terraces rehabilitation experience')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Mohammed Al-Wassar')}}</h6>
                                    <p class="sb-title">{{__('Institute Des Regions arides')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:35 -11:50')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Wild plant pests')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Hathal Al Dhafer')}}</h6>
                                    <p class="sb-title">{{__('King Saud University')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:50 - 12:00')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Discussion')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Nadine Al-Bita')}}</h6>
                                    <p class="sb-title">{{__('The Environment Center in Arab Cities')}}</p> --}}
                                </div>
                            </div>
                        </div>




                        <h1 class="breaking-heading">{{__('Fourth Session B')}}</h1>



                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:45 – 12:00')}}</p>
                                </div>
                                <div class="col-md-6">
                                    {{-- <h6>{{__('Coffee Break')}}</h6> --}}

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman: Osama Al Khalawi')}}</h6>
                                    <p class="sb-title">{{__('Member of the Board Directors of the National Center for Vegetation Development and Combating Desertification')}}</p>

                                    <h6>{{__('Appointed: Dr.Saleh Al Zamanan')}}</h6>
                                    <p class="sb-title">{{__('National Center for Vegetation Development and Combating Desertification')}}</p>

                                </div>
                            </div>
                        </div>


                        <div class="new-agent-header-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <p>{{__('Time')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <p>{{__('Title')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>{{__('Speakers')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:45 - 11:05')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Designing and landscaping residential and recreational parks')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Ashraf Al Turki')}}</h6>
                                    <p class="sb-title">{{__('King Abdulaziz University')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:05 - 11:20')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('The use of wild plants in the design and landscaping of outdoor spaces')}}
                                    </h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Prof. Dr. Omar bin Salem bin Omar Bahamam')}}</h6>
                                    <p class="sb-title">{{__('King SaUD uNIVERSITY')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:20 - 11:35')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('The role of afforestation in contributing to the strengthening of infrastructure in cities')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Eng. Andul Aziz Al Mogbel')}}</h6>
                                    <p class="sb-title">{{__('The Royal Commission for Riyadh City')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:35 - 11:50')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Biological methods using plants to stabilize sand dunes')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Prof. Dr. Majdi Al-Banna')}}</h6>
                                    <p class="sb-title">{{__('Royal Commission for Al-Ula')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:50 - 12:00')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Discussion')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Eng. Andul Aziz Al Mogbel')}}</h6>
                                    <p class="sb-title">{{__('The Royal Commission for Riyadh City')}}</p> --}}
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:00 - 12:30')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Dhohr Prayer')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Nadine Al-Bita')}}</h6>
                                    <p class="sb-title">{{__('The Environment Center in Arab Cities')}}</p> --}}
                                </div>
                            </div>
                        </div>



                        <h1 class="breaking-heading">{{__('Fifth Session A')}}</h1>



                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:30 – 1:45')}}</p>
                                </div>
                                <div class="col-md-6">
                                    {{-- <h6>{{__('Coffee Break')}}</h6> --}}

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman: Eng. Abdul Rahman Al Zughaibi')}}</h6>
                                    <p class="sb-title">{{__('Deputy Ministry for Economic Affairs and Investment')}}
                                    </p>

                                    <h6>{{__('Appointed: Dr. Fahad Almutlaq')}}</h6>
                                    <p class="sb-title">{{__('Geographic Information Science and Technology')}}</p>

                                </div>
                            </div>
                        </div>


                        <div class="new-agent-header-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <p>{{__('Time')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <p>{{__('Title')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>{{__('Speakers')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:30 - 12:50')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Indicators of Successful Afforestation and Reforestation practices')}}
                                    </h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Christopher Clarke')}}</h6>
                                    <p class="sb-title">{{__('King Abdullah University for Science and Technology')}}
                                    </p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:50 – 1:05')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('The role of afforestation in non-wood products and environmental services economic valuation')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Adel Almutlaq')}}</h6>
                                    <p class="sb-title">{{__('Reef Program')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:05 – 1:20')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Investment in multipurpose plant species')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Saadia Zarira')}}</h6>
                                    <p class="sb-title">{{__('Alhassan 2 for Agriculture and Veterinary')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:20 – 1:35')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Using of environmentally friendly techniques to rationalize irrigation')}}
                                    </h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Ole Kristian Sivertsen')}}</h6>
                                    <p class="sb-title">{{__('CEO of Desert Control')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:35 – 1:45')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Discussion')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Dr. Khalil Omar')}}</h6>
                                    <p class="sb-title">{{__('(ICBA)')}}</p> --}}
                                </div>
                            </div>
                        </div>


                        <h1 class="breaking-heading">{{__('Sixth Session B')}}</h1>



                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:30 – 1:45')}}</p>
                                </div>
                                <div class="col-md-6">
                                    {{-- <h6>{{__('Coffee Break')}}</h6> --}}

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman: Brigadier General Saher Alharbi')}}</h6>
                                    <p class="sb-title">{{__('Special Forces for Environmental Security')}}</p>

                                    <h6>{{__('Appointed: Ms. Maha Saud Alotaibi')}}</h6>
                                    <p class="sb-title">{{__('Commander of Ministry of Environment, Water and Agriculture of Saudi Arabia')}}</p>

                                </div>
                            </div>
                        </div>


                        <div class="new-agent-header-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <p>{{__('Time')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <p>{{__('Title')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>{{__('Speakers')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:30 - 12:50')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('The role of regulations and legislation in Stimulating Investment in vegetation cover development')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Tony Charter')}}</h6>
                                    <p class="sb-title">{{__('Australia')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:50 – 1:05')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('The role of regulations and legislation in Stimulating Investment in vegetation cover development')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Nora Al Hamad')}}</h6>
                                    <p class="sb-title">{{__('National Center for Vegetation Cover Development and Combating Desertification')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:05 – 1:20')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Rules and regulations in the protection of vegetation cover')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Major General. Ali Al-Asmari')}}</h6>
                                    <p class="sb-title">{{__('National Center for Vegetation Development and Combating Desertification')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:20 – 1:30')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('A global perspective on governance, possibilities and success factors of response options to address land degradation')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Barron Orr')}}</h6>
                                    <p class="sb-title">{{__('Lead Scientist, UNCCD')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:35 – 1:45')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Discussion')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Dr. Khalil Omar')}}</h6>
                                    <p class="sb-title">{{__('(ICBA)')}}</p> --}}
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:45 – 2:00')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Coffee Break')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Dr. Khalil Omar')}}</h6>
                                    <p class="sb-title">{{__('(ICBA)')}}</p> --}}
                                </div>
                            </div>
                        </div>



                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('2:00 - 2:30')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Honoring Participants and Organizers')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Dr. Khalil Omar')}}</h6>
                                    <p class="sb-title">{{__('(ICBA)')}}</p> --}}
                                </div>
                            </div>
                        </div>


                        <h1 class="breaking-heading">{{__('Recommendations Committee A')}}</h1>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/agenda-2.jpeg') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('2:30 – 3:30')}}</p>
                                </div>
                                <div class="col-md-6">
                                    {{-- <h6>{{__('Honoring Participants and Organizers')}}</h6> --}}

                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman:Dr. Ahmed Alfarhan')}}</h6>
                                    <p class="sb-title">{{__('Scientific Committee Chairman &Member of the Board Directors of the National Center for Vegetation Development and Combating Desertification')}}</p>

                                    <h6>{{__('Appointed Dr. Abdullah Alhajoj')}}</h6>
                                    <p class="sb-title">{{__('Regional Manager of Rangeland at National Center for Vegetation Cover Development & Combating Desertification')}}</p>

                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('3:30 – 4:00')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Lunch Break')}}</h6>

                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Session Chairman:Dr. Ahmed Alfarhan')}}</h6>
                                    <p class="sb-title">{{__('Scientific Committee Chairman &Member of the Board
                                        Directors of the National Center for Vegetation Development and Combating
                                        Desertification')}}</p>

                                    <h6>{{__('Appointed Dr. Abdullah Alhajoj')}}</h6>
                                    <p class="sb-title">{{__('Regional Manager of Rangeland at National Center for
                                        Vegetation Cover Development & Combating Desertification')}}</p> --}}

                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>


        </div>



    </div>



    <div class="client-section">
        <img class="client-section-1" src="{{ asset('assets/img/banner-icon/client-1.png') }}" alt="">
        <div class="custom-container">
            <div class="row justify-content-center">
                <div class="col-xl-12 col-lg-12 col-md-12">
                    <!-- Section Tittle -->
                    <div class="section-tittle text-center">
                        <h2 class="mb-0 mt-4">{{ __('Sponsors') }}</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="client-slider dots-n-arrows-5">
                @foreach($sponsors as $sponsors)
                <div class="client-slide">
                    <img class="slide-image" src="{{ asset($sponsors->image)}}" alt="">
                </div>
                @endforeach

                {{-- <div class="client-slide">
                    <img class="slide-image" src="{{ asset('assets/img/sp-new/sp-2.png') }}" alt="">
                </div>
                <div class="client-slide">
                    <img class="slide-image" src="{{ asset('assets/img/sp-new/sp-3.png') }}" alt="">
                </div>
                <div class="client-slide">
                    <img class="slide-image" src="{{ asset('assets/img/sp-new/sp-4.png') }}" alt="">
                </div>
                <div class="client-slide">
                    <img class="slide-image" src="{{ asset('assets/img/sp-new/sp-5.png') }}" alt="">
                </div>
                <div class="client-slide">
                    <img class="slide-image" src="{{ asset('assets/img/sp-new/sp-6.png') }}" alt="">
                </div>
                <div class="client-slide">
                    <img class="slide-image" src="{{ asset('assets/img/sp-new/sp-7.png') }}" alt="">
                </div> --}}



                {{-- <div class="client-slide">
                    <img class="slide-image" src="{{ asset('assets/img/sp/sp-5.png') }}" alt="">
                </div>

                <div class="client-slide">
                    <img class="slide-image" src="{{ asset('assets/img/sp/sp-6.png') }}" alt="">
                </div>

                <div class="client-slide">
                    <img class="slide-image" src="{{ asset('assets/img/sp/sp-7.png') }}" alt="">
                </div>

                <div class="client-slide">
                    <img class="slide-image" src="{{ asset('assets/img/sp/sp-8.png') }}" alt="">
                </div>

                <div class="client-slide">
                    <img class="slide-image" src="{{ asset('assets/img/sp/sp-9.png') }}" alt="">
                </div>

                <div class="client-slide">
                    <img class="slide-image" src="{{ asset('assets/img/sp/sp-10.png') }}" alt="">
                </div>

                <div class="client-slide">
                    <img class="slide-image" src="{{ asset('assets/img/sp/sp-11.png') }}" alt="">
                </div>


                <div class="client-slide">
                    <img class="slide-image" src="{{ asset('assets/img/sponsors/sp-2.jpg') }}" alt="">

                </div>
                <div class="client-slide">
                    <img class="slide-image" src="{{ asset('assets/img/sponsors/sp-1.jpg') }}" alt="">

                </div>
                <div class="client-slide">
                    <img class="slide-image" src="{{ asset('assets/img/sponsors/sp-3.jpg') }}" alt="">

                </div>
                <div class="client-slide">
                    <img class="slide-image" src="{{ asset('assets/img/sponsors/sp-4.jpg') }}" alt="">

                </div>
                <div class="client-slide">
                    <img class="slide-image" src="{{ asset('assets/img/sponsors/sp-5.png') }}" alt="">

                </div>

                <div class="client-slide">
                    <img class="slide-image" src="{{ asset('assets/img/sp/sp-1.png') }}" alt="">
                </div>

                <div class="client-slide">
                    <img class="slide-image" src="{{ asset('assets/img/sp/sp-2.png') }}" alt="">
                </div>


                <div class="client-slide">
                    <img class="slide-image" src="{{ asset('assets/img/sp/sp-3.png') }}" alt="">
                </div>


                <div class="client-slide">
                    <img class="slide-image" src="{{ asset('assets/img/sp/sp-4.png') }}" alt="">
                </div>
                --}}




            </div>
        </div>
    </div>
   
</main>
 <!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="background: #000000ba;">
<div class="modal-dialog modal-dialog-centered" role="document" style="    max-width: 550px;
">
    <div class="modal-content" style="background-image: url('{{ asset('assets/img/new-image/gal-2.png') }}');height: 400px; border-radius: 10px;    border-radius: 8px;
    border: 2px solid #b1d235;">

        <div class="" style="    display: flex;
        gap: 5px;
        padding: 10px 30px;
        border-bottom: 1px solid white;">
            <a href="{{ route('home') }}"><img class=""
                    src="{{ asset('assets/img/logo.png') }}" alt="" width="53"></a>
            <a target="_blank"><img class=""
                    src="{{ asset('assets/img/ini.png') }}" alt="" width="112"></a>
        </div>
        <div class="modal-header" style="border: 0;">
            {{-- <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5> --}}
            <button type="button" class="close right-cr" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            {{-- <img src="https://www.sustainweb.org/resources/images/landscapes/park_c_pexels.jpg" alt=""
            style="display: block; margin: auto;width: 200px; margin-top: 20px; max-width: 100%;"> --}}
            {{-- <img src="" alt=""> --}}

           
        </div>
        <a  href="{{route('livelink')}}" class="btn btn-warning banner-btn" style="    width: 80%;
        margin: auto;
        margin-bottom: 20px;
        background: white;
        color: darkgreen;
        font-weight: 700;
        border-radius: 40px;">{{__('Click To See Live Sessions')}}</a>
    </div>
</div>
</div>




{{-- @if(!session()->has('modal'))

<script>
    $(window).load(function(){
        $('#popup').modal('show');
     });
</script>

@endif
{{ session()->put('modal','shown') }}
--}}


<!-- Jquery, Popper, Bootstrap -->
<script src="{{asset('assets/js/vendor/jquery-1.12.4.min.js')}}"></script>
<script src="{{asset('assets/js/popper.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<!-- Jquery Mobile Menu -->
<script src="{{asset('assets/js/jquery.slicknav.min.js')}}"></script>

<script>
    $(document).ready(function(){
        $('#exampleModalCenter').modal('show');
    });
</script>


@endsection