<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="{{ app()->getLocale() == 'en' ? 'ltr' : 'rtl' }}">


<head>
    @toastr_css
    <meta charset="utf-8">
    <title>EXPRO</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/fav.png')}}">
    <!-- CSS here -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/slicknav.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/gijgo.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/slick.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/new-style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/responsiveness.css')}}">
    

</head>

<body>
    <!-- ? Preloader Start -->
    {{-- <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="{{asset('assets/img/logo.png')}}" alt="">
                </div>
            </div>
        </div>
    </div> --}}
    <!-- Preloader Start -->
    <header>
        <!--? Header Start -->
        <div class="header-area">
            <div class="main-header">
                <div class="custom-container">
                    <div class="row align-items-center">
                        <!-- Logo -->
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-9 col-9">
                            <div class="logo" style="display: flex;gap: 5px;">
                                <a href="#"><img class="set-logo-mob-1" src="{{asset('assets/img/logo.png')}}" alt=""
                                        width="90"></a>
                            </div>
                        </div>
                        <div class="col-md-3 col-3 show-on-mob">
                            <a onClick="document.getElementById('lang').submit();">{{
                                \App::getLocale()=='en'?'عربي':'EN' }}</a>
                            <form id="lang" method="POST" action="{{ route('lang') }}">@csrf</form>
                        </div>
                        <div class="col-xl-9 col-lg-9 col-md-9 pr-0">
                            <div class="menu-main d-flex align-items-center justify-content-end">
                                <!-- Main-menu -->
                                <div class="main-menu f-right d-none d-lg-block">
                                    <nav>
                                        <ul id="navigation">


                                            <li class="cl-han" style="cursor: pointer">
                                                <a onClick="document.getElementById('lang').submit();">{{
                                                    \App::getLocale()=='en'?'عربي':'EN' }}</a>
                                                <form id="lang" method="POST" action="{{ route('lang') }}">@csrf</form>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>


                        <!-- Mobile Menu -->
                        <div class="col-12 p-0">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->
    </header>
    @yield('content')


    <div class="reg-section">

        <div class="custom-container reg-form">

            <form action="{{route('storeregister')}}" method="POST">
                @CSRF

                <div class="form-box">
                    <h2 class="form-tit">{{__('Confirmation of attendance at the launch of the national evidence')}}
                    </h2>
                    <div class="row">
                        <div class="col-md-6 mb-md-4 mb-2">
                            <div class="form-sec">

                                <label for="">{{__('Name')}} <span style="color: red;">*</span></label>
                                <input name="name" class="@error('name') is-invalid @enderror" value="{{ old('name') }}"
                                    placeholder="{{ \App::getLocale() == " en" ?"Write your name here...": "" }}"
                                    type="text">
                                @error('name')
                                <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>
                                    {{$message}}</span>
                                @enderror


                            </div>
                        </div>

                        <div class="col-md-6 mb-md-4 mb-2">
                            <div class="form-sec">
                                <label for="">{{__('Email')}} <span style="color: red;">*</span></label>
                                <input name="email" class="@error('email') is-invalid @enderror"
                                    value="{{ old('email') }}" placeholder="{{ \App::getLocale() == " en" ?"Write your
                                    email here...": "" }}" type="email">
                                @error('email')
                                <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 mb-md-2 mb-2">
                            <div class="form-sec">
                                <label for="">{{__('Phone Number')}} <span style="color: red;">*</span></label>
                                <input type="number" name="phonenumber" placeholder="{{ \App::getLocale() == " en"
                                    ?"Write your phone number here...": "" }}"
                                    class="@error('phonenumber') is-invalid @enderror" value="{{ old('phonenumber') }}">
                                @error('phonenumber')
                                <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 mb-md-4 mb-2">
                            <div class="form-sec">
                                <label for="">{{__('Entity Name')}} <span style="color: red;">*</span></label>
                                <input name="entityname" class="@error('entityname') is-invalid @enderror"
                                    value="{{ old('entityname') }}" placeholder="{{ \App::getLocale() == " en" ?"Write
                                    your entity name here...": "" }}" type="text">
                                @error('entityname')
                                <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-sec">
                                <div class="header-right-btn d-lg-block ml-30 submit-btn text-center">
                                    <button type="submit"
                                        class="btn header-btn w-30 border-r reg-bbt">{{__('Submit')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                </div>


            </form>


        </div>
    </div>
    </div>

    <footer style="position: fixed;
    width: 100%;
    bottom: 0;">
        <!-- footer-bottom area -->
        <div class="footer-bottom-area footer-bg">
            <div class="container">
                <div class="footer-border">
                    <div class="row d-flex justify-content-between align-items-center">
                        <div class="col-xl-12 col-lg-12 ">
                            <div class="footer-copy-right">
                                <p>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    {{__('Copyright')}} &copy;
                                    <script>
                                        document.write(new Date().getFullYear());
                                    </script> {{__('All rights reserved |')}}
                                    <!-- This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a
                                        href="https://colorlib.com" target="_blank">Colorlib</a> -->
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                </p>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-4">
                            {{-- <div class="footer-social f-right">
                                <a href=""><i class="fab fa-twitter"></i></a>
                                <a href=""><i class="fab fa-facebook-f"></i></a>
                                <a href=""><i class="fas fa-globe"></i></a>
                                <a href=""><i class="fab fa-behance"></i></a>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer End-->
        {{-- <div id="back-top">
            <a title="Go to Top" href="#"> <i class="fas fa-level-up-alt"></i></a>
        </div> --}}
    </footer>
    <!-- Scroll Up -->

    {{-- @guest

    <div id="back-top-2">
        <a title="Go to Top" href="{{route('register')}}">{{__('Registration')}}</a>
    </div>

    @endguest --}}
   
    <!-- JS here -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <script src="{{asset('assets/js/vendor/modernizr-3.5.0.min.js')}}"></script>

    <!-------------------------Toastr message-------------------------------->
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css"> --}}


    <!-- Jquery, Popper, Bootstrap -->
    <script src="{{asset('assets/js/vendor/jquery-1.12.4.min.js')}}"></script>
    <script src="{{asset('assets/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>

    <script src="{{asset('admin-assets/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{asset('admin-assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Jquery Mobile Menu -->
    <script src="{{asset('assets/js/jquery.slicknav.min.js')}}"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('assets/js/slick.min.js')}}"></script>
    <!-- One Page, Animated-HeadLin -->
    <script src="{{asset('assets/js/wow.min.js')}}"></script>
    <script src="{{asset('assets/js/animated.headline.js')}}"></script>
    <script src="{{asset('assets/js/jquery.magnific-popup.js')}}"></script>

    <!-- Date Picker -->
    <script src="{{asset('assets/js/gijgo.min.js')}}"></script>
    <!-- Nice-select, sticky -->
    <script src="{{asset('assets/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.sticky.js')}}"></script>

    <!-- counter , waypoint -->
    <script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('assets/js/waypoints.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>
    <!-- contact js -->
    <script src="{{asset('assets/js/contact.js')}}"></script>
    <script src="{{asset('assets/js/jquery.form.js')}}"></script>
    <script src="{{asset('assets/js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/js/mail-script.js')}}"></script>
    <script src="{{asset('assets/js/jquery.ajaxchimp.min.js')}}"></script>
    <script src="{{asset('assets/js/lozad.min.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.10.4/gsap.min.js"></script>

    <!-- Jquery Plugins, main Jquery -->
    <script src="{{asset('assets/js/plugins.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>

    <script>
        var countDownDate = new Date("May 29, 2022 9:00:00").getTime();
        // var countDownDate = new Date("March 10, 2022 12:37:25").getTime();

// Update the count down every 1 second
const lang = "{{ app()->getLocale() }}"
var x = setInterval(function () {

    // Get today's date and time
    var now = new Date().getTime();

    // Find the distance between now and the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = (("0" + Math.floor(distance / (1000 * 60 * 60 * 24))).slice(-2)).split("");
    var hours = (("0" + Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))).slice(-2)).split("");
    var minutes = (("0" + Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))).slice(-2)).split("");
    var seconds = (("0" + Math.floor((distance % (1000 * 60)) / 1000)).slice(-2)).split("");

    // Output the result in an element with id="demo"
    document.getElementById("days").innerHTML = `<span class="${lang}-${days[0]}"></span><span class="${lang}-${days[1]}"></span>`;
    document.getElementById("hours").innerHTML = `<span class="${lang}-${hours[0]}"></span><span class="${lang}-${hours[1]}"></span>`;
    document.getElementById("minutes").innerHTML = `<span class="${lang}-${minutes[0]}"></span><span class="${lang}-${minutes[1]}"></span>`;
    document.getElementById("seconds").innerHTML = `<span class="${lang}-${seconds[0]}"></span><span class="${lang}-${seconds[1]}"></span>`;
    // If the count down is over, write some text
    // element.getElementsByClassName("aftercountdownbutton").style.display : 'none';


    if (distance < 0) {
        clearInterval(x);
        document.getElementById("countdown").innerHTML = "Expired";
        // element.getElementsByClassName("countdown").style.display : 'none';
        // element.getElementsByClassName("aftercountdownbutton").style.display : 'block';

        // document.getElementById("demo").innerHTML = `<iframe style="display: block;" width="100%" height="450px;" src="https://www.youtube.com/watch?v=BLl32FvcdVM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
    }
}, 1000);

    </script>

    <script>
        function openCity(evt, cityName) {
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }
      tablinks = document.getElementsByClassName("tablinks");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
      }
      document.getElementById(cityName).style.display = "block";
      evt.currentTarget.className += " active";
    }
    // Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
    </script>
    <script>
        $("#dd").click(function () {
        $("#navigation").css("display","none !important")
        // $("#").show();
    });
    </script>
    <script>
        //           function myFunction() {
//     var x = document.getElementsByClassName("slicknav_nav");
//     if (x.style.display === "none") {
//     //   x.style.display = "block";
//     } else {
//       x.style.display = "none";
//     }
//   }
  function myFunction(){
  var elms = document.getElementsByClassName("slicknav_nav");

  Array.from(elms).forEach((x) => {
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  })
}
    </script>
    
    

    @jquery
    @toastr_js
    @toastr_render
    
</body>


</html>