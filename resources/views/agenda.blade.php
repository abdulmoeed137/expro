@extends('layouts.app')
@section('content')
<style>
 /* timer css */

 .days-en-1 {
        position: relative;
    }
    .days-en-1::before {
        content: "1";
        position: absolute;
        font-size: 22px !important;
        color: #006b55;
        font-size: 3.5rem;
        color: #006b55 !important;
    }
    .days-ar-1 {
        position: relative;
    }
    .days-ar-1::before {
        content: "١";
        position: absolute;
        font-size: 22px !important;
        color: #006b55;
        font-size: 3.5rem;
        color: #006b55 !important;
    }
    .en-1::before {
        content: "1";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .ar-1::before {
        content: "١";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .en-2::before {
        content: "2";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .ar-2::before {
        content: "٢";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .en-3::before {
        content: "3";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .ar-3::before {
        content: "٣";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .en-4::before {
        content: "4";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .ar-4::before {
        content: "٤";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .en-5::before {
        content: "5";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .ar-5::before {
        content: "٥";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .en-6::before {
        content: "6";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .ar-6::before {
        content: "٦";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .en-7::before {
        content: "7";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .ar-7::before {
        content: "٧";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .en-8::before {
        content: "8";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .ar-8::before {
        content: "٨";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .en-9::before {
        content: "9";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .ar-9::before {
        content: "٩";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .en-0::before {
        content: "0";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }

    .ar-0::before {
        content: "٠";
        font-size: 22px !important;
        color: black;
        font-size: 3.5rem;
        color: #006b55 !important;
    }
</style>
<div class="main-agenda-section">
    <div classs="relative">
            <div class="absolute leave">
              <img src="{{ asset('assets/img/banner-icon/live-leave.png') }}" alt="">
            </div>
            <div class="absolute circle fade-in-text">
              <img src="{{ asset('assets/img/anime/circle.svg') }}" alt="">
            </div>
            <div class="absolute downwardC blink" style="left: 0;bottom: 0;">
              <img src="{{ asset('assets/img/banner-icon/banner-dbl.png') }}" alt="">
            </div>

             {{-- <div class="absolute downwardC slightMove" style="top: 277%;z-index: 9;">
              <img src="{{ asset('assets/img/banner-icon/banner-dbl.png') }}" alt="">
            </div> --}}
         </div>
    <div class="custom-container">
        <div class="row">
            <div class="col-md-6">
                <h1 class="head">{{__('Sessions')}}</h1>
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    <div class="agenda-container">
        <div class="agenda-slider dots-n-arrows-7">

            @foreach($sessions as $sessions)
            <div class="agenda-item">
                <div class="">
                    <h1>{{ \App::getLocale() == "en" ?$sessions->en->name:$sessions->ar->name  }}</h1>
                <p>{{ \App::getLocale() == "en" ?$sessions->en->title:$sessions->ar->title  }}
                </p>
                </div>
                <div class="">
                    <h1>{{__('Subjects')}}</h1>
                    <ul>
                        <li>{{ \App::getLocale() == "en" ?$sessions->en->description:$sessions->ar->description  }}
                        </li>
                        {{-- <li>{{__('(Middle East Green Initiative)')}}</li>
                        <li>{{__('G20 Global Initiative on #006b55ucing Land Degradation and Enhancing Conservation of Terrestrial Habitats')}}
                        </li> --}}

                    </ul>
                </div>
            </div>
            @endforeach
            {{-- <div class="agenda-item">
                <div class="">
                    <h1>{{ __('Session 1') }}</h1>
                <p>{{ __('Pioneer initiatives for developing vegetation cover') }}
                </p>
                </div>
                <div class="">
                    <h1>{{__('Subjects')}}</h1>
                    <ul>
                        <li>{{__('Saudi Green Initiative: goals, objectives, principles, and components')}}
                        </li>
                        <li>{{__('(Middle East Green Initiative)')}}</li>
                        <li>{{__('G20 Global Initiative on #006b55ucing Land Degradation and Enhancing Conservation of Terrestrial Habitats')}}
                        </li>

                    </ul>
                </div>
            </div>

            <div class="agenda-item">
                <div class="">
                    <h1>{{ __('Session 2') }}</h1>
                <p>{{ __('Sustainable development of natural vegetation') }}
                </p>
                </div>
                <div class="">
                    <h1>{{__('Subjects')}}</h1>
                    <ul>
                        <li>{{ __('Rehabilitation and sustainable management of rangelands') }}
                        </li>
                        <li>{{ __('Impact of Livestock grazing management and changing the animal behavior on vegetation cover') }}
                        </li>
                        <li>{{ __('Rehabilitation of forests and natural plant communities') }}
                        </li>
                        <li>{{ __('Invasive and exotic plant species in Saudi Arabia') }}
                        </li>
                        <li>{{ __('Mechanisms of controlling invasive plants') }}
                        </li>
                        <li>{{ __('Effects of exotic plants on biodiversity and environment') }}
                        </li>
                        <li>{{ __('Using modern technologies in monitoring and developing of vegetation cover') }}
                        </li>
                        <li>{{ __('Using modern technologies in motioning and combating forest fires') }}
                        </li>
                    </ul>
                </div>
            </div>

            <div class="agenda-item">
                <div class="">
                    <h1>{{ __('Session 3') }}</h1>
                <p>{{ __('Afforestation for combating desertification in arid areas') }}
                </p>
                </div>
                <div class="">
                   
                    <ul>
                        <li> {{ __('Methods and techniques of soil improvement for the development vegetation cover') }}
                        </li>
                        <li>{{ __('Methods and techniques of seed production, propagation, and seed broadcast to support afforestation projects') }}
                        </li>
                        <li>{{ __('The role of nature reserves and protected areas in the development of vegetation cover') }}
                        </li>
                        <li> {{ __('the role of local communities in the development of vegetation cover') }}

                        </li>
                        <li>{{ __('Biological methods using plants to stabilize sand dunes') }}
                        </li>
                        <li>{{ __('Indicators of successful afforestation and reforestation practices') }}

                        </li>
                        <li>{{ __('Suitable types of afforestation in different environments') }}

                        </li>
                        <li>{{ __('Afforestation in salt marshes and wetlands') }}</li>
                        <li>{{ __('wild plant pests') }}
                        </li>
                    </ul>
                </div>
            </div>

            <div class="agenda-item">
                <div class="">
                    <h1>{{ __('Session 4') }}</h1>
                <p>{{ __('Afforestation as a natural based solution to mitigate climate change impacts') }}
                </p>
                </div>
                <div class="">
                   
                    <ul>
                        <li>{{ __('Carbon sequestration and storage in mangroves (benchmarking)') }}
                        </li>
                        <li>{{ __('Carbon storage in trees and shrubs dominant in the Arabian Peninsula') }}
                        </li>
                        <li>{{ __('Afforestation to #006b55uce natural disasters') }}

                        </li>
                        <li> {{ __('Techniques for estimating the efficiency of plants in carbon fixation') }}

                        </li>
                    </ul>
                </div>
            </div>

            <div class="agenda-item">
                <div class="">
                    <h1>{{ __('Session 5') }}</h1>
                <p>{{ __('Water resources sustainability for afforestation in dryland') }}
                </p>
                </div>
                <div class="">
                   
                    <ul>
                        <li>{{ __('The use of non-conventional water sources (gray and treated) in afforestation') }}
                        </li>
                        <li>{{ __('Rainwater harvesting techniques and methods suitable in dryland') }}
                        </li>
                        <li>{{ __('Artificial rain techniques to enhance water resources in dryland') }}


                        </li>
                        <li> {{ __('Water requirement for native trees and shrubs – Theory and practice') }}

                        </li>
                        <li>{{ __('Using of environmentally friendly techniques to rationalize irrigation') }}
                        </li>

                    </ul>
                </div>
            </div>

            <div class="agenda-item">
                <div class="">
                    <h1>{{ __('Session 6') }}</h1>
                <p>{{ __('Urban Forestry') }}
                </p>
                </div>
                <div class="">
                    
                    <ul>
                        <li>{{ __('The use of wild plants in the design and landscaping of outdoor spaces') }}
                        </li>
                        <li>{{ __('Designing and landscaping residential and recreational parks') }}

                        </li>
                        <li>{{ __('The role of afforestation in contributing to the strengthening of infrastructure in cities') }}


                        </li>
                        <li> {{ __('Modern concepts in urban forestry') }}


                        </li>

                    </ul>
                </div>
            </div>


            <div class="agenda-item">
                <div class="">
                    <h1>{{ __('Session 7') }}</h1>
                <p>{{ __('The role of Saudi Green Initiative in supporting agricultural development') }}
                </p>
                </div>
                <div class="">
                   
                    <ul>
                        <li>{{ __('The use of rainwater harvesting techniques in the rehabilitation of agricultural terraces') }}

                        </li>
                        <li>{{ __('Agroforestry (integrated agriculture) and its environmental, economic and social roles') }}

                        </li>
                        <li>{{ __('The role of afforestation in contributing to the strengthening of infrastructure in cities') }}


                        </li>
                        <li> {{ __('Saltwater Dependent Plants') }}

                        </li>

                    </ul>
                </div>
            </div>



            <div class="agenda-item">
                <div class="">
                    <h1>{{ __('Session 8') }}</h1>
                <p>{{ __('International experiences in afforestation') }}
                </p>
                </div>
                <div class="">
                    
                    <ul>
                        <li>{{ __('Chinas experience in the afforestation of desert areas') }}


                        </li>
                        <li>{{ __('The African Great Green Wall') }}
                        </li>
                        <li>{{ __('Afforestation practices in Pakistan, Australia, Spain, Morocco.') }}


                        </li>
                        <li> {{ __('Participation of International Organizations and Research Centers (FAO, ACSAD, ICARDA, and ICBA) in afforestation') }}


                        </li>
                        <li>
                            {{ __('Governance, possibilities and success factors of afforestation in some countries') }}
                        </li>

                    </ul>
                </div>
            </div>


            <div class="agenda-item">
                <div class="">
                    <h1>{{ __('Session 9') }}</h1>
                <p>{{ __('The role of afforestation in enhancing investment opportunities') }}
                </p>
                </div>
                <div class="">
                    
                    <ul>
                        <li>{{ __('The role of green funds in supporting afforestation projects') }}



                        </li>
                        <li>{{ __('The role of regulations and legislation in stimulating investment in vegetation cover development') }}
                        </li>
                        <li>{{ __('Investment in multipurpose plant species') }}

                        </li>
                        <li> {{ __('Investing in national parks and nature reserves') }}



                        </li>
                        <li>
                            {{ __('Nurseries as an investment in afforestation projects') }}

                        </li>

                    </ul>
                </div>
            </div>

            <div class="agenda-item">
                <div class="">
                    <h1>{{ __('Session 10') }}</h1>
                <p>{{ __('Economic valuation of ecosystem services from vegetation development.') }}
                </p>
                </div>
                <div class="">
                   
                    <ul>
                        <li>{{ __('Afforestation and development of local plant communities (Moringa peregrina as an example)') }}



                        </li>
                        <li>{{ __('The role of afforestation in non-wood products and environmental services economic valuation') }}

                        </li>

                    </ul>
                </div>
            </div>


            <div class="agenda-item">
                <div class="">
                    <h1>{{ __('Session 11') }}</h1>
                <p>{{ __('Community participation in afforestation projects at the local and global level') }}
                </p>
                </div>
                <div class="">
                    
                    <ul>
                        <li>{{ __('The role of local communities in afforestation (local and global success stories)') }}




                        </li>
                        <li>{{ __('International experiences in community participation in tree planting initiatives') }}

                        </li>
                        <li>{{ __('The role of green association in the afforestation of KSA') }}
                        </li>
                        <li>{{ __('The role of the private sector, the non-profit organizations, and the charitable societies in vegetation cover development') }}
                        </li>


                    </ul>
                </div>
            </div> --}}



    </div>

    <div class="custom-container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-6">
                <a href="{{route('register')}}" class="agenda-reg-btn">{{__('Registration')}}</a>
            </div>
            <div class="col-md-6 col-sm-6 col-6">
                <h4 class="second-left">Remaining Time to Register</h4>
                <div class="countdown countdown-2">
                    <div class="container-day child first-child">
                        <h3 class="time-dis second">{{ __('Days') }} </h3>
                        <h3 id="days" class="agenda-time">Time</h3>
                    </div>
                    <div class="container-hour child">
                        <h3 class="time-dis second"> {{ __('Hours') }} </h3>
                        <h3 id="hours" class="agenda-time">Time</h3>
                    </div>
                
                </div>
            </div>
        </div>
    </div>


</div>

{{-- <div class="agenda-inside-main">
        <div class="custom-container">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="head">{{__('Agenda')}}</h1>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    <div class="dot-row">
        <div class="dot-item">
            <h5>{{__('Day one')}}</h5>
            <p class="date-p">{{__('Sunday, 29th May 2022')}}</p>

            <div class="dot-content-box">
                @foreach($DayOneAgenda as $DayOneAgenda)
                            <?php
                        $western_arabic = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
                        $eastern_arabic = ['٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'];
                        ?>
                            <div class="row">
                               
                                <div class="col-md-6 col-sm-6 col-6">
                                    <p class="m-0 time-para"> 
                                        @if(\App::getLocale() == "ar")
                                        {{str_replace($western_arabic, $eastern_arabic, date('g:i a', strtotime($DayOneAgenda->start)))}}
                                        –
                                        {{str_replace($western_arabic, $eastern_arabic, date('g:i a', strtotime($DayOneAgenda->end)))}}
                                        @else
                                            {{ date('g:i a', strtotime($DayOneAgenda->start))}} –
                                            {{ date('g:i a', strtotime($DayOneAgenda->end)) }}
                                        @endif
                                        
                                    </p>
                                </div>

                                <div class="col-md-6 col-sm-6 col-6">
                                    <div class="">
                                        <h4 class="m-0"> <span>{{ \App::getLocale() == "en" ?$DayOneAgenda->topic_en: $DayOneAgenda->topic_ar}}</span>
                                        </h4>

                                    </div>
                                </div>
                            </div>
                            @endforeach
            </div>
        </div>
        <div class="dot-item">
            <h5>{{__('Day two')}}</h5>
            <p class="date-p">{{__('Monday, 30th May 2022')}}</p>
            <div class="dot-content-box">
                @foreach($DayTwoAgenda as $DayTwoAgenda)
                <?php
                $western_arabic = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
                $eastern_arabic = ['٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'];
                ?>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-6">
                        <p class="m-0 time-para"> 
                            @if(\App::getLocale() == "ar")
                            {{str_replace($western_arabic, $eastern_arabic, date('g:i a', strtotime($DayTwoAgenda->start)))}}
                            –
                            {{str_replace($western_arabic, $eastern_arabic, date('g:i a', strtotime($DayTwoAgenda->end)))}}
                            @else
                                {{ date('g:i a', strtotime($DayTwoAgenda->start))}} –
                                {{ date('g:i a', strtotime($DayTwoAgenda->end)) }}
                            @endif
                            
                        </p>
                    </div>

                    <div class="col-md-6 col-sm-6 col-6">
                        <div class="">
                            <h4 class="m-0"> <span class="no-lrt-ar">{{ \App::getLocale() == "en" ?$DayTwoAgenda->topic_en: $DayTwoAgenda->topic_ar}}</span>
                            </h4>

                        </div>
                    </div>
                </div>
                @endforeach
               
            </div>
        </div>
        <div class="dot-item">
            <h5>{{__('Day three')}}</h5>
            <p class="date-p">{{__('Tuesday, 31st May 2022')}}</p>

            <div class="dot-content-box">
                @foreach($DayThreeAgenda as $DayThreeAgenda)
                            <?php
                            $western_arabic = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
                            $eastern_arabic = ['٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'];
                            ?>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-6">
                                    <p class="m-0 time-para"> 
                                        @if(\App::getLocale() == "ar")
                                        {{str_replace($western_arabic, $eastern_arabic, date('g:i a', strtotime($DayThreeAgenda->start)))}}
                                        –
                                        {{str_replace($western_arabic, $eastern_arabic, date('g:i a', strtotime($DayThreeAgenda->end)))}}
                                        @else
                                            {{ date('g:i a', strtotime($DayThreeAgenda->start))}} –
                                            {{ date('g:i a', strtotime($DayThreeAgenda->end)) }}
                                        @endif
                                        
                                    </p>
                                </div>
    
                                <div class="col-md-6 col-sm-6 col-6">
                                    <div class="">
                                        <h4 class="m-0"> <span class="no-lrt-ar">{{ \App::getLocale() == "en" ?$DayThreeAgenda->topic_en: $DayThreeAgenda->topic_ar}}</span>
                                        </h4>
    
                                    </div>
                                </div>
                            </div>
                            @endforeach

              
            </div>

        </div>
    </div>
</div> --}}


<div class="speakers-section agenda-new speakers-section-mob">
    <div class="custom-container seesion" style="margin-top: 0">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-6">
                        <h1 class="head">{{__('Agenda')}}</h1>
                    </div>
                    <div class="col-md-4 col-sm-6 col-6">
                        <h1 class="head-2">{{__('Certificates of attendance are available')}}</h1>
                    </div>
                    <div class="col-md-4 col-sm-6 col-12">
                        {{-- <a href="{{route('agenda')}}">{{__('See ALL details')}} <img class="see-all-arrow rtl-arrow" src="{{ asset('assets/img/new-image/right-arrow.png') }}" alt=""></a> --}}
                    </div>
                </div>

                <div class="schedule-tab">
                    <ul class="nav nav-tabs " role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab" aria-selected="false">
                                <h5>{{__('First Day')}}</h5>
                                <p>{{__('Sunday, 29th May 2022')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab" aria-selected="false">
                                <h5>{{__('Second Day')}}</h5>
                                <p>{{__('Monday, 30th May 2022')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  show" data-toggle="tab" href="#tabs-3" role="tab" aria-selected="true">
                                <h5>{{__('Third Day')}}</h5>
                                <p>{{__('Tuesday, 31st May 2022')}}</p>
                            </a>
                        </li>

                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane  active show" id="tabs-1" role="tabpanel">
                            <div class="new-agent-header-row">
                                <div class="row">
                                    <div class="col-md-2">
                                        <p>{{__('Time')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p>{{__('Title')}}</p>
                                    </div>
                                    <div class="col-md-4">
                                        <p>{{__('Speakers')}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="new-agent-child-row">
                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('9:00-10:00')}}</p>
                                    </div>
                                    <div class="col-md-7">
                                        <h6>{{__('Opening Ceremony')}}</h6>
                                        <ul>
                                            <li>
                                                {{__('The National Anthem of the Kingdom of Saudi Arabia')}}
                                            </li>
                                            <li>
                                                {{__('Quran')}}
                                            </li>
                                            <li>
                                                {{__('Organizing Committee Speech by Dr. Khaled Al-Abdulkader, CEO of the National Center for Vegetation Cover and Combating Desertification')}}
                                            </li>
                                            <li>
                                                {{__('Opening Speech by his Excellency Eng. Abdulrahman AlFadhli, Minister of Environment, Water & Agriculture')}}
                                            </li>
                                            <li>
                                                {{__('Professor. Mr. Lee White, the Ministry of Environment (Gabonese Republic)')}}
                                            </li>
                                            <li>
                                                {{__('Mr. Abdulrahim Houmy, Acting Director general of the National Agency for Water and Forests (Morocco)')}}
                                            </li>
                                             <li>
                                                {{__('Ibrahim Thiaw, UNCCD Executive Secretary and Deputy Executive Director of United Nations Environment Program.')}}
                                            </li>
                                            <li>
                                                {{__('Li Taian, Director of Division of Asia and Africa Affairs, Foreign Affairs Office of the Peoples Government of Gansu China')}}
                                            </li>
                                          
                                        </ul>
                                    </div>
                                    <div class="col-md-3">
                                    </div>
                                </div>
                            </div>
                            <h1 class="breaking-heading">{{__('Sponsors Honoring')}}</h1>

                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('10:00-10:45')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Exhibition Official Opening')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                    </div>
                                </div>
                            </div>
                            <h1 class="breaking-heading">{{__('First Session A')}}</h1>

                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('10:45-12:00')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6></h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('Session Chairman: Dr. Osama I. Faqeeha')}}</h6>
                                        <p class="sb-title">{{__('Deputy Minister for Environment')}}</p>

                                        <h6>{{__('Appointed: Dr. Ibrahim Aldujain')}}</h6>
                                        <p class="sb-title">{{__('Adviser at Ministry of Municipal Rural Affairs and Housing')}}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('10:45 - 11:05')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Environmental Initiatives within The Saudi Green Initiative')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('Dr. Osama I. Faqeeha')}}</h6>
                                        <p class="sb-title">{{__('Deputy Minister for Environment')}}</p>
                                        </div>
                                </div>
                            </div>


                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('11:05 - 11:20')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Strategy and National Forestry Sustainable Plan in Saudi Arabia')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('Dr. Khaled Al-Abdulkader')}}</h6>
                                        <p class="sb-title">{{__('CEO of the National Center for Vegetation Development and Combating Desertification')}}</p>
                                        </div>
                                </div>
                            </div>


                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('11:20 - 11:35')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Global Initiative on Reducing Land Degradation and Enhancing Conservation of Terrestrial Habitats')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('Dr. Muralee Thummarukudy')}}</h6>
                                        <p class="sb-title">{{__('UNCCD')}}</p>
                                        </div>
                                </div>
                            </div>

                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('11:35-11:50')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Governance, possibilities and success factors of afforestation in some countries')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('Andrea Kutter')}}</h6>
                                        <p class="sb-title">{{__('The World Bank')}}</p>
                                        </div>
                                </div>
                            </div>


                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('11:50 – 12:00')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Discussion')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        {{-- <h6>{{__('Andrea Kutter')}}</h6>
                                        <p class="sb-title">{{__('The World Bank')}}</p> --}}
                                        </div>
                                </div>
                            </div>

                            <div class="new-agent-child-row">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('12:00 -12:30')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Dhohr Prayer')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        {{-- <h6>{{__('Andrea Kutter')}}</h6>
                                        <p class="sb-title">{{__('The World Bank')}}</p> --}}
                                        </div>
                                </div>
                            </div>
                            <h1 class="breaking-heading">{{__('Second Session A')}}</h1>
                            
                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('12:30-1:45')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        {{-- <h6>{{__('Discussion')}}</h6> --}}
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('Session Chairman: MohammedQurban')}}</h6>
                                        <p class="sb-title">{{__('CEO of the National Centre for Wildlife Development')}}</p>

                                        <h6>{{__('Appointed: Dr. Ahmed Alghamdi')}}</h6>
                                        <p class="sb-title">{{__('National Center for Vegetation Development and Combating Desertification')}}</p>

                                        </div>
                                </div>
                            </div>

                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('12:30 - 12:50')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Impact of Livestock grazing management and changing the animal behavior on vegetation cover')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('Tibbo Markos')}}</h6>
                                        <p class="sb-title">{{__('FAO')}}</p>
                                        </div>
                                </div>
                            </div>


                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('12:50 - 1:05')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Rehabilitation and sustainable management of Rangelands')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('Dr. Abdullah Alhajoj')}}</h6>
                                        <p class="sb-title">{{__('National Center for Vegetation Development and Combating Desertification')}}</p>
                                        </div>
                                </div>
                            </div>

                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('1:05 - 1:20')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Rehabilitation and sustainable management of Rangelands')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('Dr. Mounir Louhaichi')}}</h6>
                                        <p class="sb-title">{{__('ICARDA')}}</p>
                                        </div>
                                </div>
                            </div>

                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('1:20 - 1:35')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('The role of nature reserves and protected areas in the development of vegetation cover')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('Abdullah Altalasat')}}</h6>
                                        <p class="sb-title">{{__('National Centre for Wildlife Development')}}</p>
                                        </div>
                                </div>
                            </div>


                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('1:35 – 1:45')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Discussion')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        {{-- <h6>{{__('Abdullah Altalasat')}}</h6>
                                        <p class="sb-title">{{__('National Centre for Wildlife Development')}}</p> --}}
                                        </div>
                                </div>
                            </div>

                            <div class="new-agent-child-row">
                                {{-- <img src="{{ asset('assets/img/new-image/user.png') }}" alt=""> --}}

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('1:45 - 2:00')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Coffee Break')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        {{-- <h6>{{__('Abdullah Altalasat')}}</h6>
                                        <p class="sb-title">{{__('National Centre for Wildlife Development')}}</p> --}}
                                        </div>
                                </div>
                            </div>
                            <h1 class="breaking-heading">{{__('Third Session B')}}</h1>

                            <div class="new-agent-child-row">
                                {{-- <img src="{{ asset('assets/img/new-image/user.png') }}" alt=""> --}}

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('12:30-1:45')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        {{-- <h6>{{__(' Coffee Break')}}</h6> --}}
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('Session Chairman:Eng. Ali Alghamdi')}}</h6>
                                        <p class="sb-title">{{__('CEO of the National Center for Environment Compliance')}}</p>

                                        <h6>{{__('Appointed: Dr. Arwa Alhuqail')}}</h6>
                                        <p class="sb-title">{{__('Princess Nourah Bint Abdul Rahman University')}}</p>
                                        </div>
                                </div>
                            </div>


                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('12:30 - 12:50')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Carbon sequestration and Storage in mangroves (Benchmarking)')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('Dr. CarlosM. Duarte')}}</h6>
                                        <p class="sb-title">{{__('King Abdullah University forScience and Technology')}}</p>

                                        </div>
                                </div>
                            </div>

                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('12:50 - 1:05')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Carbon storage in trees and shrubs dominant in the Arabian Peninsula')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__(' Massaoudou Moussa')}}</h6>
                                        <p class="sb-title">{{__('Nagger')}}</p>

                                        </div>
                                </div>
                            </div>

                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('1:05 - 1:20')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Techniques for estimating the efficiency of plants in carbon fixation')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('Mohmed Elsheikh')}}</h6>
                                        <p class="sb-title">{{__('King Saud University')}}</p>

                                        </div>
                                </div>
                            </div>


                            
                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('1:20 - 1:35')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Afforestation as nature-based solution to reduce heatwaves and floods')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('Paulo Pereira')}}</h6>
                                        <p class="sb-title">{{__('Mykolas Romeris University, Lithuania')}}</p>

                                        </div>
                                </div>
                            </div>


                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('1:35 – 1:45')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Discussion')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        {{-- <h6>{{__('Paulo Pereira')}}</h6>
                                        <p class="sb-title">{{__('Mykolas Romeris University, Lithuania')}}</p> --}}

                                        </div>
                                </div>
                            </div>


                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('1:45 – 2:00')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Coffee Break')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        </div>
                                </div>
                            </div>
                            <h1 class="breaking-heading">{{__('First Panel Discussion Room: A')}} {{__('')}}</h1>
                            <h1 class="breaking-heading">{{__('The role of the private sector, the non-profit organizations, and the charitable societies in vegetation cover development')}}</h1>

                                
                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('2:00 – 3:15')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        {{-- <h6>{{__('Coffee Break')}}</h6> --}}
                               
                                    </div>
                                    <div class="col-md-4">
                                         <h6>{{__('Session Chairman: Dr. Munir bin Fahd Al-Sahli')}}</h6>
                                        <p class="sb-title">{{__('CEO of the Environment Fund')}}</p>

                                        <h6>{{__('Appointed: Dr. Haifa Alolayan')}}</h6>
                                        <p class="sb-title">{{__('Advisor atMinistry of Environment, Water and Agriculture of Saudi Arabi')}}</p>
                                        </div>
                                </div>
                            </div>


                            <div class="new-agent-header-row">
                                <div class="row">
                                    <div class="col-md-2">
                                        <p>{{__('Time')}}</p>
                                    </div>
                                    <div class="col-md-3">
                                        <p>{{__('Speaker')}}</p>
                                    </div>
                                    <div class="col-md-3">
                                        <p>{{__('Participants and Guests')}}</p>
                                    </div>
                                    <div class="col-md-4">
                                        <p>{{__('Supervision and organization')}}</p>
                                    </div>
                                </div>
                            </div>


                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('2:00 – 2:10')}}</p>
                                    </div>
                                    <div class="col-md-3">
                                        <h6>{{__('Dr. Khaled Mreziq Al-Harbi, Director of Innovation and Customer Experience Department')}}</h6>
                                       
                                    </div>
                                    <div class="col-md-3">
                                         <h6>{{__('Local Banks - Environment Fund + The World Bank WWF Agricultural companies and major companies(SABIC, Aramco, Red Sea, Qiddiya and NEOM)')}}</h6>
                                       
                                        </div>

                                        <div class="col-md-4">
                                            <h6>{{__('Supervisor Dr. Dr. Haifa Alolayan')}}</h6>
                                            <h6>{{__('Ms. Shahd Aldurman')}}</h6>
                                            <h6>{{__('Ms. Safa Alsalamah')}}</h6>
                                           
                                           </div>
                                </div>
                            </div>


                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('2:10 – 2:20')}}</p>
                                    </div>
                                    <div class="col-md-3">
                                        <h6>{{__('Eng./ Abdulrahman bin Suleiman Abdullah Al - Rashoud, Director General of Quality and Development Programs')}}</h6>
                                       
                                    </div>
                                    <div class="col-md-3">
                                        <h6>{{__('Local Banks - Environment Fund + The World Bank WWF Agricultural companies and major companies(SABIC, Aramco, Red Sea, Qiddiya and NEOM)')}}</h6>
                                       
                                        </div>

                                        <div class="col-md-4">
                                            <h6>{{__('Supervisor Dr. Dr. Haifa Alolayan')}}</h6>
                                            <h6>{{__('Ms. Shahd Aldurman')}}</h6>
                                            <h6>{{__('Ms. Safa Alsalamah')}}</h6>
                                           </div>
                                </div>
                            </div>


                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('2:20 – 2:30')}}</p>
                                    </div>
                                    <div class="col-md-3">
                                        <h6>{{__('Eng. Ahmed Abdullah Alotaibi, Green Department and Environmental Policy (Aramco)')}}</h6>
                                       
                                    </div>
                                    <div class="col-md-3">
                                        <h6>{{__('Local Banks - Environment Fund + The World Bank WWF Agricultural companies and major companies(SABIC, Aramco, Red Sea, Qiddiya and NEOM)')}}</h6>
                                       
                                        </div>

                                        <div class="col-md-4">
                                            <h6>{{__('Supervisor Dr. Dr. Haifa Alolayan')}}</h6>
                                            <h6>{{__('Ms. Shahd Aldurman')}}</h6>
                                            <h6>{{__('Ms. Safa Alsalamah')}}</h6>
                                           </div>
                                </div>
                            </div>


                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('2:30 - 2:40')}}</p>
                                    </div>
                                    <div class="col-md-3">
                                        <h6>{{__('Morne Fourie Roshn Real Estate')}}</h6>
                                       
                                    </div>
                                    <div class="col-md-3">
                                        <h6>{{__('Local Banks - Environment Fund + The World Bank WWF Agricultural companies and major companies(SABIC, Aramco, Red Sea, Qiddiya and NEOM)')}}</h6>
                                       
                                        </div>

                                        <div class="col-md-4">
                                            <h6>{{__('Supervisor Dr. Dr. Haifa Alolayan')}}</h6>
                                            <h6>{{__('Ms. Shahd Aldurman')}}</h6>
                                            <h6>{{__('Ms. Safa Alsalamah')}}</h6>
                                           </div>
                                </div>
                            </div>

                              <div class="new-agent-child-row">
                             

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('2:40 – 3:15')}}</p>
                                    </div>
                                    <div class="col-md-3">
                                        <h6>{{__('Open Panel Discussion')}}</h6>
                                       
                                    </div>
                                    <div class="col-md-3">
                                         {{-- <h6>{{__('Local Banks - Environment Fund +The World Bank WWF Agricultural companies and major companies (SABIC, Aramco, Red Sea, Qiddiya and NEOM)')}}</h6> --}}
                                       
                                        </div>

                                        <div class="col-md-4">
                                            {{-- <h6>{{__('Supervisor Dr. Dr. Haifa Alolayan Ms. Shahd Aldurman Ms. Safa Alsalamah')}}</h6> --}}
                                           
                                           </div>
                                </div>
                            </div>


                            <h1 class="breaking-heading">{{__('Second Panel Discussion')}} {{__('Room: B')}}</h1>
                            <h1 class="breaking-heading">{{__('The Modern techniques for the effective management of nurseries and seed banks to support vegetation development projects')}}</h1>



                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('2:00 – 3:15')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        {{-- <h6>{{__('Coffee Break')}}</h6> --}}
                            
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('Session Chairman Khaled Aljarallah')}}</h6>
                                        <p class="sb-title">{{__('CEO of the National Center for the Prevention and Control of Plants Pests and Animal Diseases')}}</p>

                                        <h6>{{__('Appointed: Dr. Abdullah Alburaiki')}}</h6>
                                        <p class="sb-title">{{__('National Center for Vegetation Development and Combating Desertification.')}}</p>
                                        </div>
                                </div>
                            </div>


                            <div class="new-agent-header-row">
                                <div class="row">
                                    <div class="col-md-2">
                                        <p>{{__('Time')}}</p>
                                    </div>
                                    <div class="col-md-3">
                                        <p>{{__('Speaker')}}</p>
                                    </div>
                                    <div class="col-md-3">
                                        <p>{{__('Participants and Guests')}}</p>
                                    </div>
                                    <div class="col-md-4">
                                        <p>{{__('Supervision and organization')}}</p>
                                    </div>
                                </div>
                            </div>


                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('2:00 – 2:10')}}</p>
                                    </div>
                                    <div class="col-md-3">
                                        <h6>{{__('M. Nasser Bu Dasta Executive Manager at Climate Technology')}}</h6>
                                            {{-- <h6>{{__('Dr. Naiemah Alenezy Hail University')}}</h6> --}}
                                       
                                    </div>
                                    <div class="col-md-3">
                                         <h6>{{__('Nurseries, seed dispersal companies and seed multiplication stations')}}</h6>
                                       
                                        </div>

                                        <div class="col-md-4">
                                            <h6>{{__('Supervisor Dr. Abdullah Alburaiki')}}</h6>
                                            <h6>{{__('Ms. Rawan Al-Qahtani')}}</h6>
                                            <h6>{{__('Ms. Amira Al-Qahtani')}}</h6>
                                            <h6>{{__('Ms. Reham Al-Mutairi')}}</h6>
                                           
                                           </div>
                                </div>
                            </div>


                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('2:00 – 2:20')}}</p>
                                    </div>
                                    <div class="col-md-3">
                                        <h6>{{__('Dr. Naiemah Alenezy Hail University')}}</h6>
                                       
                                    </div>
                                    <div class="col-md-3">
                                         <h6>{{__('Nurseries, seed dispersal companies and seed multiplication stations')}}</h6>
                                       
                                        </div>

                                        <div class="col-md-4">
                                            <h6>{{__('Supervisor Dr. Abdullah Alburaiki')}}</h6>
                                            <h6>{{__('Ms. Rawan Al-Qahtani')}}</h6>
                                            <h6>{{__('Ms. Amira Al-Qahtani')}}</h6>
                                            <h6>{{__('Ms. Reham Al-Mutairi')}}</h6>
                                           
                                           </div>
                                </div>
                            </div>



                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('2:20 – 2:30')}}</p>
                                    </div>
                                    <div class="col-md-3">
                                        <h6>{{__('Bruce Pederson Mawarid Holding Investment')}}</h6>
                                            {{-- <h6>{{__('Mawarid Holding Investment')}}</h6> --}}
                                       
                                    </div>
                                    <div class="col-md-3">
                                         <h6>{{__('Resources Investment Holding')}}</h6>
                                        </div>

                                        <div class="col-md-4">
                                                                                               
                                           </div>
                                </div>
                            </div>

                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('2:30 – 3:15')}}</p>
                                    </div>
                                    <div class="col-md-3">
                                        <h6>{{__('Open Panel Discussion')}}</h6>
                                            {{-- <h6>{{__('Mawarid Holding Investment')}}</h6> --}}
                                       
                                    </div>
                                    <div class="col-md-3">
                                         
                                        </div>

                                        <div class="col-md-4">
                                                                                               
                                           </div>
                                </div>
                            </div>

                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">

                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('3:15 – 4:00')}}</p>
                                    </div>
                                    <div class="col-md-3">
                                        <h6>{{__('Lunch Break')}}</h6>
                                            {{-- <h6>{{__('Mawarid Holding Investment')}}</h6> --}}
                                       
                                    </div>
                                    <div class="col-md-3">
                                         
                                        </div>

                                        <div class="col-md-4">
                                                                                               
                                           </div>
                                </div>
                            </div>



                        </div>
                        <div class="tab-pane " id="tabs-2" role="tabpanel">
                            <br>
                        <h1 class="breaking-heading">{{__('First Session A')}}</h1>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:15 – 10:30')}}</p>
                                </div>
                                <div class="col-md-6">
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman: Dr. Khaled Al-Abdulkader')}}</h6>
                                    <p class="sb-title">{{__('CEO of the National Center for Vegetation Development & Combating Desertification')}}</p>

                                    <h6>{{__('Appointed:Eng. Sameer Malaika')}}</h6>
                                    <p class="sb-title">{{__('National Center for Vegetation & Development Combating Desertification')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-header-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <p>{{__('Time')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <p>{{__('Title')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>{{__('Speakers')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:15 - 9:35')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Rehabilitation of Forests and natural plant communities')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Shahid Rashid Awan')}}</h6>
                                    <p class="sb-title">{{__('Pakistan')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:35 - 9:50')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Rehabilitation of Forests and natural plant communities')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Rahma Aljeraisy')}}</h6>
                                    <p class="sb-title">{{__('King Khalid University')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:50 – 10:05')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Using modern technologies in monitoring and developing of vegetation cover')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Ms.Elsy Ibrahim')}}</h6>
                                    <p class="sb-title">{{__('Belgium')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:05 – 10:20')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Post-fire Management in the climate context')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Paulo Pereira')}}</h6>
                                    <p class="sb-title">{{__('Mykolas Romeris University, Lithuania')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            {{-- <img src="{{ asset('assets/img/new-image/user.png') }}" alt=""> --}}
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:20 - 10:30')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Discussion')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Paulo Pereira')}}</h6>
                                    <p class="sb-title">{{__('Mykolas Romeris University, Lithuania')}}</p> --}}
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            {{-- <img src="{{ asset('assets/img/new-image/user.png') }}" alt=""> --}}
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:30 - 10:45')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Coffee Break')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Paulo Pereira')}}</h6>
                                    <p class="sb-title">{{__('Mykolas Romeris University, Lithuania')}}</p> --}}
                                </div>
                            </div>
                        </div>

                        <h1 class="breaking-heading">{{__('Second Session B')}}</h1>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:15 – 10:30')}}</p>
                                </div>
                                <div class="col-md-6">
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman: Dr. Hamad Albatshan')}}</h6>
                                    <p class="sb-title">{{__('Deputy Minister for Innovation and Development')}}</p>

                                    <h6>{{__('Appointed: Dr. Mubark Alrashedi')}}</h6>
                                    <p class="sb-title">{{__('Ministry of Environment, Water and Agriculture of Saudi Arabia')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-header-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <p>{{__('Time')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <p>{{__('Title')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>{{__('Speakers')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:15 - 9:35')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Effects of exotic plants on biodiversity and environment')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Tony Miller')}}</h6>
                                    <p class="sb-title">{{__('Royal Commission for Al-Ula')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:35 - 9:50')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Invasive and exotic plant species in Saudi Arabia')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Jacob Thomas Pandalayil')}}</h6>
                                    <p class="sb-title">{{__('India')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('9:50 – 10:05')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Mechanisms of controlling invasive plants')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Prof. Dr. Jamal Muhammad Hajjar')}}</h6>
                                    <p class="sb-title">{{__('King Faisal University')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:05 – 10:20')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Suitable types of afforestation in different environments')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Abdulaziz Al-Saeed')}}</h6>
                                    <p class="sb-title">{{__('Board Member of National Center for Vegetation Development and Combating Desertification')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            {{-- <img src="{{ asset('assets/img/new-image/user.png') }}" alt=""> --}}
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:20 - 10:30')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Discussion')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Dr. Abdulaziz Al-Saeed')}}</h6>
                                    <p class="sb-title">{{__('Board Member of National Center for Vegetation Development and Combating Desertification')}}</p> --}}
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            {{-- <img src="{{ asset('assets/img/new-image/user.png') }}" alt=""> --}}
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:30 – 10:45')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Coffee Break')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Dr. Abdulaziz Al-Saeed')}}</h6>
                                    <p class="sb-title">{{__('Board Member of National Center for Vegetation Development and Combating Desertification')}}</p> --}}
                                </div>
                            </div>
                        </div>

                    <h1 class="breaking-heading">{{__('Third Session A')}}</h1>


                    
                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('10:45-12:00')}}</p>
                            </div>
                            <div class="col-md-6">
                                {{-- <h6>{{__('Coffee Break')}}</h6> --}}
                       
                            </div>
                            <div class="col-md-4">
                                <h6>{{__('Session Chairman: Mr. Saleh alhumaidi')}}</h6>
                                <p class="sb-title">{{__('Board Member of National Center for Vegetation Development and Combating Desertification')}}</p>

                                <h6>{{__('Appointed:: dr. Najeeb Alharbi')}}</h6>
                                <p class="sb-title">{{__('National Center for Vegetation Development and Combating Desertification')}}</p>

                            </div>
                        </div>
                    </div>


                    <div class="new-agent-header-row">
                        <div class="row">
                            <div class="col-md-2">
                                <p>{{__('Time')}}</p>
                            </div>
                            <div class="col-md-6">
                                <p>{{__('Title')}}</p>
                            </div>
                            <div class="col-md-4">
                                <p>{{__('Speakers')}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('10:45 - 11:05')}}</p>
                            </div>
                            <div class="col-md-6">
                                <h6>{{__('The role of green areas in enhancing quality of life and public health')}}</h6>
                       
                            </div>
                            <div class="col-md-4">
                                <h6>{{__('Dr. Saif Al Ghais')}}</h6>
                                <p class="sb-title">{{__('Director General, Environment Protection and Development Authority, UAE')}}</p>
                            </div>
                        </div>
                    </div>


                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('11:05 - 11:20')}}</p>
                            </div>
                            <div class="col-md-6">
                                <h6>{{__('The role of green areas in enhancing quality of life and public health')}}</h6>
                       
                            </div>
                            <div class="col-md-4">
                                <h6>{{__('Dr. Amal Al-Daej')}}</h6>
                                <p class="sb-title">{{__('National Center for Vegetation Development and Combating Desertification..')}}</p>
                            </div>
                        </div>
                    </div>


                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('11:20- 11:35')}}</p>
                            </div>
                            <div class="col-md-6">
                                <h6>{{__('Modern concepts in Urban Forestry')}}</h6>
                       
                            </div>
                            <div class="col-md-4">
                                <h6>{{__('M. Mohammed Abdullah Al-Khaled')}}</h6>
                                <p class="sb-title">{{__('Saudi ARAMCO')}}</p>
                            </div>
                        </div>
                    </div>


                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('11:35 -11:50')}}</p>
                            </div>
                            <div class="col-md-6">
                                <h6>{{__('Modern concepts in Urban Forestry')}}</h6>
                       
                            </div>
                            <div class="col-md-4">
                                <h6>{{__('Nadine Al-Bitar')}}</h6>
                                <p class="sb-title">{{__('The Environment Center in Arab Cities')}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('11:50-12:00')}}</p>
                            </div>
                            <div class="col-md-6">
                                <h6>{{__('Discussion')}}</h6>
                       
                            </div>
                            <div class="col-md-4">
                                {{-- <h6>{{__('Nadine Al-Bita')}}</h6>
                                <p class="sb-title">{{__('The Environment Center in Arab Cities')}}</p> --}}
                            </div>
                        </div>
                    </div>




                    <h1 class="breaking-heading">{{__('Fourth Session B')}}</h1>


                    
                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('10:45 – 12:00')}}</p>
                            </div>
                            <div class="col-md-6">
                                {{-- <h6>{{__('Coffee Break')}}</h6> --}}
                       
                            </div>
                            <div class="col-md-4">
                                <h6>{{__('Session Chairman: Dr. Abdulaziz Al Shaibani')}}</h6>
                                <p class="sb-title">{{__('Deputy Minister for Water')}}</p>

                                <h6>{{__('Appointed: Dr. Mishaal Al Harbi')}}</h6>
                                <p class="sb-title">{{__('National Center for Vegetation Development and Combating Desertification..')}}</p>

                            </div>
                        </div>
                    </div>


                    <div class="new-agent-header-row">
                        <div class="row">
                            <div class="col-md-2">
                                <p>{{__('Time')}}</p>
                            </div>
                            <div class="col-md-6">
                                <p>{{__('Title')}}</p>
                            </div>
                            <div class="col-md-4">
                                <p>{{__('Speakers')}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('10:45 - 11:05')}}</p>
                            </div>
                            <div class="col-md-6">
                                <h6>{{__('Afforestation Impacts on Hydrological Ecosystem Services')}}</h6>
                       
                            </div>
                            <div class="col-md-4">
                                <h6>{{__('Prof. Damia Barcelo Culleres')}}</h6>
                                <p class="sb-title">{{__('Spanish National Research Council')}}</p>
                            </div>
                        </div>
                    </div>


                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('11:05 - 11:20')}}</p>
                            </div>
                            <div class="col-md-6">
                                <h6>{{__('The use of non-conventional water sources (gray and treated) in afforestation')}}</h6>
                       
                            </div>
                            <div class="col-md-4">
                                <h6>{{__('Eng. Abdul Mohsen Alfraihi')}}</h6>
                                <p class="sb-title">{{__('Ministry of Environment, Water and Agriculture at Al Gaseem')}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('11:20 -11:35')}}</p>
                            </div>
                            <div class="col-md-6">
                                <h6>{{__('Water irrigation for plants')}}</h6>
                       
                            </div>
                            <div class="col-md-4">
                                <h6>{{__('Dr. Abdullah Al-Rajhi')}}</h6>
                                <p class="sb-title">{{__('King Abdulaziz City for Science and Technology')}}</p>
                            </div>
                        </div>
                    </div>


                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('11:35 -11:50')}}</p>
                            </div>
                            <div class="col-md-6">
                                <h6>{{__('Saltwater Dependent Plants')}}</h6>
                       
                            </div>
                            <div class="col-md-4">
                                <h6>{{__('Dr.Mohammed Muzakr Almutairi')}}</h6>
                                <p class="sb-title">{{__('Ministry of Environment, Water and Agriculture')}}</p>
                            </div>
                        </div>
                    </div>


                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('11:50-12:00')}}</p>
                            </div>
                            <div class="col-md-6">
                                <h6>{{__('Discussion')}}</h6>
                       
                            </div>
                            <div class="col-md-4">
                                {{-- <h6>{{__('M. Mohammed Abdullah Al-Khaled')}}</h6>
                                <p class="sb-title">{{__('Saudi ARAMCO')}}</p> --}}
                            </div>
                        </div>
                    </div>


                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('12:00 -12:30')}}</p>
                            </div>
                            <div class="col-md-6">
                                <h6>{{__('Dhohr Prayer')}}</h6>
                       
                            </div>
                            <div class="col-md-4">
                                {{-- <h6>{{__('Nadine Al-Bita')}}</h6>
                                <p class="sb-title">{{__('The Environment Center in Arab Cities')}}</p> --}}
                            </div>
                        </div>
                    </div>



                    <h1 class="breaking-heading">{{__('Fifth Session A')}}</h1>


                    
                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('12:30 – 1:45')}}</p>
                            </div>
                            <div class="col-md-6">
                                {{-- <h6>{{__('Coffee Break')}}</h6> --}}
                       
                            </div>
                            <div class="col-md-4">
                                <h6>{{__('Session Chairman Zaid Alothman')}}</h6>
                                <p class="sb-title">{{__('Dean of College of Science - King Saud University')}}</p>

                                <h6>{{__('Appointed:Dr. Turki Alturki')}}</h6>
                                <p class="sb-title">{{__('King Abdulazize City for Science and Technology')}}</p>

                            </div>
                        </div>
                    </div>


                    <div class="new-agent-header-row">
                        <div class="row">
                            <div class="col-md-2">
                                <p>{{__('Time')}}</p>
                            </div>
                            <div class="col-md-6">
                                <p>{{__('Title')}}</p>
                            </div>
                            <div class="col-md-4">
                                <p>{{__('Speakers')}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('12:30 - 12:50')}}</p>
                            </div>
                            <div class="col-md-6">
                                <h6>{{__('Methods and Techniques of Soil improvement for the development vegetation cover')}}</h6>
                       
                            </div>
                            <div class="col-md-4">
                                <h6>{{__('Ole Kristian Sivertsen')}}</h6>
                                <p class="sb-title">{{__('CEO of Desert Control')}}</p>
                            </div>
                        </div>
                    </div>


                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('12:50 – 1:05')}}</p>
                            </div>
                            <div class="col-md-6">
                                <h6>{{__('Methods and Techniques of Soil improvement for the development vegetation cover')}}</h6>
                       
                            </div>
                            <div class="col-md-4">
                                <h6>{{__('Dr. Muhammad Ahmad Al-Sanad')}}</h6>
                                <p class="sb-title">{{__('King Faisal University')}}</p>
                            </div>
                        </div>
                    </div>


                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('1:05 – 1:20')}}</p>
                            </div>
                            <div class="col-md-6">
                                <h6>{{__('The role of Sabkah and Wetland for the development of vegetation cover')}}</h6>
                       
                            </div>
                            <div class="col-md-4">
                                <h6>{{__('Dr. Suleiman Al-Khatib')}}</h6>
                                <p class="sb-title">{{__('Deputy Minister of Agriculture')}}</p>
                            </div>
                        </div>
                    </div>


                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('1:20 – 1:35')}}</p>
                            </div>
                            <div class="col-md-6">
                                <h6>{{__('The role of Sabkah and Wetland for the development of vegetation cover')}}</h6>
                       
                            </div>
                            <div class="col-md-4">
                                <h6>{{__('Dr. Khalil Omar')}}</h6>
                                <p class="sb-title">{{__('(ICBA)')}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('1:35 – 1:45')}}</p>
                            </div>
                            <div class="col-md-6">
                                <h6>{{__('Discussion')}}</h6>
                       
                            </div>
                            <div class="col-md-4">
                                {{-- <h6>{{__('Dr. Khalil Omar')}}</h6>
                                <p class="sb-title">{{__('(ICBA)')}}</p> --}}
                            </div>
                        </div>
                    </div>


                    <h1 class="breaking-heading">{{__('Sixth Session B')}}</h1>


                    
                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('12:30 – 1:45')}}</p>
                            </div>
                            <div class="col-md-6">
                                {{-- <h6>{{__('Coffee Break')}}</h6> --}}
                       
                            </div>
                            <div class="col-md-4">
                                <h6>{{__('Session Chairman: Dr. Saud Alrowaily')}}</h6>
                                <p class="sb-title">{{__('Member of Shura')}}</p>

                                <h6>{{__('Appointed: Mr. Kutaiba Alsaadon')}}</h6>
                                <p class="sb-title">{{__('Advisor Ministry of Environment, Water and Agriculture of Saudi Arabia')}}</p>

                            </div>
                        </div>
                    </div>


                    <div class="new-agent-header-row">
                        <div class="row">
                            <div class="col-md-2">
                                <p>{{__('Time')}}</p>
                            </div>
                            <div class="col-md-6">
                                <p>{{__('Title')}}</p>
                            </div>
                            <div class="col-md-4">
                                <p>{{__('Speakers')}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('12:30 - 12:50')}}</p>
                            </div>
                            <div class="col-md-6">
                                <h6>{{__('Afforestation practices in Pakistan')}}</h6>
                       
                            </div>
                            <div class="col-md-4">
                                <h6>{{__('Shabir Hussain')}}</h6>
                                <p class="sb-title">{{__('Pakistan')}}</p>
                            </div>
                        </div>
                    </div>


                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('12:50 – 1:05')}}</p>
                            </div>
                            <div class="col-md-6">
                                <h6>{{__('Afforestation practices in Morocco')}}</h6>
                       
                            </div>
                            <div class="col-md-4">
                                <h6>{{__('Dr. Khaled Al Sharqi')}}</h6>
                                <p class="sb-title">{{__('Morocco')}}</p>
                            </div>
                        </div>
                    </div>


                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('1:05 – 1:20')}}</p>
                            </div>
                            <div class="col-md-6">
                                <h6>{{__('Afforestation practices in Spain')}}</h6>
                       
                            </div>
                            <div class="col-md-4">
                                <h6>{{__('Mr. Fransisco Martinez')}}</h6>
                                <p class="sb-title">{{__('Spain')}}</p>
                            </div>
                        </div>
                    </div>


                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('1:20 – 1:35')}}</p>
                            </div>
                            <div class="col-md-6">
                                <h6>{{__('Afforestation practices in Saudi Arabia')}}</h6>
                       
                            </div>
                            <div class="col-md-4">
                                <h6>{{__('Dr. Abdullah Alburaiki')}}</h6>
                                <p class="sb-title">{{__('National Center for Vegetation Development and Combating Desertification')}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('1:35 – 1:45')}}</p>
                            </div>
                            <div class="col-md-6">
                                <h6>{{__('Discussion')}}</h6>
                       
                            </div>
                            <div class="col-md-4">
                                {{-- <h6>{{__('Dr. Khalil Omar')}}</h6>
                                <p class="sb-title">{{__('(ICBA)')}}</p> --}}
                            </div>
                        </div>
                    </div>

                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('1:45 – 2:00')}}</p>
                            </div>
                            <div class="col-md-6">
                                <h6>{{__('Coffee Break')}}</h6>
                       
                            </div>
                            <div class="col-md-4">
                                {{-- <h6>{{__('Dr. Khalil Omar')}}</h6>
                                <p class="sb-title">{{__('(ICBA)')}}</p> --}}
                            </div>
                        </div>
                    </div>

                 


                    <h1 class="breaking-heading">{{__('First Panel Discussion Room A')}}</h1>
                    <h1 class="breaking-heading">{{__('Investing in national parks and nature reserves')}}</h1>


                    
                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('2:00 – 3:15')}}</p>
                            </div>
                            <div class="col-md-6">
                                {{-- <h6>{{__('Coffee Break')}}</h6> --}}
                       
                            </div>
                            <div class="col-md-4">
                                <h6>{{__('Session Chairman: Dr. Talal Alhuraiki')}}</h6>
                                <p class="sb-title">{{__('The CEO of Imam Abdulaziz bin Mohammed Royal Reserve Development Authority')}}</p>

                                <h6>{{__('Appointed: Ms. Amani Albalawi')}}</h6>
                                <p class="sb-title">{{__('National Center for Vegetation Cover and Combating Desertification')}}</p>

                            </div>
                        </div>
                    </div>


                    <div class="new-agent-header-row">
                        <div class="row">
                            <div class="col-md-2">
                                <p>{{__('Time')}}</p>
                            </div>
                            <div class="col-md-3">
                                <p>{{__('Speaker')}}</p>
                            </div>
                            <div class="col-md-3">
                                <p>{{__('Participants and Guests')}}</p>
                            </div>
                            <div class="col-md-4">
                                <p>{{__('Supervision and organization')}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="row new-agent-child-row">
                        <div class="col-md-2">
                            <p class="time-pra">{{__('2:00 – 2:10')}}</p>
                        </div>
                        <div class="col-md-3">
                            <h6>{{__('Prince Saud bin Nahar Al-Saud Governor of Taif Governorate')}}</h6>
                           
                        </div>
                        <div class="col-md-3">
                             <h6>{{__('Parks Investors Royal Reserve members and Beneficiaries.')}}</h6>
                        </div>

                            <div class="col-md-4">
                                {{-- <h6>{{__('Supervisor Ms. Amani Albalawi')}}</h6> --}}
                                <h6>{{__('Supervisor Ms. Amani Albalawi')}}</h6>
                                <h6>{{__('Mr.Hamad from Imam Abdul Aziz Reserve Development Authority')}}</h6>
                                <h6>{{__('Ms.Wassef Al - Qahtani')}}</h6>
                              
                            </div>
                    </div>


                    <div class="row new-agent-child-row">
                        <div class="col-md-2">
                            <p class="time-pra">{{__('2:10 – 2:20')}}</p>
                        </div>
                        <div class="col-md-3">
                            <h6>{{__('Prince: Haifa Al Saud Ministry of Tourism')}}</h6>
                           
                        </div>
                        <div class="col-md-3">
                             <h6>{{__('Parks Investors Royal Reserve members and Beneficiaries.')}}</h6>
                        </div>

                            <div class="col-md-4">
                                <h6>{{__('Supervisor Ms. Amani Albalawi')}}</h6>
                                <h6>{{__('Mr.Hamad from Imam Abdul Aziz Reserve Development Authority')}}</h6>
                                <h6>{{__('Ms.Wassef Al - Qahtani')}}</h6>
                              
                            </div>
                    </div>
                    <div class="row new-agent-child-row">
                        <div class="col-md-2">
                            <p class="time-pra">{{__('2:20 – 2:30')}}</p>
                        </div>
                        <div class="col-md-3">
                            <h6>{{__('Ms. Kawthar from the Imam Abdulaziz Bin Mohammed Royal Reserve Development Authority')}}</h6>
                           
                        </div>
                        <div class="col-md-3">
                             <h6>{{__('Parks Investors Royal Reserve members and Beneficiaries.')}}</h6>
                        </div>

                            <div class="col-md-4">
                                <h6>{{__('Supervisor Ms. Amani Albalawi')}}</h6>
                                <h6>{{__('Mr.Hamad from Imam Abdul Aziz Reserve Development Authority')}}</h6>
                                <h6>{{__('Ms.Wassef Al - Qahtani')}}</h6>
                              
                            </div>
                    </div>

                    <div class="row new-agent-child-row">
                        <div class="col-md-2">
                            <p class="time-pra">{{__('2:30 – 2:40')}}</p>
                        </div>
                        <div class="col-md-3">
                            <h6>{{__('Dr. Muhammad Mufrah')}}</h6>
                           
                        </div>
                        <div class="col-md-3">
                            <h6>{{__('Parks Investors Royal Reserve members and Beneficiaries.')}}</h6>
                        </div>

                            <div class="col-md-4">
                                {{-- <h6>{{__('Supervisor Ms. Amani Albalawi')}}</h6>
                                <h6>{{__('Mr.Hamad from Imam Abdul Aziz Reserve Development Authority')}}</h6>
                                <h6>{{__('Ms.Wassef Al - Qahtani')}}</h6> --}}
                            </div>
                    </div>


                    <div class="row new-agent-child-row">
                        <div class="col-md-2">
                            <p class="time-pra">{{__('2:40 – 3:25')}}</p>
                        </div>
                        <div class="col-md-3">
                            <h6>{{__('Open Panel Discussion.')}}</h6>
                           
                        </div>
                        <div class="col-md-3">
                             {{-- <h6>{{__('Parks Investors Royal Reserve
                                members and Beneficiaries.')}}</h6> --}}
                        </div>

                            <div class="col-md-4">
                                {{-- <h6>{{__('Supervisor Ms. Amani Albalawi')}}</h6>
                                <h6>{{__('Mr. Hamad from Imam Abdul Aziz Reserve Development Authority
                                    Ms. Wassef Al-Qahtani')}}</h6> --}}
                              
                            </div>
                    </div>




                    
                    <h1 class="breaking-heading">{{__('Second Panel Discussion')}} {{__('Room: B')}}</h1>
                    <h1 class="breaking-heading">{{__('The role of green association in the afforestation of KSA')}} </h1>


                    
                    <div class="new-agent-child-row">
                        <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="time-pra">{{__('2:00 – 3:15')}}</p>
                            </div>
                            <div class="col-md-6">
                                {{-- <h6>{{__('Coffee Break')}}</h6> --}}
                       
                            </div>
                            <div class="col-md-4">
                                <h6>{{__('Session Chairman: Prience Moteb bin Fahad Al Saud')}}</h6>
                                <p class="sb-title">{{__('The President of Environmental Association& Combat Desertification and Weather and Climate Saudi Association.')}}</p>

                                <h6>{{__('Appointed: Dr. Abdulrahman Alseqair')}}</h6>
                                <p class="sb-title">{{__('Qassim University')}}</p>

                            </div>
                        </div>
                    </div>


                    <div class="new-agent-header-row">
                        <div class="row">
                            <div class="col-md-2">
                                <p>{{__('Time')}}</p>
                            </div>
                            <div class="col-md-3">
                                <p>{{__('Speaker')}}</p>
                            </div>
                            <div class="col-md-3">
                                <p>{{__('Participants and Guests')}}</p>
                            </div>
                            <div class="col-md-4">
                                <p>{{__('Supervision and organization')}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="row new-agent-child-row">
                        <div class="col-md-2">
                            <p class="time-pra">{{__('2:00 – 2:10')}}</p>
                        </div>
                        <div class="col-md-3">
                            <h6>{{__('Talal Al-Rasheed, Environment Friend Association in Sharqia')}}</h6>
                           
                        </div>
                        <div class="col-md-3">
                             <h6>{{__('All relevant Associations')}}</h6>
                        </div>

                            <div class="col-md-4">
                                <h6>{{__('Supervisor: Dr. Abdulrahman')}}</h6>
                                <h6>{{__('Alseqair')}}</h6>
                                <h6>{{__('Ms. Nouf Aldrehm')}}</h6>
                                <h6>{{__('Ms. Afnan Alotaibi')}}</h6>
                              
                            </div>
                    </div>

                    <div class="row new-agent-child-row">
                        <div class="col-md-2">
                            <p class="time-pra">{{__('2:10 – 2:20')}}</p>
                        </div>
                        <div class="col-md-3">
                            <h6>{{__('M. Saud Al-Zahrani, Al-Baha Albaha Association')}}</h6>
                           
                        </div>
                        <div class="col-md-3">
                             <h6>{{__('All relevant Associations')}}</h6>
                        </div>

                            <div class="col-md-4">
                                <h6>{{__('Supervisor: Dr. Abdulrahman')}}</h6>
                                <h6>{{__('Alseqair')}}</h6>
                                <h6>{{__('Ms. Nouf Aldrehm')}}</h6>
                                <h6>{{__('Ms. Afnan Alotaibi')}}</h6>
                              
                            </div>
                    </div>


                    <div class="row new-agent-child-row">
                        <div class="col-md-2">
                            <p class="time-pra">{{__('2:20 – 2:30')}}</p>
                        </div>
                        <div class="col-md-3">
                            <h6>{{__('Dr Abdullah Alfahad, Environment Friends Association in Zulfi')}}</h6>
                           
                        </div>
                        <div class="col-md-3">
                             <h6>{{__('All relevant Associations')}}</h6>
                        </div>

                            <div class="col-md-4">
                                <h6>{{__('Supervisor: Dr. Abdulrahman')}}</h6>
                                <h6>{{__('Alseqair')}}</h6>
                                <h6>{{__('Ms. Nouf Aldrehm')}}</h6>
                                <h6>{{__('Ms. Afnan Alotaibi')}}</h6>
                              
                            </div>
                    </div>


                    <div class="row new-agent-child-row">
                        <div class="col-md-2">
                            <p class="time-pra">{{__('2:30 - 2:40')}}</p>
                        </div>
                        <div class="col-md-3">
                            <h6>{{__('Mustafa Ismail Alawi Morocco')}}</h6>
                           
                        </div>
                        <div class="col-md-3">
                             <h6>{{__('All relevant Associations')}}</h6>
                        </div>

                            <div class="col-md-4">
                                <h6>{{__('Supervisor: Dr. Abdulrahman')}}</h6>
                                <h6>{{__('Alseqair')}}</h6>
                                <h6>{{__('Ms. Nouf Aldrehm')}}</h6>
                                <h6>{{__('Ms. Afnan Alotaibi')}}</h6>
                              
                            </div>
                    </div>


                    <div class="row new-agent-child-row">
                        <div class="col-md-2">
                            <p class="time-pra">{{__('2:40 – 3:15')}}</p>
                        </div>
                        <div class="col-md-3">
                            <h6>{{__('Open Panel Discussion')}}</h6>
                           
                        </div>
                        <div class="col-md-3">
                             {{-- <h6>{{__('All relevant Associations')}}</h6> --}}
                        </div>

                            <div class="col-md-4">
                                {{-- <h6>{{__('Supervisor: Dr. Abdulrahman Alseqair')}}</h6>
                                <h6>{{__('Ms. Nouf Aldrehm')}}</h6>
                                <h6>{{__('Ms. Afnan Alotaibi')}}</h6> --}}
                              
                            </div>
                    </div>


                    <div class="row new-agent-child-row">
                        <div class="col-md-2">
                            <p class="time-pra">{{__('3:15 -4:00')}}</p>
                        </div>
                        <div class="col-md-3">
                            <h6>{{__('Lunch Break')}}</h6>
                           
                        </div>
                        <div class="col-md-3">
                             {{-- <h6>{{__('All relevant Associations')}}</h6> --}}
                        </div>

                            <div class="col-md-4">
                                {{-- <h6>{{__('Supervisor: Dr. Abdulrahman Alseqair')}}</h6>
                                <h6>{{__('Ms. Nouf Aldrehm')}}</h6>
                                <h6>{{__('Ms. Afnan Alotaibi')}}</h6> --}}
                              
                            </div>
                    </div>



                






                        </div>
                        <div class="tab-pane " id="tabs-3" role="tabpanel">

                            <br>
                            <h1 class="breaking-heading">{{__('First Session A')}}</h1>

                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('9:15 – 10:30')}}</p>
                                    </div>
                                    <div class="col-md-6">
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('Session Chairman: Dr. Ayman Gholam')}}</h6>
                                        <p class="sb-title">{{__('CEO of the National Center for Meteorology')}}</p>

                                        <h6>{{__('Appointed:Eng. Ahmed Al Nuaimi')}}</h6>
                                        <p class="sb-title">{{__('National Center for Vegetation Development and Combating Desertification')}}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="new-agent-header-row">
                                <div class="row">
                                    <div class="col-md-2">
                                        <p>{{__('Time')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p>{{__('Title')}}</p>
                                    </div>
                                    <div class="col-md-4">
                                        <p>{{__('Speakers')}}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('9:15 –9:35')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Rainwater harvesting techniques and methods suitable in Dryland')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('prof. Abdulaziz Alturbag')}}</h6>
                                        <p class="sb-title">{{__('King Saud University')}}</p>
                                    </div>
                                </div>
                            </div>


                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('9:35– 9:50')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Rainwater harvesting techniques and methods suitable in Dryland')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('Mr. Dr. Mohamed Taher Al-Barqawi')}}</h6>
                                        <p class="sb-title">{{__('ICSAD')}}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('9:50 – 10:05')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Artificial rain techniques to enhance water resources in dryland')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('Eng. Ayman Albar')}}</h6>
                                        <p class="sb-title">{{__('National Center for Meteorology')}}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('10:05 - 10:20')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Artificial rain techniques to enhance water resources in dryland')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('Kucera Paul')}}</h6>
                                        <p class="sb-title">{{__('National Center for Meteorology')}}</p>
                                    </div>
                                </div>
                            </div>


                            <div class="new-agent-child-row">
                                {{-- <img src="{{ asset('assets/img/new-image/user.png') }}" alt=""> --}}
                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('10:20 – 10:30')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Discussion')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        {{-- <h6>{{__('Paulo Pereira')}}</h6>
                                        <p class="sb-title">{{__('Mykolas Romeris University, Lithuania')}}</p> --}}
                                    </div>
                                </div>
                            </div>

                            <h1 class="breaking-heading">{{__('Second Session B')}}</h1>

                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('9:15 – 10:30')}}</p>
                                    </div>
                                    <div class="col-md-6">
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('Session Chairman: : Dr. Thobet Al Shahrani')}}</h6>
                                        <p class="sb-title">{{__('Member of the Board Directors of the National Center for Vegetation Development and Combating Desertification')}}</p>

                                        <h6>{{__('Appointed:Eng. Mossa Alowa')}}</h6>
                                        <p class="sb-title">{{__('National Center for Vegetation Development and Combating Desertification')}}</p>
                                    </div>
                                </div>
                            </div>


                            <div class="new-agent-header-row">
                                <div class="row">
                                    <div class="col-md-2">
                                        <p>{{__('Time')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p>{{__('Title')}}</p>
                                    </div>
                                    <div class="col-md-4">
                                        <p>{{__('Speakers')}}</p>
                                    </div>
                                </div>
                            </div>


                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('9:15 - 9:35')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('The role of Local communities in the development of vegetation cover')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('Dr. Laila Saeed Al Harthy Amman')}}</h6>
                                        <p class="sb-title">{{__('Oman')}}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('9:35 - 9:50')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('The role of Local communities in the development of vegetation cover')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('Shabir Hussain')}}</h6>
                                        <p class="sb-title">{{__('Pakistan')}}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('9:50 – 10:05')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('International experiences in community participation in tree planting initiatives')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('Dr. Abdul Hamid Hamed')}}</h6>
                                        <p class="sb-title">{{__('FAO')}}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="new-agent-child-row">
                                <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('10:05 – 10:20')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Afforestation and development of local plant communities')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{__('Issam Ahbri')}}</h6>
                                        <p class="sb-title">{{__('Morocco')}}</p>
                                    </div>
                                </div>
                            </div>


                            <div class="new-agent-child-row">
                                {{-- <img src="{{ asset('assets/img/new-image/user.png') }}" alt=""> --}}
                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('10:20 - 10:30')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Discussion')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        {{-- <h6>{{__('Dr. Abdulaziz Al-Saeed')}}</h6>
                                        <p class="sb-title">{{__('Board Member of National Center for Vegetation Development and Combating Desertification')}}</p> --}}
                                    </div>
                                </div>
                            </div>


                            <div class="new-agent-child-row">
                                {{-- <img src="{{ asset('assets/img/new-image/user.png') }}" alt=""> --}}
                                <div class="row">
                                    <div class="col-md-2">
                                        <p class="time-pra">{{__('10:30 – 10:45')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>{{__('Coffee Break')}}</h6>
                               
                                    </div>
                                    <div class="col-md-4">
                                        {{-- <h6>{{__('Dr. Abdulaziz Al-Saeed')}}</h6>
                                        <p class="sb-title">{{__('Board Member of National Center for Vegetation Development and Combating Desertification')}}</p> --}}
                                    </div>
                                </div>
                            </div>

                        <h1 class="breaking-heading">{{__('Third Session A')}}</h1>


                        
                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:45 - 12:00')}}</p>
                                </div>
                                <div class="col-md-6">
                                    {{-- <h6>{{__('Coffee Break')}}</h6> --}}
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman: Eng. Hashim Al-Dabbagh')}}</h6>
                                    <p class="sb-title">{{__('Head of Aseer Region Development Authority')}}</p>

                                    <h6>{{__('Appointed: Dr. Areej Alosaimi')}}</h6>
                                    <p class="sb-title">{{__('Imam Abdulrahman Bin Faisal University')}}</p>

                                </div>
                            </div>
                        </div>


                        <div class="new-agent-header-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <p>{{__('Time')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <p>{{__('Title')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>{{__('Speakers')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:45 - 11:05')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Agroforestry (integrated agriculture) and its environmental, economic and social roles')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Hamed Daly-Hassen')}}</h6>
                                    <p class="sb-title">{{__('Director General of National Observatory Agriculture')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:05 - 11:20')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Agricultural Terraces rehabilitation experience')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Mr. Saud AlKhial')}}</h6>
                                    <p class="sb-title">{{__('Ministry of Environment, Water and Agriculture of Saudi Arabia')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:20- 11:35')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Agricultural Terraces rehabilitation experience')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Mohammed Al-Wassar')}}</h6>
                                    <p class="sb-title">{{__('Institute Des Regions arides')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:35 -11:50')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Wild plant pests')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Hathal Al Dhafer')}}</h6>
                                    <p class="sb-title">{{__('King Saud University')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:50 - 12:00')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Discussion')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Nadine Al-Bita')}}</h6>
                                    <p class="sb-title">{{__('The Environment Center in Arab Cities')}}</p> --}}
                                </div>
                            </div>
                        </div>




                        <h1 class="breaking-heading">{{__('Fourth Session B')}}</h1>


                        
                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:45 – 12:00')}}</p>
                                </div>
                                <div class="col-md-6">
                                    {{-- <h6>{{__('Coffee Break')}}</h6> --}}
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman: Osama Al Khalawi')}}</h6>
                                    <p class="sb-title">{{__('Member of the Board Directors of the National Center for Vegetation Development and Combating Desertification')}}</p>

                                    <h6>{{__('Appointed: Dr.Saleh Al Zamanan')}}</h6>
                                    <p class="sb-title">{{__('National Center for Vegetation Development and Combating Desertification')}}</p>

                                </div>
                            </div>
                        </div>


                        <div class="new-agent-header-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <p>{{__('Time')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <p>{{__('Title')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>{{__('Speakers')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('10:45 - 11:05')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Designing and landscaping residential and recreational parks')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Ashraf Al Turki')}}</h6>
                                    <p class="sb-title">{{__('King Abdulaziz University')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:05 - 11:20')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('The use of wild plants in the design and landscaping of outdoor spaces')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Prof. Dr. Omar bin Salem bin Omar Bahamam')}}</h6>
                                    <p class="sb-title">{{__('King SaUD uNIVERSITY')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:20 - 11:35')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('The role of afforestation in contributing to the strengthening of infrastructure in cities')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Eng. Andul Aziz Al Mogbel')}}</h6>
                                    <p class="sb-title">{{__('The Royal Commission for Riyadh City')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:35 - 11:50')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Biological methods using plants to stabilize sand dunes')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Prof. Dr. Majdi Al-Banna')}}</h6>
                                    <p class="sb-title">{{__('Royal Commission for Al-Ula')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('11:50 - 12:00')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Discussion')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Eng. Andul Aziz Al Mogbel')}}</h6>
                                    <p class="sb-title">{{__('The Royal Commission for Riyadh City')}}</p> --}}
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:00 - 12:30')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Dhohr Prayer')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Nadine Al-Bita')}}</h6>
                                    <p class="sb-title">{{__('The Environment Center in Arab Cities')}}</p> --}}
                                </div>
                            </div>
                        </div>



                        <h1 class="breaking-heading">{{__('Fifth Session A')}}</h1>


                        
                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:30 – 1:45')}}</p>
                                </div>
                                <div class="col-md-6">
                                    {{-- <h6>{{__('Coffee Break')}}</h6> --}}
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman: Eng. Abdul Rahman Al Zughaibi')}}</h6>
                                    <p class="sb-title">{{__('Deputy Ministry for Economic Affairs and Investment')}}</p>

                                    <h6>{{__('Appointed: Dr. Fahad Almutlaq')}}</h6>
                                    <p class="sb-title">{{__('Geographic Information Science and Technology')}}</p>

                                </div>
                            </div>
                        </div>


                        <div class="new-agent-header-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <p>{{__('Time')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <p>{{__('Title')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>{{__('Speakers')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:30 - 12:50')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Indicators of Successful Afforestation and Reforestation practices')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Christopher Clarke')}}</h6>
                                    <p class="sb-title">{{__('King Abdullah University for Science and Technology')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:50 – 1:05')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('The role of afforestation in non-wood products and environmental services economic valuation')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Gassan Bakri')}}</h6>
                                    <p class="sb-title">{{__('Reef Program')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:05 – 1:20')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Investment in multipurpose plant species')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Saadia Zarira')}}</h6>
                                    <p class="sb-title">{{__('Alhassan 2 for Agriculture and Veterinary')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:20 – 1:35')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Using of environmentally friendly techniques to rationalize irrigation')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Ole Kristian Sivertsen')}}</h6>
                                    <p class="sb-title">{{__('CEO of Desert Control')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:35 – 1:45')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Discussion')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Dr. Khalil Omar')}}</h6>
                                    <p class="sb-title">{{__('(ICBA)')}}</p> --}}
                                </div>
                            </div>
                        </div>


                        <h1 class="breaking-heading">{{__('Sixth Session B')}}</h1>


                        
                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:30 – 1:45')}}</p>
                                </div>
                                <div class="col-md-6">
                                    {{-- <h6>{{__('Coffee Break')}}</h6> --}}
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Session Chairman: Brigadier General Saher Alharbi')}}</h6>
                                    <p class="sb-title">{{__('Special Forces for Environmental Security')}}</p>

                                    <h6>{{__('Appointed: Ms. Maha Saud Alotaibi')}}</h6>
                                    <p class="sb-title">{{__('Commander of Ministry of Environment, Water and Agriculture of Saudi Arabia')}}</p>

                                </div>
                            </div>
                        </div>


                        <div class="new-agent-header-row">
                            <div class="row">
                                <div class="col-md-2">
                                    <p>{{__('Time')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <p>{{__('Title')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>{{__('Speakers')}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:30 - 12:50')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('The role of regulations and legislation in Stimulating Investment in vegetation cover development')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Tony Charter')}}</h6>
                                    <p class="sb-title">{{__('Australia')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('12:50 – 1:05')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Green Kingdom Studies Methodology and Targets')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Eng. Khalid bin Abdulrahman AlOthman')}}</h6>
                                    <p class="sb-title">{{__('Head of Emaar Engineering Consultancy Company')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:05 – 1:20')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('The role of regulations and legislation in Stimulating Investment in vegetation cover development')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Dr. Nora Al Hamad')}}</h6>
                                    <p class="sb-title">{{__('National Center for Vegetation Cover Development and Combating Desertification')}}</p>
                                </div>
                            </div>
                        </div>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:20 – 1:30')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Rules and regulations in the protection of vegetation cover')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    <h6>{{__('Major General. Ali Al-Asmari')}}</h6>
                                    <p class="sb-title">{{__('National Center for Vegetation Development and Combating Desertification')}}</p>
                                </div>
                            </div>
                        </div>
                        

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:35 – 1:45')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Discussion')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Dr. Khalil Omar')}}</h6>
                                    <p class="sb-title">{{__('(ICBA)')}}</p> --}}
                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('1:45 – 2:00')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Coffee Break')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Dr. Khalil Omar')}}</h6>
                                    <p class="sb-title">{{__('(ICBA)')}}</p> --}}
                                </div>
                            </div>
                        </div>



                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('2:00 - 2:30')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Honoring Participants and Organizers')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                    {{-- <h6>{{__('Dr. Khalil Omar')}}</h6>
                                    <p class="sb-title">{{__('(ICBA)')}}</p> --}}
                                </div>
                            </div>
                        </div>


<h1 class="breaking-heading">{{__('Recommendations Committee A')}}</h1>


                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('2:30 – 3:30')}}</p>
                                </div>
                                <div class="col-md-6">
                                    {{-- <h6>{{__('Honoring Participants and Organizers')}}</h6> --}}
                           
                                </div>
                                <div class="col-md-4">
                                     <h6>{{__('Session Chairman:Dr. Ahmed Alfarhan')}}</h6>
                                   <p class="sb-title">{{__('Scientific Committee Chairman &Member of the Board Directors of the National Center for Vegetation Development and Combating Desertification')}}</p>

                                   <h6>{{__('Appointed Dr. Abdullah Alhajoj')}}</h6>
                                   <p class="sb-title">{{__('Regional Manager of Rangeland at National Center for Vegetation Cover Development & Combating Desertification')}}</p>

                                </div>
                            </div>
                        </div>

                        <div class="new-agent-child-row">
                            <img src="{{ asset('assets/img/new-image/user.png') }}" alt="">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="time-pra">{{__('3:30 – 4:00')}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>{{__('Lunch Break')}}</h6>
                           
                                </div>
                                <div class="col-md-4">
                                     {{-- <h6>{{__('Session Chairman:Dr. Ahmed Alfarhan')}}</h6>
                                   <p class="sb-title">{{__('Scientific Committee Chairman &Member of the Board Directors of the National Center for Vegetation Development and Combating Desertification')}}</p>

                                   <h6>{{__('Appointed Dr. Abdullah Alhajoj')}}</h6>
                                   <p class="sb-title">{{__('Regional Manager of Rangeland at National Center for Vegetation Cover Development & Combating Desertification')}}</p> --}}

                                </div>
                            </div>
                        </div>



            
                        </div>
                    </div>
                </div>
                  
                 
            </div>

           

        </div>

@endsection
