<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\EventAttendees;
use App\Models\Scanqr;
use App\Models\ScanqrUser;
use App\Models\User;
use App\Models\Workshop;
use App\Models\WorkshopAttendees;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;

class ApiController extends Controller
{
    public function userdetail($qrcodeid){
        $response=[];
        $msg=[];
        // return response()->json(['error'=>"user Found"], 200);
        $userdetail = User::where('qrcodeid', $qrcodeid)->first();
        

        if(!isset($userdetail)){

        $msg[]='User Not Found';
        return response()->json([
            'msg' => $msg,
        ],404); 
        }
        if($userdetail->qrcodestatus==0){
            $userdetail->qrcodestatus = 1;
            $userdetail->save();
        }
        else{
            $userdetail->qrcodestatus += 1;
            $userdetail->save();
        }
           $msg[]='User Found';
        $response[]=$userdetail;
           return response()->json([
            'result' => $response,
            'msg'=> $msg,
        ],200);
         
    }
    public function qrscan($userid,$title){
        $msg=[];
        $user = User::find($userid);

        if(!isset($user)){
            $msg[]='User Not Found';
            return response()->json([
                'msg' => $msg,
            ],404);
        }
        DB::beginTransaction();
    try {
        // $user = User::find($userid);
        
        
        if($title == "event"){
            $event = new EventAttendees();
            $event->user_id = $userid;
            $event->date = now()->format('Y-m-d');

            $event->save();
        DB::commit();
        $msg[]='Scanned Entry Added';
           return response()->json([
            'msg'=> $msg,
        ],200);

        }
        if($title=="workshop"){
            $workshop = new WorkshopAttendees();
            $workshop->user_id = $userid;
            $workshop->date = now()->format('Y-m-d');
            $workshop->save();
        DB::commit();
        $msg[]='Scanned Entry Added';
           return response()->json([
            'msg'=> $msg,
        ],200);
        }
        $msg[]='Enter url Properly';
           return response()->json([
            'msg'=> $msg,
        ],404);
        
            // all good
    }
        
          
        catch (\Exception $e) {
            DB::rollback();
            $msg[]='Something happened wrong';
            return response()->json([
                'msg' => $msg,
            ],401);
            
            // something went wrong
        }
    }
        
}
