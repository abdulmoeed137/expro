<?php

namespace App\Http\Controllers;

use App\Models\Invalidemail;
use Illuminate\Http\Request;

class EmailController extends Controller
{
    public function sendEmail(){
        dispatch(new ReminderEmail)->delay(now()->addSecond());
        return response()->json(['message'=>'Email Sent Succesfully']);
    }
    public function Send_Email_Qrcopy(Request $request){
        $randomNumber = random_int(100000, 999999);

        $details = [

            'qrcodeid' => $request->qrcodehiddenfield,
           
        ];
        try{
            $mail = \Mail::to($request->emailofmodal)->send(new \App\Mail\RegisterMail($details));
            if (\App::getLocale() == "en") {
                return response()->json(['message'=>'Email Sent Succesfully']);
        
                        // toastr()->success('You are Successfully Registered');
                        // return redirect()->route('register');
                    }
                return response()->json(['message'=>'تم التسجيل بنجاح']);
            //code to send the mail
         
         }catch(\Exception $e){
            //  dd("hello");
            // $data = new Invalidemail();
            // $data->name = $request->name;
            // $data->email = $request->email;
            // $data->phonenumber = $request->phonenumber;
            // $data->companyname = $request->companyname;
            // $data->jobtitle = $request->jobtitle;
            // $data->qrcodeid = $randomNumber;
            // $data->save();
    
            if (\App::getLocale() == "en") {
        return response()->json(['message'=>'Email Sent Succesfully']);

                // toastr()->success('You are Successfully Registered');
                // return redirect()->route('register');
            }
        return response()->json(['message'=>'تم التسجيل بنجاح']);
        // return response()->json(['message'=>'Email Sent Succesfully','status'=>200]);


            // toastr()->success('تم التسجيل بنجاح');
            // return redirect()->route('register');
        //    dd("there is some error");
         }
        // dd($request->emailofmodal);
    }
}
