<?php

namespace App\Http\Controllers;

use App\Models\Agenda;
use App\Models\Consultancy;
use App\Models\EventAttendees;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Registration;
use App\Models\User;
use App\Models\MeetingLink;
use App\Models\Register;
use App\Models\Session;
use App\Models\Setting;
use App\Models\Speaker;
use App\Models\Sponsor;
use App\Models\WorkshopAttendees;
use Hash;


class AdminController extends Controller
{
    public function edit()
    {
        $user = auth()->user();
        return view('Admin.edit', compact('user'));
    }

    public function update(Request $request)
    {
        $user = auth()->user();
        $rules = [
            'name' => 'required|string|min:2|max:255',
            'email' => 'required|email:filter|max:255|unique:users,email'
        ];
        $msg = 'Profile info updated successfully';
        if ($request->hasAny(['old_password', 'password', 'confirmation_password'])) {
            $rules = [
                'old_password' => 'required|min:8|max:255',
                'password' => 'required|min:8|max:255',
            ];
            $msg = 'Profile password changed successfully';
        }
        $validated = $request->validate($rules);
        if ($request->hasAny(['old_password', 'password', 'confirmation_password'])) {
            $user->update(['password' => Hash::make($validated['password'])]);
        } else {
            $user->update([
                'name' => $validated['name'],
                'email' => $validated['email']
            ]);
        }
        toastr()->success($msg);
        return back();
    }
    public function dashboard()
    {
        // $from = Date('2022-05-29');
        // $to = Date('2022-06-1');
        // ---------------------------------------Graph-----------------------------------------
        $per_day_registrations = DB::table('registers')->select('created_at', DB::raw("COUNT(id) as users_count"))->groupBy(DB::raw("DATE(created_at)"))->get();
// dd("this is here");
        $label = [];
        $graph_data = [];
        $colours = [];
        
        foreach ($per_day_registrations as $per_day_attendant) {
            $label[] = date('d', strtotime($per_day_attendant->created_at)) . '-' . date('M', strtotime($per_day_attendant->created_at));
            $graph_data[] = $per_day_attendant->users_count;
            // $name[] = $per_day_attendant->name;
            $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
        }

        $chart = new Register();
        $chart->labels = ($label);
        $chart->dataset = ($graph_data);
        $chart->colours = $colours;
        // $chart->name = ($name);
        // ---------------------------------------Graph-----------------------------------------

               // ---------------------------------------Graph2-----------------------------------------
            //    $reference_registrations = DB::table('consultancies')->select('created_at', DB::raw("COUNT(id) as users_count"))->groupBy(DB::raw("DATE(created_at)"))->get();
              
            //            $label = [];
            //            $graph_data = [];
            //            $colours = [];
                       
            //            foreach ($reference_registrations as $per_reference_attendant) {
            //             $label[] = date('d', strtotime($per_reference_attendant->created_at)) . '-' . date('M', strtotime($per_reference_attendant->created_at));
            //                $graph_data[] = $per_reference_attendant->users_count;
                           
            //                $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
            //            }
               
            //            $chart2 = new Consultancy();
            //            $chart2->labels = ($label);
            //            $chart2->dataset = ($graph_data);
            //            $chart2->colours = $colours;
                       
                       // ---------------------------------------Graph2-----------------------------------------
               // ---------------------------------------Graph3-----------------------------------------
            //    $event_registrations = DB::table('event_attendees')->select('created_at', DB::raw("COUNT(id) as users_count"))->whereBetween('created_at', [$from, $to])->groupBy(DB::raw("DATE(created_at)"))->get();
              
            //            $label = [];
            //            $graph_data = [];
            //            $colours = [];
                       
            //            foreach ($event_registrations as $event_registrations) {
            //             $label[] = date('d', strtotime($event_registrations->created_at)) . '-' . date('M', strtotime($event_registrations->created_at));
            //                $graph_data[] = $event_registrations->users_count;
            //                // $name[] = $per_day_attendant->name;
            //                $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
            //            }
               
            //            $chart3 = new EventAttendees();
            //            $chart3->labels = ($label);
            //            $chart3->dataset = ($graph_data);
            //            $chart3->colours = $colours;
                       
                       // ---------------------------------------Graph3-----------------------------------------
               // ---------------------------------------Graph4-----------------------------------------
               
            
            //    $workshop_registrations = DB::table('workshop_attendees')->select('created_at', DB::raw("COUNT(id) as users_count"))->whereBetween('created_at', [$from, $to])->groupBy(DB::raw("DATE(created_at)"))->get();
           
            //            $label = [];
            //            $graph_data = [];
            //            $colours = [];
                       
            //            foreach ($workshop_registrations as $workshop_registrations) {
            //             $label[] = date('d', strtotime($workshop_registrations->created_at)) . '-' . date('M', strtotime($workshop_registrations->created_at));
            //                $graph_data[] = $workshop_registrations->users_count;
            //                // $name[] = $per_day_attendant->name;
            //                $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
            //            }
               
            //            $chart4 = new WorkshopAttendees();
            //            $chart4->labels = ($label);
            //            $chart4->dataset = ($graph_data);
            //            $chart4->colours = $colours;
                      
                       // ---------------------------------------Graph4-----------------------------------------
               

        $registrations = Register::count();
        // $consultancycount = Consultancy::count();
        // $eventattendeescount = EventAttendees::whereBetween('created_at', [$from, $to])->count();
        // $workshopattendeescount = WorkshopAttendees::whereBetween('created_at', [$from, $to])->count();
        // $submissions = Submission::count();
        // $sessions = Session::count();
        // $writers = Writer::count();
        // $users = User::count();
        return view('Admin.dashboard',compact('chart','registrations'));
    }
    public function Registrations()
    {
        $registrations = Register::all();

        // dd(Submission::with('user')->get());
        // $ids = $submission->pluck('id')->toArray();
        // $templates = Template::where('status', 1)->orderBy('name')->latest()->get();
        // if ($request->ajax()) {
        //     return \DataTables::of($submission)
        //         ->addColumn('selection', function ($row) {
        //             return "<input type='checkbox' class='sender' name='user" . $row->id . "' id='user" . $row->id . "' value='" . $row->id . "' >";
        //         })

        //         ->addColumn('created_at', function ($row) {
        //             return $row->created_at->format('d-M-Y H:i a');
        //         })
        //         ->rawColumns(['selection'])
        //         ->make(true);
        // }
        return view('Admin.Registration.registration', compact('registrations'));
    }
     // ----------------------------------------Speakers---------------------------------------

     public function speakers()
     {
         $speakers = Speaker::withTrashed()->latest()->get();
         // dd($speakers);
         return view('Admin.Speaker.index', compact('speakers'));
     }
 
     public function speakerForm()
     {
         return view('Admin.Speaker.create');
     }
 
     public function speakerCreate(Request $request)
     {
         $request->validate([
             'image' => 'required|file|mimes:png,jpg',
             'ar_name' => 'required|string|max:255',
             'ar_title' => 'required|string|max:255',
             'ar_description' => 'string',
             'en_name' => 'required|string|max:255',
             'en_title' => 'required|string|max:255',
             'en_description' => 'string',
         ]);
         $speaker = new Speaker();
         $name = md5($request->en_name . time()) . ".{$request->image->getClientOriginalExtension()}";
         $path = $request->image->storeAs("public/speakers", $name);
         $speaker->image = str_replace('public', 'storage', $path);
         $speaker->ar = [
             'name' => $request->ar_name,
             'title' => $request->ar_title,
             'description' => $request->ar_description,
         ];
         $speaker->en = [
             'name' => $request->en_name,
             'title' => $request->en_title,
             'description' => $request->en_description,
         ];
 
         $speaker->save();
         toastr()->success("Speaker created successfully");
 
         // $this->sendPasswordEmail($data['email'], $password);
         return redirect()->to(route('admin.speakers'));
     }
 
     public function editSpeaker($id)
     {
         $speaker = Speaker::findOrFail($id);
         return view('Admin.Speaker.update', compact('speaker'));
     }
 
     public function updateSpeaker(Request $request, $id)
     {
         $speaker = Speaker::findOrFail($id);
         $request->validate([
             'image' => 'nullable|file|mimes:png,jpg',
             'ar_name' => 'required|string|max:255',
             'ar_title' => 'required|string|max:255',
             'ar_description' => 'string',
             'en_name' => 'required|string|max:255',
             'en_title' => 'required|string|max:255',
             'en_description' => 'string',
         ]);
 
         if ($request->has('image')) {
             $url = public_path("/storage/speakers/");
             $parts = explode('/', $speaker->image);
             $url .= end($parts);
             @unlink($url);
 
             $name = md5($request->en_name . time()) . ".{$request->image->getClientOriginalExtension()}";
             $path = $request->image->storeAs("public/speakers", $name);
             $speaker->image = str_replace('public', 'storage', $path);
         }
         $speaker->ar = [
             'name' => $request->ar_name,
             'title' => $request->ar_title,
             'description' => $request->ar_description,
         ];
         $speaker->en = [
             'name' => $request->en_name,
             'title' => $request->en_title,
             'description' => $request->en_description,
         ];
 
         $speaker->save();
         toastr()->success("Speaker has successfully updated");
         return redirect()->route('admin.speakers')->with('message','Speaker has successfully updated');
     }
 
     public function deleteSpeakers(Speaker $speaker)
     {
         $speaker->delete();
         toastr()->success("Speaker deleted successfully");
         return back()->with('message','Speaker deleted successfully');
     }
     public function restoreSpeakers($id)
     {
         $speaker = Speaker::withTrashed()->findOrFail($id);
         $speaker->restore();
         toastr()->success("Speaker restored successfully");
         return back()->with('message','Speaker restored successfully');
     }

       // ----------------------------Sessions---------------------------------

    public function sessions()
    {
        $sessions = Session::withTrashed()->latest()->get();
        // dd($speakers);
        return view('Admin.Session.index', compact('sessions'));
    }

    public function sessionForm()
    {
        return view('Admin.Session.create');
    }

    public function sessionCreate(Request $request)
    {
        $request->validate([
            // 'image' => 'nullable|file|mimes:png,jpg',
            'ar_name' => 'required|string|max:255',
            'ar_title' => 'required|string|max:255',
            'ar_description' => 'required|string',
            'en_name' => 'required|string|max:255',
            'en_title' => 'required|string|max:255',
            'en_description' => 'required|string',
        ]);
        $session = new Session();
        // if ($request->has('image')) {
        //     $name = md5($request->en_name . time()) . ".{$request->image->getClientOriginalExtension()}";
        //     $path = $request->image->storeAs("public/Sessions", $name);
        //     $session->image = str_replace('public', 'storage', $path);
        // }else{
        //     $session->image = null;

        // }
            $session->image = null;
        
        $session->ar = [
            'name' => $request->ar_name,
            'title' => $request->ar_title,
            'description' => $request->ar_description,
        ];
        $session->en = [
            'name' => $request->en_name,
            'title' => $request->en_title,
            'description' => $request->en_description,
        ];

        $session->save();
        toastr()->success("Session created successfully");

        // $this->sendPasswordEmail($data['email'], $password);
        return redirect()->to(route('admin.sessions'));
    }

    public function editSession($id)
    {
        $session = Session::findOrFail($id);
        return view('Admin.Session.update', compact('session'));
    }

    public function updateSession(Request $request, $id)
    {
        $session = Session::findOrFail($id);
        $request->validate([
            // 'image' => 'nullable|file|mimes:png,jpg',
            'ar_name' => 'required|string|max:255',
            'ar_title' => 'required|string|max:255',
            'ar_description' => 'required|string',
            'en_name' => 'required|string|max:255',
            'en_title' => 'required|string|max:255',
            'en_description' => 'required|string',
        ]);

        // if ($request->has('image')) {
        //     $url = public_path("/storage/Sessions/");
        //     $parts = explode('/', $session->image);
        //     $url .= end($parts);
        //     @unlink($url);

        //     $name = md5($request->en_name . time()) . ".{$request->image->getClientOriginalExtension()}";
        //     $path = $request->image->storeAs("public/Sessions", $name);
        //     $session->image = str_replace('public', 'storage', $path);
        // }
            $session->image = null;

        $session->ar = [
            'name' => $request->ar_name,
            'title' => $request->ar_title,
            'description' => $request->ar_description,
        ];
        $session->en = [
            'name' => $request->en_name,
            'title' => $request->en_title,
            'description' => $request->en_description,
        ];

        $session->save();
        toastr()->success("Session has successfully updated");
        return redirect()->route('admin.sessions')->with('message','Session has successfully updated');
    }

    public function deleteSession(Session $session)
    {
        $session->delete();
        toastr()->success("Session deleted successfully");
        return back()->with('message','Session deleted successfully');
    }
    public function restoreSession($id)
    {
        $session = Session::withTrashed()->findOrFail($id);
        $session->restore();
        toastr()->success("Session restored successfully");
        return back()->with('message','Session restored successfully');
    }
       // ----------------------------Sponsors---------------------------------

    public function sponsors()
    {
        $sessions = Sponsor::withTrashed()->latest()->get();
        // dd($speakers);
        return view('Admin.Sponsor.index', compact('sessions'));
    }

    public function sponsorForm()
    {
        return view('Admin.Sponsor.create');
    }

    public function sponsorCreate(Request $request)
    {
        $request->validate([
            'image' => 'required|file|mimes:png,jpg',
            'ar_title' => 'nullable|string|max:255',
            'en_title' => 'nullable|string|max:255',
        ]);
        $session = new Sponsor();
        $name = md5($request->en_name . time()) . ".{$request->image->getClientOriginalExtension()}";
        $path = $request->image->storeAs("public/Sponsors", $name);
        $session->image = str_replace('public', 'storage', $path);
        $session->ar = [
            'title' => $request->ar_title,
        ];
        $session->en = [
            
            'title' => $request->en_title,
            
        ];

        $session->save();
        toastr()->success("Sponsor created successfully");

        // $this->sendPasswordEmail($data['email'], $password);
        return redirect()->to(route('admin.sponsors'));
    }

    public function editSponsor($id)
    {
        $session = Sponsor::findOrFail($id);
        return view('Admin.Sponsor.update', compact('session'));
    }

    public function updateSponsor(Request $request, $id)
    {
        $session = Sponsor::findOrFail($id);
        $request->validate([
            'image' => 'nullable|file|mimes:png,jpg',
            'ar_title' => 'nullable|string|max:255',
            'en_title' => 'nullable|string|max:255',
        ]);

        if ($request->has('image')) {
            $url = public_path("/storage/Sponsors/");
            $parts = explode('/', $session->image);
            $url .= end($parts);
            @unlink($url);

            $name = md5($request->en_name . time()) . ".{$request->image->getClientOriginalExtension()}";
            $path = $request->image->storeAs("public/Sponsors", $name);
            $session->image = str_replace('public', 'storage', $path);
        }
        $session->ar = [
            'title' => $request->ar_title,
        ];
        $session->en = [
            'title' => $request->en_title,
        ];

        $session->save();
        toastr()->success("Sponsor has successfully updated");
        return redirect()->route('admin.sponsors')->with('message','Sponsor has successfully updated');
    }

    public function deleteSponsor(Sponsor $sponsor)
    {
        $sponsor->delete();
        toastr()->success("Sponsor deleted successfully");
        return back()->with('message','Sponsor deleted successfully');
    }
    public function restoreSponsor($id)
    {
        $session = Sponsor::withTrashed()->findOrFail($id);
        $session->restore();
        toastr()->success("Sponsor restored successfully");
        return back()->with('message','Sponsor restored successfully');
    }
    // ---------------------------------------------Agenda -----------------------------------


    public function agenda()
    {
        $agendas = Agenda::withTrashed()->latest()->get();
        return view('Admin.Agenda.index', compact('agendas'));
    }

    public function agendaAdd()
    {
        return view('Admin.Agenda.create');
    }

    public function agendaCreate(Request $request) {
        $data = $request->all();
        Agenda::create($data);
            toastr()->success('Agenda has successfuly created');
            return redirect(route('admin.agenda.index'));

    }
    public function agendaEdit($id){
        $agendaData = Agenda::findorFail($id);
        // dd($agendaData);
        return view('Admin.Agenda.update',compact('agendaData'));
    }
    public function agendaUpdate(Request $request,$id){
        $agendaData = Agenda::findorFail($id);
            $agendaData->day = $request->day;
           
            $agendaData->start = $request->start;
            $agendaData->end = $request->end;
            
            $agendaData->topic_en = $request->topic_en;
            $agendaData->topic_ar = $request->topic_ar;
            $agendaData->desc_en = $request->desc_en;
            $agendaData->desc_ar = $request->desc_ar;
            $agendaData->save();
            toastr()->success('Agenda has successfuly updated');
            return redirect(route('admin.agenda.index'));
    }
    public function deleteAgenda(Agenda $agenda)
    {
        $agenda->delete();
        toastr()->success("Agenda deleted successfully");
        return back()->with('message','Agenda deleted successfully');
    }
    public function restoreAgenda($id)
    {
        $agenda = Agenda::withTrashed()->findOrFail($id);
        $agenda->restore();
        toastr()->success("Agenda restored successfully");
        return back()->with('message','Agenda restored successfully');
    }
    // -----------------------Links Admin Panel-------------------------
    public function links()
    {
        $links = MeetingLink::all();
        $active = Setting::firstOrCreate(
            ['title' => 'active_links'],
            ['value' => 0]
        )->value;
        return view('Admin.links', compact('links', 'active'));
    }

    public function toggleLink()
    {
        $active = Setting::where('title', 'active_links')->first();
        $active->update(['value' => $active->value == 1 ? 0 : 1]);
        toastr()->success('Links active status changed successfully');
        return back();
    }

    public function linkCreate(Request $request)
    {
        MeetingLink::create($request->all());
        toastr()->success('Meeting Link has successfuly added');
        return back();
    }

    public function linkUpdate(Request $request, $id)
    {
        $data = $request->all();
        MeetingLink::find($id)->update($data);
        toastr()->success('Meeting Link has successfuly updated');
        return back();
    }
    public function Consultancy(){
        $consultancy = Consultancy::all();
        return view('Admin.consultancy',compact('consultancy'));
    }
    public function WorkshopAttendees(){
        $workshopattendees = WorkshopAttendees::with('user')->orderBy('date')->get();
        
        return view('Admin.WorkshopAttendees',compact('workshopattendees'));
    }
    public function EventAttendees(){
        $eventattendees = EventAttendees::with('user')->orderBy('date')->get();
        
        return view('Admin.EventAttendees',compact('eventattendees'));
    }
}
