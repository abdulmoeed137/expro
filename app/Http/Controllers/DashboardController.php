<?php

namespace App\Http\Controllers;

use App\Models\Consultancy;
use App\Models\MeetingLink;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    function livelink()
    {
        $youtubelink = MeetingLink::where('title', 'youtube')->get();
        $twitterlink = MeetingLink::where('title', 'twitter')->get();
        return view('livelink', compact('youtubelink', 'twitterlink'));
    }
    public function consultancy()
    {
        return view('startconsultancy');
    }
    public function VerifyUser()
    {
        return view('reminder');
    }
    public function VerifyUser_Store(Request $request)
    {
        // dd($request);
        $qrcodebyemail = User::select('qrcodeid','email')->where('email',$request->email)->first();
        // dd($qrcodebyemail->qrcodeid);
        $qrcodebyphonenumber = User::select('qrcodeid','email')->where('phonenumber',$request->email)->first();
        $differentiatevarriable = [];
        
        if(isset($qrcodebyemail)){
            // dd("this comees here");
            $differentiatevarriable[]="qrcodebyemail";
            // dd($differentiatevarriable[0]);
            // return response()->json(['qrcodeid'=>$qrcodebyemail,'differentiatevarriable'=>$differentiatevarriable]);
            return view('verifyqr',compact('qrcodebyphonenumber','qrcodebyemail','differentiatevarriable'));
        }
        elseif(isset($qrcodebyphonenumber)){
            $differentiatevarriable[]="qrcodebyphonenumber";
            return view('verifyqr',compact('qrcodebyphonenumber','qrcodebyemail','differentiatevarriable'));
        }
        // elseif(isset($qrcodebyemail) && isset($qrcodebyphonenumber)){
        //     $differentiatevarriable[]="qrcodebyemail";
        //     return view('verifyqrbyboth',compact('qrcodebyemail'));

        // }
        else{
            return redirect()->back()->with('error_code', 5);

        }
    }
    public function qr()
    {
        return view('qr');
    }
    public function show_consultancy_form()
    {
        return view('consultancyform');
    }
    public function store_consultancy_form(Request $request)
    {
        $this->validate($request, [
            'o_name' => 'required',
            'c_name' => 'required',
            'email' => 'required|email|unique:consultancies',
            'mobilenumber' => 'required|max:15|regex:/^([0-9\+]*)$/',
            'dayallotted_receiving' => 'required',
            'consultation_time' => 'required',
        ]);
        $consultancy = new Consultancy();

        $consultancy->organization_name = $request->o_name;
        $consultancy->candidate_name = $request->c_name;
        $consultancy->email = $request->email;
        $consultancy->mobilenumber = $request->mobilenumber;
        $consultancy->dayallotted_receiving = $request->dayallotted_receiving;
        $consultancy->consultation_time = $request->consultation_time;
        $consultancy->save();
        return redirect()->route('consultancycomplete');
    }
    public function complete_consultancy_form()
    {
        return view('completeconsultancy');
    }
}
