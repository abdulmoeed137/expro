<?php

namespace App\Http\Controllers;

use App\Models\Agenda;
use App\Models\Invalidemail;
use App\Models\MeetingLink;
use App\Models\Register;
use App\Models\Registration;
use App\Models\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Speaker;
use App\Models\Sponsor;
use App\Models\Template;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Flasher\Prime\FlasherInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    public function registration(){
        return view('registration');
    }
    public function StoreRegistration(Request $request){
        
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:registers',
            'phonenumber' => 'required|max:15|regex:/^([0-9\+]*)$/',
            'entityname' => 'required'
           
        ]);
        // dd($request);
        $register = new Register();
        $register->name = $request->name;
        $register->email = $request->email;
        $register->phonenumber = $request->phonenumber;
        $register->entityname = $request->entityname;
        $register->save();
       
        if (\App::getLocale() == "en") {
            toastr()->success('You are Successfully Registered');
            return redirect()->route('home');
        }
        // $flasher->addSuccess('Data has been saved successfully!');
        toastr()->success('تم التسجيل بنجاح');
        return redirect()->route('home');
    }

    
    public function showregsitration()
    {
        return view('auth.register');
    }
    // public function registration(Request $request)
    // {
    //     $randomNumber = random_int(100000, 999999);
    //     $this->validate($request, [
    //         'name' => 'required',
    //         // 'country' => 'required|regex:/^([a-zA-Z]+\s)*[a-zA-Z]+$/',
    //         'email' => 'required|email|unique:users',
    //         'phonenumber' => 'required|max:15|regex:/^([0-9\+]*)$/',
    //         // 'mobilenumber' => 'required|max:15|regex:/^([0-9\+]*)$/',
    //         'companyname' => 'required',
    //         // 'companywebsite' => 'required|regex:/^(?!-)[A-Za-z0-9-]+([\\-\\.]{1}[a-z0-9]+)*\\.[A-Za-z]{2,6}$/',
    //         'jobtitle' => 'required',
    //         // 'reference' => 'required',

    //         // 'c_password' => 'required|same:password',
    //     ]);
    //     //    dd($qr);
    //     $data = new User();
    //     $data->name = $request->name;
    //     $data->email = $request->email;
    //     $data->phonenumber = $request->phonenumber;
    //     $data->companyname = $request->companyname;
    //     $data->jobtitle = $request->jobtitle;
    //     $data->qrcodeid = $randomNumber;
    //     $data->save();
    //     $image = QrCode::format('png')
    //         ->size(200)->errorCorrection('H')
    //         ->generate($data->qrcodeid);
    //     $output_file = 'public/qr-code/' .$data->qrcodeid.'.png';
    //     Storage::disk('local')->put($output_file, $image);
    //     // $qrcode = QrCode::size(100)->generate($data->qrcodeid);

    //     // $name = md5($qrcode) . '.png';
    //     // $path = $qrcode->storeAs("public/qrcode", $name);
    //     // dd($data->qrcodeid);
    //     // $image = QrCode::format('png')->merge('../public/assets/img/speakers/Fransisco_Martinez.jpg', 0.3, true)
    //     //          ->size(200)->errorCorrection('H')
    //     //          ->generate('A simple example of QR code!');
    //     //         $output_file = '../public/Qrimg/qr-code/img-' . time() . '.png';
    //     //         Storage::disk('local')->put($output_file, $image);
    //     // $randomstring = md5($request->name.time());
    //     // $qr = QrCode::size(300)->generate('hbskjhvbkj');
    //     // $qr->storeAs("public/QrCode",$randomstring);

    //     $template = Template::where('name', 'Registration')->first();
    //     $details = [

    //         // 'subject' => $template->subject,
    //         // 'content'=>$template->content,
    //         'qrcodeid' => $data->qrcodeid,
    //         // 'qr'=>$image,
    //         // 'body' => 'You have successfully registered at King Salman Global Academy.',
    //         // 'email' => $request->Email,
    //         // 'name'=>$request->Name,
    //     ];

    //    try{
    //     $mail = \Mail::to($request->email)->send(new \App\Mail\RegisterMail($details));

    //     //code to send the mail
     
    //  }catch(\Exception $e){
    //     //  dd("hello");
    //     $data = new Invalidemail();
    //     $data->name = $request->name;
    //     $data->email = $request->email;
    //     $data->phonenumber = $request->phonenumber;
    //     $data->companyname = $request->companyname;
    //     $data->jobtitle = $request->jobtitle;
    //     $data->qrcodeid = $randomNumber;
    //     $data->save();

    //     if (\App::getLocale() == "en") {
    //         toastr()->success('You are Successfully Registered');
    //         return redirect()->route('register');
    //     }
    //     toastr()->success('تم التسجيل بنجاح');
    //     return redirect()->route('register');
    // //    dd("there is some error");
    //  }
    
    //     if (\App::getLocale() == "en") {
    //         toastr()->success('You are Successfully Registered');
    //         return redirect()->route('register');
    //     }
    //     toastr()->success('تم التسجيل بنجاح');
    //     return redirect()->route('register');
    // }

    public function index()
    {
        $speakers = Speaker::all()->take(8);
        $sponsors = Sponsor::all();
        $DayOneAgenda = Agenda::where('day','1')->get();
        $DayTwoAgenda = Agenda::where('day','2')->get();
        $DayThreeAgenda = Agenda::where('day','3')->get();
        return view('home', compact('speakers','sponsors','DayOneAgenda','DayTwoAgenda','DayThreeAgenda'));
    }
    public function dashboard()
    {
        $meetinglink = MeetingLink::count();
        // dd($meetinglink);
        return view('dashboard',compact('meetinglink'));
    }
    public function livelink()
    {
        return view('livelink');
    }
    public function emailtemplate()
    {
        $qrcodeid = User::where('name', 'tahmeer')->get(['qrcodeid']);
        // dd($qrcodeid);
        return view('Admin.emails.register', compact('qrcodeid'));
    }
    public function registration2(){
        return view('auth.register2');
    }
    public function registration_store(Request $request)
    {
        $randomNumber = random_int(100000, 999999);
        $this->validate($request, [
            'name' => 'required',
            'country' => 'required',
            'email' => 'required|email|unique:users',
            'phonenumber' => 'required|max:15|regex:/^([0-9\+]*)$/',
            'mobilenumber' => 'required|max:15|regex:/^([0-9\+]*)$/',
            'companyname' => 'required',
            'companywebsite' => 'required|regex:/^(?!-)[A-Za-z0-9-]+([\\-\\.]{1}[a-z0-9]+)*\\.[A-Za-z]{2,6}$/',
            'jobtitle' => 'required',
            'reference' => 'required',

            // 'c_password' => 'required|same:password',
        ]);
        //    dd($qr);
        $data = new User();
        $data->name = $request->name;
        $data->country = $request->country;
        $data->email = $request->email;
        $data->password = Hash::make($request->Password);
        $data->phonenumber = $request->phonenumber;
        $data->mobilenumber = $request->mobilenumber;
        $data->companyname = $request->companyname;
        $data->companywebsite = $request->companywebsite;
        $data->jobtitle = $request->jobtitle;
        $data->reference = $request->reference;
        $data->qrcodeid = $randomNumber;
        $data->save();
        // dd($data->qrcodeid);
        $image = QrCode::format('png')
            ->size(200)->errorCorrection('H')
            ->generate($data->qrcodeid);
        $output_file = 'public/qr-code/' .$data->qrcodeid.'.png';
        Storage::disk('local')->put($output_file, $image);
        // $qrcode = QrCode::size(100)->generate($data->qrcodeid);

        // $name = md5($qrcode) . '.png';
        // $path = $qrcode->storeAs("public/qrcode", $name);
        // dd($data->qrcodeid);
        // $image = QrCode::format('png')->merge('../public/assets/img/speakers/Fransisco_Martinez.jpg', 0.3, true)
        //          ->size(200)->errorCorrection('H')
        //          ->generate('A simple example of QR code!');
        //         $output_file = '../public/Qrimg/qr-code/img-' . time() . '.png';
        //         Storage::disk('local')->put($output_file, $image);
        // $randomstring = md5($request->name.time());
        // $qr = QrCode::size(300)->generate('hbskjhvbkj');
        // $qr->storeAs("public/QrCode",$randomstring);

        $template = Template::where('name', 'Registration')->first();
        $details = [

            // 'subject' => $template->subject,
            // 'content'=>$template->content,
            'qrcodeid' => $data->qrcodeid,
            // 'qr'=>$image,
            // 'body' => 'You have successfully registered at King Salman Global Academy.',
            // 'email' => $request->Email,
            // 'name'=>$request->Name,
        ];

        \Mail::to($request->email)->send(new \App\Mail\RegisterMail($details));
        if (\App::getLocale() == "en") {
            toastr()->success('You are Successfully Registered');
            return redirect()->route('register2');
        }
        toastr()->success('تم التسجيل بنجاح');
        return redirect()->route('register2');
    }
    public function speakers(){
        $speakers = Speaker::all();
        return view('speakers',compact('speakers'));
    }
    public function agenda(){
        $sessions = Session::all();
        $DayOneAgenda = Agenda::where('day','1')->get();
        $DayTwoAgenda = Agenda::where('day','2')->get();
        $DayThreeAgenda = Agenda::where('day','3')->get();
        return view('agenda',compact('sessions','DayOneAgenda','DayTwoAgenda','DayThreeAgenda'));
    }
    public function objectives(){
        return view('objective');
    }
}
