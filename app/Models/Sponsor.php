<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Sponsor extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'image', 'ar', 'en'
    ];
    protected $hidden = [
        'ar', 'en',
    ];

    protected $casts = [
        'ar' => 'object',
        'en' => 'object',
    ];

    protected $appends = [
        'title'
    ];

    
    public function getTitleAttribute()
    {
        return $this->{app()->getLocale()}->title;
    }
    
}