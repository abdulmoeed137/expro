<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Consultancy extends Model
{
    use HasFactory;
    protected $fillable = ['organiation_name','candidate_name','email','mobile_number','dayallotted_receiving','consultation_time'];
}
