<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invalidemail extends Model
{
    use HasFactory;
    protected $fillable = ['name','email','phonenumber','companyname','jobtitle','qrcodeid'
];
}
