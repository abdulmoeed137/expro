<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegisterMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $details;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // $this->from('no-reply@spider-connect.com')->subject($this->details['subject'])
        //     ->to($this->details['subject'], $this->details['content'],$this->details['name'])
        //     ->view('Admin.emails.template')->with([
        //         'subject' => $this->details['subject'],
        //         'content' => $this->details['content'],
        //         // 'qr' => $this->details['qr'],
        //     ]);
        return $this->subject('Greening Arabia')
                    ->view('Admin.emails.register')->with([
                        'qrcodeid' => $this->details['qrcodeid'],
                    ]);
    }
}
