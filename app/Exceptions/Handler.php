<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;


class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $e)
    {

        if (!config('app.debug')) {

            // 404 page when a model is not found
            if ($e instanceof \ModelNotFoundException) {
                return response()->view('customerror', [], 404);
            }

            // custom error message
            if ($e instanceof \ErrorException) {
                return response()->view('customerror', [], 500);
            } else {
                return parent::render($request, $e);
            }
        }


        return parent::render($request, $e);
    }


    protected function unauthenticated($request, AuthenticationException $exception)
    {

        $guard = $exception->guards()[0];
        switch ($guard) {
            case 'admin':
                $loginForm = "adminlogin";
                break;
            default:
                $loginForm = 'login';
                break;
        }
        return $request->expectsJson()
            ? response()->json(['message' => $exception->getMessage()], 401)
            : redirect()->guest(route($loginForm));
    }
}
